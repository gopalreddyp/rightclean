package com.rightchoice.rightclean.views.activities

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.databinding.library.baseAdapters.BR
import com.rightchoice.rightclean.R
import com.rightchoice.rightclean.activity_view_models.ChangePasswordActivityViewModel
import com.rightchoice.rightclean.activity_view_models.ForgotPasswordActivityViewModel
import com.rightchoice.rightclean.activity_view_models.navigators.ChangePasswordActivityNavigator
import com.rightchoice.rightclean.activity_view_models.navigators.ForgotPasswordActivityNavigator
import com.rightchoice.rightclean.databinding.ActivityChangePasswordBinding
import com.rightchoice.rightclean.databinding.ActivityForgotPasswordBinding
import com.rightchoice.rightclean.model.repository.webservices.callbacks.ChangePasswordCallback
import com.rightchoice.rightclean.model.repository.webservices.callbacks.ForgotPasswordCallback
import com.rightchoice.rightclean.model.repository.webservices.pojo.response.ChangePasswordResponseModel
import com.rightchoice.rightclean.model.repository.webservices.pojo.response.ForgotPasswordResponseModel
import com.rightchoice.rightclean.model.utilities.AppConstants
import com.rightchoice.rightclean.model.utilities.LogUtility
import com.techresources.clearmots.model.exceptions.NoInternetConnectionException
import okhttp3.MultipartBody
import okhttp3.RequestBody

class ChangePasswordActivity
    : BaseActivity2<ActivityChangePasswordBinding, ChangePasswordActivityViewModel>(), ChangePasswordActivityNavigator {

    override val viewModel = ChangePasswordActivityViewModel::class.java


    override fun getBindingVariable() = BR.viewModel

    override val layoutId = R.layout.activity_change_password;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectedViewModel.setNavigator(this)
    }

    override fun initInstances() {


            val requestBody: RequestBody = MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart(AppConstants.APIKeys.EMAIL, "sindhu.rightchoice1@gmail.com")
                    .addFormDataPart(AppConstants.APIKeys.PASSWORD, "sindhu@12345")
                    .addFormDataPart(AppConstants.APIKeys.CONFIRM_PASSWORD, "sindhu@12345")
                    .build()
            setProgressVisibility(View.VISIBLE)

            try {
                injectedViewModel.getChangePasswordDetails(requestBody, object : ChangePasswordCallback {

                    override fun onSuccess(response: ChangePasswordResponseModel?) {
                        setProgressVisibility(View.GONE)
                    }

                    override fun onFail(throwable: Throwable?) {
                        setProgressVisibility(View.GONE)
                        LogUtility.logInfo("ForgotPasswordActivity", "onFailure ${throwable?.message}")
                    }
                })
            } catch (e: NoInternetConnectionException) {
                setProgressVisibility(View.GONE)
                Toast.makeText(this@ChangePasswordActivity, e.message, Toast.LENGTH_SHORT).show()
            }
        }

            }





