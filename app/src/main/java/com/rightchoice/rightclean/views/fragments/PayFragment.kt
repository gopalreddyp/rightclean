package com.rightchoice.rightclean.views.fragments

import android.os.Bundle
import android.view.View
import androidx.databinding.library.baseAdapters.BR
import com.rightchoice.rightclean.R
import com.rightchoice.rightclean.activity_view_models.PayFragmentViewModel
import com.rightchoice.rightclean.activity_view_models.navigators.PayFragmentNavigator
import com.rightchoice.rightclean.databinding.FragmentPayBinding

class PayFragment :BaseFragment2<FragmentPayBinding,PayFragmentViewModel>(),PayFragmentNavigator {

    override val viewModel = PayFragmentViewModel::class.java

    override fun getBindingVariable() = BR.viewModel

    override val layoutId = R.layout.fragment_pay;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectedViewModel.setNavigator(this)

    }

    override fun initUserInterface(view: View?) {

    }


}