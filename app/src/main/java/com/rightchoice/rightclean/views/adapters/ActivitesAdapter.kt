package com.rightchoice.rightclean.views.adapters

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.rightchoice.rightclean.R


class ActivitesAdapter(mainActivityList: List<String>,
                            ) : RecyclerView.Adapter<ActivitesAdapter.ViewHolder>() {

    var mainActivityList: List<String> = mainActivityList

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view:View = LayoutInflater.from(parent.context).inflate(R.layout.row_activities,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvMainActivity.setText("MainActivity" )
    }

    override fun getItemCount(): Int {
        return 10
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        lateinit var tvMainActivity: TextView
        lateinit var rvSubactivities: RecyclerView

        init {
            val activity: Activity = itemView.context as Activity
            tvMainActivity = itemView.findViewById(R.id.tvMainActivity)
            rvSubactivities = itemView.findViewById(R.id.rvSubActivities)
            val subActivityAdapter = SubActivitesAdapter(subActivitiesList)
            rvSubactivities.layoutManager = LinearLayoutManager(activity,RecyclerView.VERTICAL,false)
            rvSubactivities.adapter = subActivityAdapter
        }

        private val subActivitiesList:List<String>
        private get() {
            val list:MutableList<String> = ArrayList()
            return list
        }

    }

}




