package com.rightchoice.rightclean.views.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.rightchoice.rightclean.R


class ContactsAdapter(contactList: List<String>,
                            ) : RecyclerView.Adapter<ContactsAdapter.ViewHolder>() {

    var contactList: List<String> = contactList

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view:View = LayoutInflater.from(parent.context).inflate(R.layout.row_contacts,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
      holder.tvContactName.setText(R.string.dummy_name )
    }

    override fun getItemCount(): Int {
        return 10
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        lateinit var tvContactName: TextView

        init {
           tvContactName = itemView.findViewById(R.id.tvContactName)
        }



    }

}




