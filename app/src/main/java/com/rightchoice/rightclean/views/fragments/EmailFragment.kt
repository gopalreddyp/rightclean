package com.rightchoice.rightclean.views.fragments

import android.content.Intent
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.databinding.library.baseAdapters.BR
import com.rightchoice.rightclean.R
import com.rightchoice.rightclean.activity_view_models.EmailFragmentViewModel
import com.rightchoice.rightclean.activity_view_models.navigators.EmailFragmentNavigator
import com.rightchoice.rightclean.databinding.FragmentEmailBinding
import com.rightchoice.rightclean.model.repository.webservices.callbacks.SelectModuleCallback
import com.rightchoice.rightclean.model.repository.webservices.pojo.response.SelectModuleResponseModel
import com.rightchoice.rightclean.model.repository.webservices.web_apis.WebApi
import com.rightchoice.rightclean.model.utilities.AppConstants
import com.rightchoice.rightclean.model.utilities.LogUtility
import com.techresources.clearmots.model.exceptions.NoInternetConnectionException
import kotlinx.android.synthetic.main.fragment_email.*
import okhttp3.MultipartBody
import okhttp3.RequestBody


class EmailFragment : BaseFragment2<FragmentEmailBinding, EmailFragmentViewModel>(), EmailFragmentNavigator {

    private val TAG = "EmailFragment"

    override val viewModel = EmailFragmentViewModel::class.java

    override fun getBindingVariable() = BR.viewModel

    override val layoutId = R.layout.fragment_email;

    private lateinit var To:String
    private lateinit var From:String
    private lateinit var Message:String
    private lateinit var Subject:String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectedViewModel.setNavigator(this)
    }

    override fun initUserInterface(view: View?) {
        etCustomText.isVerticalScrollBarEnabled = true
        etCustomText.movementMethod = ScrollingMovementMethod()

        val requestBody: RequestBody = MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart(AppConstants.APIKeys.RESULT,"Contacts")
                .build()
        setProgressVisibility(View.VISIBLE)

        try {
            injectedViewModel.selectModule(requestBody, object : SelectModuleCallback {
                override fun onSuccess(response: SelectModuleResponseModel?) {
                    setProgressVisibility(View.GONE)
                    if (response?.status == WebApi.Status.SUCCESS) {
                        Log.d(TAG, "onSuccess: "+ response)
                    } else {
                        Toast.makeText(context, getString(R.string.error_server), Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onFail(throwable: Throwable?) {
                    setProgressVisibility(View.GONE)
                    LogUtility.logInfo(TAG, "onFailure ${throwable?.message}")
                }
            })
        } catch (e: NoInternetConnectionException) {
            setProgressVisibility(View.GONE)
            Toast.makeText(context, e.message, Toast.LENGTH_SHORT).show()
        }

            val modules = arrayListOf<SelectModuleResponseModel>()
            val adapter = ArrayAdapter(requireContext(),R.layout.support_simple_spinner_dropdown_item,modules)
            adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line)
            spComposeEmail.adapter = adapter;

       // sendEmail()
    }

    private fun sendEmail() {
        btnSendEmail.setOnClickListener {

            To = etToComposeEmail.text.toString()
            Subject = etEmailSubject.text.toString()
            Message = etCustomText.text.toString()
             val emailIntent:Intent = Intent(Intent.ACTION_SEND)
            emailIntent.putExtra(Intent.EXTRA_EMAIL, etToComposeEmail.text.toString())
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, Subject)
            emailIntent.putExtra(Intent.EXTRA_TEXT, Message)

            emailIntent.setType("message/rfc822")

            startActivity(Intent.createChooser(emailIntent, "Select Email Sending Apps"))
        }
    }

    override fun closeFragment() {
       // getActivity().getFragmentManager().popBackStack();
       // fragmentManager?.popBackStack()
    }

}