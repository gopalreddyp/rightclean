package com.rightchoice.rightclean.views


class MailService {

    private var toList: String? = null
    private var ccList: String? = null
    private var bccList: String? = null
    private var subject: String? = null

  /*  private val SMTP_SERVER: String = DataService
            .getSetting(DataService.SETTING_SMTP_SERVER)*/
    private var from: String? = null
    private var txtBody: String? = null
    private var htmlBody: String? = null
    private var replyToList: String? = null
    private var authenticationRequired = false

    fun MailService(
            from: String?, toList: String?, subject: String?, txtBody: String?, htmlBody: String?,
    ) {
        this.txtBody = txtBody
        this.htmlBody = htmlBody
        this.subject = subject
        this.from = from
        this.toList = toList
        ccList = null
        bccList = null
        replyToList = null
        authenticationRequired = true
    }

  /*  @Throws(AddressException::class, MessagingException::class)
    fun sendAuthenticated() {
        authenticationRequired = true
        send()
    }*/

    /*private class SMTPAuthenticator : javax.mail.Authenticator() {
        protected val passwordAuthentication: PasswordAuthentication
            protected get() {
                val username: String = DataService
                        .getSetting(DataService.SETTING_SMTP_USER)
                val password: String = DataService
                        .getSetting(DataService.SETTING_SMTP_PASSWORD)
                return PasswordAuthentication(username, password)
            }
    }*/

    fun getToList(): String? {
        return toList
    }

    fun setToList(toList: String?) {
        this.toList = toList
    }

    fun getCcList(): String? {
        return ccList
    }

    fun setCcList(ccList: String?) {
        this.ccList = ccList
    }

    fun getBccList(): String? {
        return bccList
    }

    fun setBccList(bccList: String?) {
        this.bccList = bccList
    }
    fun getSubject(): String? {
        return subject
    }

    fun setSubject(subject: String?) {
        this.subject = subject
    }

    fun setFrom(from: String?) {
        this.from = from
    }

    fun setTxtBody(body: String?) {
        txtBody = body
    }

    fun setHtmlBody(body: String?) {
        htmlBody = body
    }

    fun getReplyToList(): String? {
        return replyToList
    }

    fun setReplyToList(replyToList: String?) {
        this.replyToList = replyToList
    }

    fun isAuthenticationRequired(): Boolean {
        return authenticationRequired
    }
    fun setAuthenticationRequired(authenticationRequired: Boolean) {
        this.authenticationRequired = authenticationRequired
    }
}


