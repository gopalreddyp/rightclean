package com.rightchoice.rightclean.views.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.rightchoice.rightclean.R


class SubActivitesAdapter(subActivityList: List<String>,
                            ) : RecyclerView.Adapter<SubActivitesAdapter.ViewHolder>() {

    var subActivityList: List<String> = subActivityList

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view:View = LayoutInflater.from(parent.context).inflate(R.layout.row_subactivities,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvSubActivity.setText("SubActivity")
    }

    override fun getItemCount(): Int {
        return 2
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        lateinit var tvSubActivity: TextView

        init {
            tvSubActivity = itemView.findViewById(R.id.tvSubActivities)

        }
    }

}




