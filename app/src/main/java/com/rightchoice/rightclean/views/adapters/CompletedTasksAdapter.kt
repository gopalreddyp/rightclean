package com.rightchoice.rightclean.views.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.rightchoice.rightclean.R
import com.rightchoice.rightclean.models.CompletedTasksModelClass


class CompletedTasksAdapter(completedTasksModelClass: List<CompletedTasksModelClass>,
                            ) : RecyclerView.Adapter<CompletedTasksAdapter.ViewHolder>() {

    var completedTasksModelClassList: List<CompletedTasksModelClass> = completedTasksModelClass

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view:View = LayoutInflater.from(parent.context).inflate(R.layout.row_completed_tasks,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val completedTasksModelClass:CompletedTasksModelClass= completedTasksModelClassList.get(position)
        holder.tvTaskNumber.setText(completedTasksModelClass.taskNumber)
        holder.tvTaskTitle.setText(completedTasksModelClass.taskTitle)
        holder.tvTaskDate.setText(completedTasksModelClass.taskDate)
        holder.ivTaskImage.setImageResource(completedTasksModelClass.taskImage)
    }

    override fun getItemCount(): Int {
        return completedTasksModelClassList.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        lateinit var tvTaskTitle: TextView
        lateinit var tvTaskNumber: TextView
        lateinit var tvTaskDate: TextView
        lateinit var ivTaskImage: ImageView
        lateinit var clCompletedTask: ConstraintLayout

        init {
            tvTaskDate = itemView.findViewById(R.id.tvTaskDate)
            tvTaskNumber = itemView.findViewById(R.id.tv_business_name)
            tvTaskTitle = itemView.findViewById(R.id.tvTaskName)
            ivTaskImage = itemView.findViewById(R.id.ivTask)
            clCompletedTask = itemView.findViewById(R.id.clCompletedTask)
        }
    }

}




