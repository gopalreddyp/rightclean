package com.rightchoice.rightclean.views.communication_provider

import com.rightchoice.rightclean.model.repository.webservices.pojo.response.ScheduleDetails

interface SchedularCommunicator {
    fun onDataLoadedSuccessfully(schedularDataClass: ScheduleDetails?)
}