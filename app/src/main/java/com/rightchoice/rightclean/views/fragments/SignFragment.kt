package com.rightchoice.rightclean.views.fragments

import android.os.Bundle
import android.view.View
import androidx.databinding.library.baseAdapters.BR
import com.rightchoice.rightclean.R
import com.rightchoice.rightclean.activity_view_models.SignFragmentViewModel
import com.rightchoice.rightclean.activity_view_models.navigators.SignFragmentNavigator
import com.rightchoice.rightclean.databinding.SignFragmentBinding

class SignFragment :BaseFragment2<SignFragmentBinding,SignFragmentViewModel>(),SignFragmentNavigator {

    override val viewModel = SignFragmentViewModel::class.java

    override fun getBindingVariable() = BR.signFragmentViewModel

    override val layoutId = R.layout.sign_fragment;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectedViewModel.setNavigator(this)

    }

    override fun initUserInterface(view: View?) {

    }


}