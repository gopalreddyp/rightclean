package com.rightchoice.rightclean.views.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.rightchoice.rightclean.R
import com.rightchoice.rightclean.model.repository.webservices.pojo.response.ScheduleDetails

class ScheduleAdapter(scheduleDetails: List<ScheduleDetails>?): RecyclerView.Adapter<ScheduleAdapter.ViewHolder>() {

    var scheduleList:List<ScheduleDetails>? = scheduleDetails

    override fun getItemCount(): Int {
        return scheduleList!!.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v:View = LayoutInflater.from(parent.context).inflate(R.layout.row_schedule,parent,false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.tvTime.text = scheduleList?.get(position)?.time_start
        holder.tvTask.text = scheduleList?.get(position)?.activitytype
        holder.tvClientName.text = scheduleList?.get(position)?.priority
        holder.tvAddress.text = scheduleList?.get(position)?.location
    }
    inner class ViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvClientName: TextView = itemView.findViewById(R.id.tvClientName)
        var tvAddress: TextView = itemView.findViewById(R.id.tvAddress)
        var tvTask:TextView = itemView.findViewById(R.id.tvTask)
        var tvTime:TextView = itemView.findViewById(R.id.tvTime)
    }

}