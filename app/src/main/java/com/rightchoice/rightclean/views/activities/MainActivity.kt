package com.rightchoice.rightclean.views.activities

import android.Manifest
import android.content.DialogInterface
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.rightchoice.rightclean.CircularProgressdialog.CircularProgressDialogue
import com.rightchoice.rightclean.DashboardPackage.DashboardActivity
import com.rightchoice.rightclean.R
import com.rightchoice.rightclean.model.repository.shared_preferences.SharedPreferenceManager
import com.rightchoice.rightclean.model.utilities.AppUtility.checkIfCameraAvailable
import com.rightchoice.rightclean.model.utilities.GeneralDialogues
import com.rightchoice.rightclean.model.utilities.GeneralDialogues.DialogueClickListener2
import com.rightchoice.rightclean.model.utilities.NetworkUtility.isConnectedToInternet
import com.rightchoice.rightclean.model.utilities.ToastUtility.showSmallDurationToast
import com.rightchoice.rightclean.view_models.LoginViewModel
import java.util.*

class MainActivity : BaseActivity() {

    private var mLoginViewModel: LoginViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Objects.requireNonNull(supportActionBar)?.hide()
        initInstanceVariables()
        checkCamera()
    }

    private fun checkCamera() {
        if (checkIfCameraAvailable()) {
            checkPermissions()
        } else {
            CircularProgressDialogue.showAlertDialgo(this,
                    getString(R.string.error_camera_not_found), getString(R.string.title_warning))
        }
    }

    private fun initInstanceVariables() {
        mLoginViewModel = LoginViewModel(application)
    }

    private fun checkPermissions() {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION)
                .withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                        if (report.areAllPermissionsGranted()) {
                            if (isConnectedToInternet(this@MainActivity) && report.areAllPermissionsGranted()) {
                                Snackbar.make(findViewById(android.R.id.content), getString(R.string.connected_to_internet), Snackbar.LENGTH_LONG).show()
                                CircularProgressDialogue.showDialog(this@MainActivity, "", "")
                                CircularProgressDialogue.dismissdialog()
                                if (mLoginViewModel!!.getSharedPreferences().getBoolean(SharedPreferenceManager.SharedPrefKeys.KEY_IS_USER_LOGGED_IN)) {
                                    navigateEnd(DashboardActivity::class.java)
                                } else {
                                    navigateEnd(CompanyIDActivity::class.java)
                                }
                            } else {
                                GeneralDialogues.show1ButtonsDialogue(this@MainActivity, { dialogInterface: DialogInterface ->
                                    checkPermissions()
                                    dialogInterface.dismiss()
                                }, getString(R.string.error_no_internet_connection))
                            }
                        }
                        if (report.isAnyPermissionPermanentlyDenied) showSettingsDialog()
                    }

                    override fun onPermissionRationaleShouldBeShown(permissions: List<PermissionRequest>, token: PermissionToken) {
                        token.continuePermissionRequest()
                    }
                }).withErrorListener { showSmallDurationToast(getString(R.string.error_occurred)) }
                .onSameThread()
                .check()
    }

    private fun showSettingsDialog() {
        GeneralDialogues.show2ButtonsDialogue(this@MainActivity, object : DialogueClickListener2 {
            override fun onPositiveButtonClicked(dialogInterface: DialogInterface) {
                dialogInterface.cancel()
                openOSSettingsUI()
            }

            override fun onNegativeButtonClicked(dialogInterface: DialogInterface) {
                dialogInterface.dismiss()
            }
        }, getString(R.string.dialogue_setting_screen_text), getString(R.string.go_2_settings), getString(R.string.cancel))
    }
}