package com.rightchoice.rightclean.views.fragments

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.rightchoice.rightclean.R
import androidx.databinding.library.baseAdapters.BR
import com.rightchoice.rightclean.activity_view_models.ContactSearchFragmentViewModel
import com.rightchoice.rightclean.activity_view_models.navigators.ContactsSearchFragmentNavigator
import com.rightchoice.rightclean.views.adapters.ContactsAdapter
import com.rightchoice.rightclean.databinding.FragmentContactsSearchBinding
import kotlinx.android.synthetic.main.fragment_contacts_search.*

class ContactsSearchFragment :BaseFragment2<FragmentContactsSearchBinding, ContactSearchFragmentViewModel>(),ContactsSearchFragmentNavigator {

    override val viewModel = ContactSearchFragmentViewModel::class.java

    override fun getBindingVariable() = BR.viewModel


    override val layoutId = R.layout.fragment_contacts_search;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectedViewModel.setNavigator(this)

    }

    override fun initUserInterface(view: View?) {
        setAdapter()

    }

    private fun setAdapter() {
        val contactsAdapter = ContactsAdapter(contactsList)
        rvContacts.layoutManager = LinearLayoutManager(context,RecyclerView.VERTICAL,false)
        rvContacts.adapter = contactsAdapter
    }

    private val contactsList:List<String>
          private get() {
              val list:MutableList<String> = ArrayList()
              return list
          }

}