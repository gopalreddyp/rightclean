package com.rightchoice.rightclean.views.fragments

import android.os.Bundle
import android.view.View
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.rightchoice.rightclean.R
import com.rightchoice.rightclean.activity_view_models.CompleteFragmentViewModel
import com.rightchoice.rightclean.activity_view_models.navigators.CompleteFragmentNavigator
import com.rightchoice.rightclean.views.adapters.CompletedTasksAdapter
import com.rightchoice.rightclean.databinding.FragmentCompleteBinding
import com.rightchoice.rightclean.models.CompletedTasksModelClass
import kotlinx.android.synthetic.main.fragment_complete.*

class CompleteFragment :BaseFragment2<FragmentCompleteBinding,CompleteFragmentViewModel>(),CompleteFragmentNavigator {

    override val viewModel = CompleteFragmentViewModel::class.java

    override fun getBindingVariable() = BR.viewModel

    override val layoutId = R.layout.fragment_complete;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectedViewModel.setNavigator(this)

    }

    override fun initUserInterface(view: View?) {
        setAdapter()
    }

    private fun setAdapter() {

        val completedTasksAdapter = CompletedTasksAdapter(list)
        rvTasks.layoutManager = LinearLayoutManager(context,RecyclerView.VERTICAL,false)
        rvTasks.adapter = completedTasksAdapter
    }

   private val list:List<CompletedTasksModelClass>
   private get() {
            val list:MutableList<CompletedTasksModelClass> = ArrayList()
            list.add(CompletedTasksModelClass("Task 1","House Cleaning","July 14",R.drawable.checkbox))
       return list
   }


}