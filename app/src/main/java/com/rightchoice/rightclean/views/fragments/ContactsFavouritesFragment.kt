package com.rightchoice.rightclean.views.fragments

import android.os.Bundle
import android.view.View
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.rightchoice.rightclean.R
import com.rightchoice.rightclean.activity_view_models.ContactFavoritesFragmentViewModel
import com.rightchoice.rightclean.activity_view_models.navigators.ContactsFavoritesFragmentNavigator
import com.rightchoice.rightclean.views.adapters.ContactsAdapter
import com.rightchoice.rightclean.databinding.FragmentContactsFavoritesBinding
import kotlinx.android.synthetic.main.fragment_contacts_search.*

class ContactsFavouritesFragment :BaseFragment2<FragmentContactsFavoritesBinding, ContactFavoritesFragmentViewModel>(), ContactsFavoritesFragmentNavigator {

    override val viewModel = ContactFavoritesFragmentViewModel::class.java

    override fun getBindingVariable() = BR.viewModel

    override val layoutId = R.layout.fragment_contacts_favorites;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectedViewModel.setNavigator(this)

    }

    override fun initUserInterface(view: View?) {
        setAdapter()

    }

    private fun setAdapter() {
        val contactsAdapter = ContactsAdapter(contactsList)
        rvContacts.layoutManager = LinearLayoutManager(context,RecyclerView.VERTICAL,false)
        rvContacts.adapter = contactsAdapter
    }

    private val contactsList:List<String>
          private get() {
              val list:MutableList<String> = ArrayList()
              return list
          }

}