package com.rightchoice.rightclean.views.activities

import android.content.Intent
import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.rightchoice.rightclean.activity_view_models.BaseViewModel
import com.rightchoice.rightclean.utilities.PreferenceManager
import dagger.android.support.DaggerAppCompatActivity
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

abstract class BaseActivity2<VDB : ViewDataBinding, BVM : BaseViewModel<*>> :
        DaggerAppCompatActivity() {

    //region VARIABLES
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var preferenceManager: PreferenceManager

    lateinit var injectedViewModel: BVM

    lateinit var viewDataBinding: VDB

    abstract val viewModel: Class<BVM>

    abstract fun getBindingVariable(): Int

    @get:LayoutRes
    abstract val layoutId: Int

    private val disposableDelegate = lazy { CompositeDisposable() }

    private val compositeDisposable by disposableDelegate

    //endregion

    //region LIFECYCLE
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        executeDataBinding()
        initInstances()
    }

    override fun onDestroy() {
        if (disposableDelegate.isInitialized()) {
            compositeDisposable.dispose()
        }
        super.onDestroy()
    }
    //endregion

    //region UTIL
    protected abstract fun initInstances()

    private fun executeDataBinding() {
        viewDataBinding = DataBindingUtil.setContentView(this, layoutId)
        injectedViewModel = ViewModelProviders.of(this, viewModelFactory).get(viewModel)
        viewDataBinding.setVariable(getBindingVariable(), injectedViewModel)
        viewDataBinding.executePendingBindings()
    }
    //endregion

    //endregion
    protected fun moveAppBack(isRootTask: Boolean) {
        moveTaskToBack(isRootTask)
    }

    protected fun navigateEnd(activity: Class<*>?) {
        val intent = Intent(this, activity)
        startActivity(intent)
        finish()
    }
}