package com.rightchoice.rightclean.views.activities

import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import androidx.databinding.library.baseAdapters.BR
import com.rightchoice.rightclean.R
import com.rightchoice.rightclean.activity_view_models.ForgotPasswordActivityViewModel
import com.rightchoice.rightclean.activity_view_models.navigators.ForgotPasswordActivityNavigator
import com.rightchoice.rightclean.databinding.ActivityForgotPasswordBinding
import com.rightchoice.rightclean.model.repository.webservices.callbacks.ForgotPasswordCallback
import com.rightchoice.rightclean.model.repository.webservices.pojo.response.ForgotPasswordResponseModel
import com.rightchoice.rightclean.model.utilities.AppConstants
import com.rightchoice.rightclean.model.utilities.LogUtility
import com.rightchoice.rightclean.model.utilities.ToastUtility
import com.techresources.clearmots.model.exceptions.NoInternetConnectionException
import kotlinx.android.synthetic.main.activity_forgot_password.*
import okhttp3.MultipartBody
import okhttp3.RequestBody

class ForgotPasswordActivity
    : BaseActivity2<ActivityForgotPasswordBinding, ForgotPasswordActivityViewModel>(), ForgotPasswordActivityNavigator {

    override val viewModel = ForgotPasswordActivityViewModel::class.java

    override fun getBindingVariable() = BR.viewModel

    override val layoutId = R.layout.activity_forgot_password;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectedViewModel.setNavigator(this)
    }

    override fun initInstances() {
        etForgotPasswordUserName.requestFocus()
    }

    override fun submit() {
        forgotPasswordAPI()
    }

    override fun goToSignInActivity() {
        //startActivity(Intent(this,SignInActivity::class.java))
        onBackPressed()
    }

    override fun setProgressVisibility(visibility: Int) {
        viewDataBinding.progressBar.visibility = visibility
    }


    private fun forgotPasswordAPI() {
        val userName = viewDataBinding.etForgotPasswordUserName.text.toString().trim()
        val email = viewDataBinding.etForgotPasswordEmail.text.toString().trim()
        val validationResponse = (validateCredentials(userName, email))

        if (validationResponse == null) {
            setProgressVisibility(View.VISIBLE)
            val requestBody: RequestBody = MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart(AppConstants.APIKeys.USER_NAME, userName)
                    .addFormDataPart(AppConstants.APIKeys.EMAIL_ID, email)
                    .build()
            setProgressVisibility(View.VISIBLE)

            try {
                injectedViewModel.getForgotPasswordDetails(requestBody, object : ForgotPasswordCallback {

                    override fun onSuccess(response: ForgotPasswordResponseModel?) {
                        setProgressVisibility(View.GONE)
                        Toast.makeText(this@ForgotPasswordActivity,"Email has been sent successfully!",Toast.LENGTH_SHORT).show()
                    }

                    override fun onFail(throwable: Throwable?) {
                        setProgressVisibility(View.GONE)
                        LogUtility.logInfo("ForgotPasswordActivity", "onFailure ${throwable?.message}")
                    }
                })
            } catch (e: NoInternetConnectionException) {
                setProgressVisibility(View.GONE)
                Toast.makeText(this@ForgotPasswordActivity, e.message, Toast.LENGTH_SHORT).show()
            }

            }else {
            ToastUtility.showToast(validationResponse)
        }

}
    private fun validateCredentials(userName: String, email: String): String? {
        if (TextUtils.isEmpty(userName))
            return getString(R.string.error_invalid_user)
        else if (TextUtils.isEmpty(email)) {
            return getString(R.string.error_invalid_email)
        }
        return null
    }
    }


