package com.rightchoice.rightclean.views.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.rightchoice.rightclean.R
import com.rightchoice.rightclean.views.adapters.HomeAdapter.ViewHolder
import com.rightchoice.rightclean.models.HomeModelClass


class HomeAdapter(homeModelClassList: List<HomeModelClass>,
                  homeImageClick: HomeImageClick) : RecyclerView.Adapter<ViewHolder>() {

    var homeImageClick: HomeImageClick = homeImageClick
    var homeModelClassList:List<HomeModelClass> = homeModelClassList
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v: View = LayoutInflater.from(parent.context).inflate(R.layout.row_home, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        var homeModel: HomeModelClass = homeModelClassList.get(position)
        holder.tvTitle.setText(homeModel.title)
        holder.ivHomeImage.setImageResource(homeModel.homeImages)
        holder.itemView.setOnClickListener {
            homeImageClick.onItemClick(position)
        }

    }

    override fun getItemCount(): Int {
        return homeModelClassList.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        lateinit var tvTitle:TextView
        lateinit var ivHomeImage:ImageView
        lateinit var homeAdapterContainer:ConstraintLayout
        init {
            tvTitle = itemView.findViewById(R.id.tvTitle)
            ivHomeImage = itemView.findViewById(R.id.iv_feature_type)
            homeAdapterContainer = itemView.findViewById(R.id.home_adapter_container)
        }
    }

    interface HomeImageClick{
        fun onItemClick(position:Int)
    }
}