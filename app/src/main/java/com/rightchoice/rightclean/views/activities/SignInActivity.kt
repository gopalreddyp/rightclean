package com.rightchoice.rightclean.views.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Handler
import android.text.TextUtils
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.MotionEvent
import android.view.View
import androidx.databinding.library.baseAdapters.BR
import com.google.android.material.snackbar.Snackbar
import com.rightchoice.rightclean.DashboardPackage.DashboardActivity
import com.rightchoice.rightclean.R
import com.rightchoice.rightclean.activity_view_models.SignInActivityViewModel
import com.rightchoice.rightclean.activity_view_models.navigators.SignInActivityNavigator
import com.rightchoice.rightclean.databinding.ActivitySignInBinding
import com.rightchoice.rightclean.model.repository.webservices.callbacks.LoginResponseCallback
import com.rightchoice.rightclean.model.repository.webservices.pojo.response.LoginResponseModel
import com.rightchoice.rightclean.model.repository.webservices.web_apis.WebApi
import com.rightchoice.rightclean.model.utilities.AppConstants
import com.rightchoice.rightclean.model.utilities.LogUtility
import com.rightchoice.rightclean.model.utilities.ToastUtility
import com.techresources.clearmots.model.exceptions.NoInternetConnectionException
import okhttp3.MultipartBody
import okhttp3.RequestBody

class SignInActivity : BaseActivity2<ActivitySignInBinding, SignInActivityViewModel>(), SignInActivityNavigator {

    private val TAG = "CompanyIDActivity"

    private var mShowingPasswordFormat = true

    override val viewModel = SignInActivityViewModel::class.java

    override fun getBindingVariable() = BR.viewModel

    override val layoutId = R.layout.activity_sign_in;

    @SuppressLint("ClickableViewAccessibility")
    override fun initInstances() {
        injectedViewModel.setNavigator(this)
        viewDataBinding.etPassword.apply {
            setOnTouchListener(View.OnTouchListener { v, event ->
                val DRAWABLE_RIGHT = 2
                if (event.action == MotionEvent.ACTION_UP) {
                    if (event.rawX >= getRight() - getCompoundDrawables().get(DRAWABLE_RIGHT).getBounds().width()) {
                        transformationMethod = HideReturnsTransformationMethod.getInstance()
                        if (mShowingPasswordFormat)
                            transformationMethod = PasswordTransformationMethod.getInstance()

                        mShowingPasswordFormat = !mShowingPasswordFormat
                        return@OnTouchListener true
                    }
                }
                false
            })
        }
    }

    override fun setProgressVisibility(visibility: Int) {
        viewDataBinding.progressBar.visibility = visibility
    }

    override fun login() {
        attemptLogin()
    }

    override fun getForgotPasswordActivity() {
        startActivity(Intent(this,ForgotPasswordActivity::class.java))
    }

    private fun attemptLogin() {
        val userName = viewDataBinding.etEmailAddress.text.toString().trim()
        val password = viewDataBinding.etPassword.text.toString().trim()
        val validationResponse = (validateCredentials(userName, password))

        if  (validationResponse == null) {
            setProgressVisibility(View.VISIBLE)
            try {
                val requestBody: RequestBody = MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart(AppConstants.APIKeys.USER_NAME, userName)
                        .addFormDataPart(AppConstants.APIKeys.PASSWORD, password)
                        .build()
                injectedViewModel.attemptLogin(requestBody, object : LoginResponseCallback {
                    override fun onSuccess(response: LoginResponseModel?) {
                        setProgressVisibility(View.GONE)
                        if (response != null) {
                            if (response.status == WebApi.Status.SUCCESS) {
                                injectedViewModel.saveLoginResponse(response)
                                Snackbar.make(findViewById(android.R.id.content), "Welcome ${response.data.name}", Snackbar.LENGTH_LONG).show()
                                Handler().postDelayed({
                                    navigateEnd(DashboardActivity::class.java)
                                }, 2000)
                            } else if (response.status == WebApi.Status.ERROR) {
                                ToastUtility.showToast(getString(R.string.error_server))
                            }
                        }
                    }

                    override fun onFail(throwable: Throwable?) {
                        setProgressVisibility(View.GONE)
                        LogUtility.logInfo(TAG, throwable!!.message!!)
                    }
                })
            } catch (e: NoInternetConnectionException) {
                setProgressVisibility(View.GONE)
                ToastUtility.showToast(e.message)
            }
        } else {
            ToastUtility.showToast(validationResponse)
        }
    }

    private fun validateCredentials(userName: String, password: String): String? {
        if (TextUtils.isEmpty(userName)) /*||
                !Patterns.EMAIL_ADDRESS.matcher(userName).matches())*/
            return getString(R.string.error_invalid_user)
        else if (TextUtils.isEmpty(password)) {
            return getString(R.string.error_invalid_password)
        }
        return null
    }

}