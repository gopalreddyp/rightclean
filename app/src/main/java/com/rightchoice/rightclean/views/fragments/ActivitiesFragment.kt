package com.rightchoice.rightclean.views.fragments

import android.os.Bundle
import android.view.View
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.rightchoice.rightclean.R
import com.rightchoice.rightclean.activity_view_models.ActivitiesFragmentViewModel
import com.rightchoice.rightclean.activity_view_models.navigators.ActivitiesFragmentNavigator
import com.rightchoice.rightclean.views.adapters.ActivitesAdapter
import com.rightchoice.rightclean.databinding.FragmentActivityBinding
import kotlinx.android.synthetic.main.fragment_activity.*

class ActivitiesFragment :BaseFragment2<FragmentActivityBinding,ActivitiesFragmentViewModel>(),ActivitiesFragmentNavigator {

    override val viewModel = ActivitiesFragmentViewModel::class.java

    override fun getBindingVariable() = BR.viewModel

    override val layoutId = R.layout.fragment_activity;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectedViewModel.setNavigator(this)
    }

    override fun initUserInterface(view: View?) {
        setActivityAdapter()
    }

    private fun setActivityAdapter() {

        val activitiesAdapter = ActivitesAdapter(activityList)
        rvActivities.layoutManager = LinearLayoutManager(context,RecyclerView.VERTICAL,false)
        rvActivities.adapter = activitiesAdapter
    }

   private val activityList:List<String>
   private get() {
            val list:MutableList<String> = ArrayList()
            list.add("MainActivity")
       return list
   }


}