package com.rightchoice.rightclean.views.fragments

import android.os.Bundle
import android.view.View
import androidx.databinding.library.baseAdapters.BR
import com.rightchoice.rightclean.R
import com.rightchoice.rightclean.activity_view_models.CallFragmentViewModel
import com.rightchoice.rightclean.activity_view_models.navigators.CallFragmentNavigator
import com.rightchoice.rightclean.databinding.FragmentCallBinding
import com.rightchoice.rightclean.views.ViewPagerAdapter
import kotlinx.android.synthetic.main.fragment_call.*

class CallFragment :BaseFragment2<FragmentCallBinding, CallFragmentViewModel>(), CallFragmentNavigator {

    override val viewModel = CallFragmentViewModel::class.java

    override fun getBindingVariable() = BR.viewModel

    override val layoutId = R.layout.fragment_call;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectedViewModel.setNavigator(this)

    }

    override fun initUserInterface(view: View?) {
        setUpAccountViewPager()
        tlContacts.setupWithViewPager(vpContacts)

    }

    private fun setUpAccountViewPager() {
        val callAdapter = ViewPagerAdapter(childFragmentManager)
        callAdapter.addFragment(ContactsSearchFragment(), resources.getString(R.string.contacts))
        callAdapter.addFragment(ContactsFavouritesFragment(), resources.getString(R.string.favorites))
        vpContacts.adapter = callAdapter
    }


}