package com.rightchoice.rightclean.views.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.rightchoice.rightclean.R
import com.rightchoice.rightclean.model.repository.webservices.pojo.response.ScheduleDetails
import com.rightchoice.rightclean.model.utilities.AppConstants
import com.rightchoice.rightclean.model.utilities.LogUtility
import com.rightchoice.rightclean.views.communication_provider.SchedularCommunicator


class SchedularAdpater(val communicator: SchedularCommunicator, val onClickSchedularOptions: OnClickSchedularOptions) : RecyclerView.Adapter<SchedularAdpater.ViewHolder>() {

    private val TAG = "SchedularAdpater"

    var mScheduleList: List<ScheduleDetails> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.row_schedule_tasks, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentItem = mScheduleList[position]
        holder.tvTasksNumber.text = currentItem.business_name
        holder.tvScheduleTask.text = currentItem.activitytype
        holder.tvDate.text = "${currentItem.date_start} -- ${currentItem.due_date}"
        holder.tvTiming.text = "${currentItem.time_start} -- ${currentItem.time_end}"
        holder.tvTotalHours.text = currentItem.duration_hours.toString()

        LogUtility.logInfo(TAG, "Schedule Task Status ${currentItem.event_status}")
        if (currentItem.event_status == AppConstants.ScheduleWorkStatus.NOT_STARTED){
            holder.tvCheckout.visibility = View.GONE
            holder.tvUploadImages.visibility = View.GONE
            holder.tvCheckin.visibility = View.VISIBLE
        } else {
            holder.tvCheckout.visibility = View.VISIBLE
            holder.tvUploadImages.visibility = View.VISIBLE
            holder.tvCheckin.visibility = View.GONE
        }
    }

    override fun getItemCount(): Int {
        return mScheduleList.size
    }

    fun setItems(scheduleDetails: List<ScheduleDetails>) {
        this.mScheduleList = scheduleDetails
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var tvScheduleTask: TextView = itemView.findViewById(R.id.tvScheduleTask)
        var tvDate: TextView = itemView.findViewById(R.id.tvSchedularDate)
        var tvTasksNumber: TextView = itemView.findViewById(R.id.tv_business_name)
        var tvTiming: TextView = itemView.findViewById(R.id.tvSchedularTiming)
        var tvTotalHours: TextView = itemView.findViewById(R.id.tvSchedularHours)
        var tvCheckin: TextView = itemView.findViewById(R.id.tvCheckIn)
        var tvCheckout: TextView = itemView.findViewById(R.id.tvCheckOut)
        var tvUploadImages: TextView = itemView.findViewById(R.id.tvUploadImages)
        var clMainContainer: ConstraintLayout = itemView.findViewById(R.id.main_container)

        init {
            clMainContainer.setOnClickListener(this)
            tvCheckin.setOnClickListener(this)
            tvCheckout.setOnClickListener(this)
            tvUploadImages.setOnClickListener(this)
        }

        override fun onClick(view: View?) {
            when(view?.id) {
                R.id.main_container -> communicator.onDataLoadedSuccessfully(mScheduleList[bindingAdapterPosition])
                R.id.tvCheckIn -> onClickSchedularOptions.onClickCheckIn(bindingAdapterPosition)
                R.id.tvCheckOut -> onClickSchedularOptions.onClickCheckOut(bindingAdapterPosition)
                R.id.tvChooseFile -> onClickSchedularOptions.onClickUploadImage(bindingAdapterPosition)
            }
        }
    }

    interface OnClickSchedularOptions {
        fun onClickCheckIn(position: Int)
        fun onClickCheckOut(position: Int)
        fun onClickUploadImage(position: Int)
    }
}

