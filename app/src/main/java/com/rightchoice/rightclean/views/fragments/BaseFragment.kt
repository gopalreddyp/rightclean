package com.rightchoice.rightclean.views.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

abstract class BaseFragment : Fragment() {

    protected var mView: View? = null

    private var mContentView = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mView = inflater.inflate(mContentView, container, false)
        return mView
    }

    protected fun setContentView(contentView: Int) {
        mContentView = contentView
    }
}