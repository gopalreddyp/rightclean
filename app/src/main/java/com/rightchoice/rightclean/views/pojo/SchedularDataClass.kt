package com.rightchoice.rightclean.views.pojo

data class SchedularDataClass(var street:String,
                              var state:String,
                              var country:String,
                              var businessName:String)
