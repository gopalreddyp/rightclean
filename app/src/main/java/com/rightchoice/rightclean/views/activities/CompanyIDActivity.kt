package com.rightchoice.rightclean.views.activities

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.databinding.library.baseAdapters.BR
import com.rightchoice.rightclean.R
import com.rightchoice.rightclean.activity_view_models.CompanyIDActivityViewModel
import com.rightchoice.rightclean.activity_view_models.navigators.CompanyIDActivityNavigator
import com.rightchoice.rightclean.databinding.ActivityCompanyIdBinding
import com.rightchoice.rightclean.model.repository.webservices.callbacks.GetCompanyDetailsCallback
import com.rightchoice.rightclean.model.repository.webservices.pojo.response.GetCompanyDetailsResponseModel
import com.rightchoice.rightclean.model.repository.webservices.web_apis.WebApi
import com.rightchoice.rightclean.model.utilities.AppConstants
import com.rightchoice.rightclean.model.utilities.LogUtility
import com.techresources.clearmots.model.exceptions.NoInternetConnectionException
import kotlinx.android.synthetic.main.activity_filter.view.*
import okhttp3.MultipartBody
import okhttp3.RequestBody


class CompanyIDActivity : BaseActivity2<ActivityCompanyIdBinding, CompanyIDActivityViewModel>(), CompanyIDActivityNavigator {

        private val TAG = "CompanyIDActivity"

        override val viewModel = CompanyIDActivityViewModel::class.java

        override fun getBindingVariable() = BR.viewModel

        override val layoutId = R.layout.activity_company_id;

        override fun initInstances() {

        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectedViewModel.setNavigator(this)
    }

    override fun getLoginScreen() {
        viewDataBinding.etCompanyID.apply {
            if (text?.length == AppConstants.ThreshHolds.COMPANY_ID) {
                val requestBody: RequestBody = MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart(AppConstants.APIKeys.ID, text.toString())
                        .build()
                setProgressVisibility(View.VISIBLE)
                try {
                    injectedViewModel.getCompanyDetails(requestBody, object : GetCompanyDetailsCallback {
                        override fun onSuccess(response: GetCompanyDetailsResponseModel?) {
                            setProgressVisibility(View.GONE)
                            if (response?.status == WebApi.Status.SUCCESS) {
                                injectedViewModel.saveCompanyDetails(response.result)
                                navigateEnd(SignInActivity::class.java)
                            } else {
                                Toast.makeText(this@CompanyIDActivity, getString(R.string.error_server), Toast.LENGTH_SHORT).show()
                            }
                        }

                        override fun onFail(throwable: Throwable?) {
                            setProgressVisibility(View.GONE)
                            LogUtility.logInfo(TAG, "onFailure ${throwable?.message}")
                        }
                    })
                } catch (e: NoInternetConnectionException) {
                    setProgressVisibility(View.GONE)
                    Toast.makeText(this@CompanyIDActivity, e.message, Toast.LENGTH_SHORT).show()
                }
            } else {
                setError(getString(R.string.error_enter_valid_company_id))
            }
        }
    }

    override fun setProgressVisibility(visibility: Int) {
        viewDataBinding.progressBar.visibility = visibility
    }

    override fun showHidePasswordData() {

    }

}