package com.rightchoice.rightclean.views.activities

import android.os.Bundle
import android.widget.ImageView
import androidx.databinding.library.baseAdapters.BR
import com.rightchoice.rightclean.R
import com.rightchoice.rightclean.activity_view_models.ProfileActivityViewModel
import com.rightchoice.rightclean.activity_view_models.navigators.ProfileActivityNavigator
import com.rightchoice.rightclean.databinding.ActivityProfileBinding

class ProfileActivity
    : BaseActivity2<ActivityProfileBinding,ProfileActivityViewModel>(), ProfileActivityNavigator{

    override val viewModel = ProfileActivityViewModel::class.java

    override fun getBindingVariable() = BR.homeActivityViewModel

    override val layoutId = R.layout.activity_profile;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectedViewModel.setNavigator(this)
    }

    override fun initInstances() {

        val ivBack:ImageView = findViewById(R.id.ivBack)

        ivBack.setOnClickListener {
            onBackPressed()
        }

        val loginDetails = injectedViewModel.getLoginResponse();

        viewDataBinding.tvEmployeeName.text = "Employee Name : ${loginDetails.data.name}"
        viewDataBinding.tvPhoneEmail.text = "Phone/Mail : ${loginDetails.data.userData.phone} / ${loginDetails.data.userData.email}"

    }



}
