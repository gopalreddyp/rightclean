package com.rightchoice.rightclean.views.fragments

import android.os.Bundle
import android.view.View
import androidx.databinding.library.baseAdapters.BR
import androidx.fragment.app.FragmentManager
import com.rightchoice.rightclean.R
import com.rightchoice.rightclean.activity_view_models.HomeFragmentViewModel
import com.rightchoice.rightclean.activity_view_models.navigators.HomeFragmentNavigator
import com.rightchoice.rightclean.databinding.FragmentHomeBinding
import com.rightchoice.rightclean.model.repository.webservices.pojo.response.ScheduleDetails
import com.rightchoice.rightclean.views.communication_provider.SchedularCommunicator
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : BaseFragment2<FragmentHomeBinding, HomeFragmentViewModel>(),
        HomeFragmentNavigator, SchedularCommunicator {

    private val TAG = "HomeFragment"

    override val viewModel = HomeFragmentViewModel::class.java
    override fun getBindingVariable() = BR.viewModel
    override val layoutId = R.layout.fragment_home;

    private var isHide: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectedViewModel.setNavigator(this)
    }

    override fun initUserInterface(view: View?) {}

    override fun setDropDown() {
        if (!isHide) {
            isHide = true
            clDropDown.visibility = View.VISIBLE
        } else {
            isHide = false
            clDropDown.visibility = View.GONE
        }
    }


    override fun getSchedularFragment() {
        val schedularFragment = SchedularFragment(this)
        val manager: FragmentManager = childFragmentManager
        manager.beginTransaction().replace(R.id.clHomeContainer, schedularFragment).commit()
    }

    override fun getEmailFragment() {
        val emailfragment = EmailFragment()
        val manager: FragmentManager = childFragmentManager
        manager.beginTransaction().replace(R.id.clHomeContainer, emailfragment).commit()

    }

    override fun onDataLoadedSuccessfully(schedularDataClass: ScheduleDetails?) {
        clGoogleMapHeader.visibility = View.VISIBLE
        tvName.text = schedularDataClass?.business_name
        tvSchedularAddress.text = schedularDataClass?.street.toString() +
                schedularDataClass?.state +
                schedularDataClass?.country
    }


}

/*  override fun getPayFragment() {
      val payFragment = PayFragment()
      val manager:FragmentManager = childFragmentManager
      manager.beginTransaction().replace(R.id.clHomeContainer,payFragment).commit()
  }

  override fun getCompletedTaskFragment() {
      val completeFragment = CompleteFragment()
      val manager:FragmentManager = childFragmentManager
      manager.beginTransaction().replace(R.id.clHomeContainer,completeFragment).commit()
  }

  override fun getActivityFragment() {
      val activityFragment = ActivitiesFragment()
      val manager:FragmentManager = childFragmentManager
      manager.beginTransaction().replace(R.id.clHomeContainer,activityFragment).commit()
  }


  override fun getCallFragment() {
      val callFragment = CallFragment()
      val manager:FragmentManager = childFragmentManager
      manager.beginTransaction().replace(R.id.clHomeContainer,callFragment).commit()    }*/

