package com.rightchoice.rightclean.views.activities

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import androidx.appcompat.app.AppCompatActivity
import com.rightchoice.rightclean.views.fragments.BaseFragment



abstract class BaseActivity : AppCompatActivity() {

    object RequestCodes {
        const val ACTION_APPLICATION_DETAILS_SETTINGS: Int = 1

        private const val TAG = "BaseActivity"
    }

    protected var mBaseFragment: BaseFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    protected fun navigate(activity: Class<*>?) {
        val intent = Intent(this, activity)
        startActivity(intent)
    }

    protected fun navigateEnd(activity: Class<*>?) {
        val intent = Intent(this, activity)
        startActivity(intent)
        finish()
    }

    fun navigate(activity: Class<*>?, bundle: Bundle?) {
        val intent = Intent(this, activity)
        intent.putExtras(bundle!!)
        startActivity(intent)
    }

    protected fun navigateEnd(activity: Class<*>?, bundle: Bundle?) {
        val intent = Intent(this, activity)
        intent.putExtras(bundle!!)
        startActivity(intent)
        finish()
    }


    protected fun navigate(activity: Class<*>?, flag: Int) {
        val intent = Intent(this, activity)
        intent.addFlags(flag)
        startActivity(intent)
    }

    protected fun navigate(intent: Intent) {
        startActivity(intent)
    }

    protected fun openOSSettingsUI() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", packageName, null)
        intent.data = uri
        startActivityForResult(intent, RequestCodes.ACTION_APPLICATION_DETAILS_SETTINGS)
    }

    protected fun openWebLink(link: String) {
        val intent = Intent()
        intent.action = Intent.ACTION_VIEW
        intent.addCategory(Intent.CATEGORY_BROWSABLE)
        intent.data = Uri.parse(link)
        startActivity(intent)
    }
}
