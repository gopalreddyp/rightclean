package com.rightchoice.rightclean.views.fragments

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.location.Location
import android.view.View
import android.widget.CalendarView
import android.widget.Toast
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.rightchoice.rightclean.JsonNetworkPackage.AppController
import com.rightchoice.rightclean.R
import com.rightchoice.rightclean.activity_view_models.SchedularFragmentViewModel
import com.rightchoice.rightclean.activity_view_models.navigators.SchedularFragmentNavigator
import com.rightchoice.rightclean.databinding.FragmentSchedularBinding
import com.rightchoice.rightclean.model.repository.webservices.callbacks.GetSchedularDetailsCallback
import com.rightchoice.rightclean.model.repository.webservices.callbacks.StartWorkCallback
import com.rightchoice.rightclean.model.repository.webservices.pojo.response.GetSchedularDetailsResponseModel
import com.rightchoice.rightclean.model.repository.webservices.pojo.response.ScheduleDetails
import com.rightchoice.rightclean.model.repository.webservices.pojo.response.StartWorkResponse
import com.rightchoice.rightclean.model.repository.webservices.web_apis.WebApi
import com.rightchoice.rightclean.model.utilities.*
import com.rightchoice.rightclean.views.adapters.SchedularAdpater
import com.rightchoice.rightclean.views.communication_provider.SchedularCommunicator
import com.techresources.clearmots.model.exceptions.NoInternetConnectionException
import kotlinx.android.synthetic.main.fragment_schedular.*
import okhttp3.MultipartBody


class SchedularFragment(val communicator: SchedularCommunicator) : BaseFragment2<FragmentSchedularBinding,
        SchedularFragmentViewModel>(), SchedularFragmentNavigator, SchedularAdpater.OnClickSchedularOptions {

    override val viewModel = SchedularFragmentViewModel::class.java
    override fun getBindingVariable() = BR.viewModel
    override val layoutId = R.layout.fragment_schedular
    private lateinit var mSchedularAdapter: SchedularAdpater

    //#region Overridden Methods

    override fun initUserInterface(view: View?) {
        injectedViewModel.setNavigator(this)

        initTaskList()

        val currentDate = DateTimeUtility.getCurrentDate(DateTimeUtility.DateTimeFormat.FORMAT_FOR_TASKS_LISt)
        getScheduleTasks(currentDate)

        viewDataBinding.cvSchedular.setOnDateChangeListener(CalendarView.OnDateChangeListener { view, year, month, dayOfMonth ->
            val selectedDate = "$year-${injectedViewModel.getFormattedMonth(month)}-$dayOfMonth"
            getScheduleTasks(selectedDate)
        })
    }

    override fun setProgressVisibility(visibility: Int) {
        viewDataBinding.progressBar.visibility = visibility
    }

    override fun onClickCheckIn(position: Int) {
        if (LocationUtility.isLocationEnabled(requireContext())) {
            if (PermissionUtility.checkIfPermissionGranted(requireContext(), AppConstants.Permissions.LOCATION_PERMISSION)) {
                //Progress dialogue should be shown here, as before location there is not benefit to start work
                LocationUtility.getLocation(requireContext()) {
                    startWork(mSchedularAdapter.mScheduleList[position], it)
                }
            } else {
                PermissionUtility.requestPermission(requireActivity(), AppConstants.Permissions.LOCATION_PERMISSION)
            }
        } else {
            ToastUtility.showToast(getString(R.string.error_location_disabled))
            LocationUtility.gotoLocationSettings(requireActivity(), AppConstants.RequestCode.LOCATION_REQUEST_CODE)
        }
    }

    override fun onClickCheckOut(position: Int) {

    }

    override fun onClickUploadImage(position: Int) {

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            AppConstants.RequestCode.LOCATION_REQUEST_CODE -> {
                if (resultCode != RESULT_OK) {
                    ToastUtility.showToast(getString(R.string.error_location_enabled_error))
                }
            }
        }
    }
    //#endregion Overridden Methods

    //#region Utility Methods
    private fun initTaskList() {
        mSchedularAdapter = SchedularAdpater(communicator, this)
        rvScheduleTask.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        rvScheduleTask.adapter = mSchedularAdapter
    }

    private fun setSchedularAdapter(scheduleDetails: GetSchedularDetailsResponseModel?) {
        viewDataBinding.rvScheduleTask.visibility = View.VISIBLE
        viewDataBinding.tvNoDataFound.visibility = View.GONE

        scheduleDetails?.result?.let {
            mSchedularAdapter.setItems(it)
            mSchedularAdapter.notifyDataSetChanged()
        }
    }


    //region API's and request Params

    private fun getScheduleTasks(date: String) {
        setProgressVisibility(View.VISIBLE)
        try {
            injectedViewModel.getSchedularDetails(getScheduleTaskParams(date), object : GetSchedularDetailsCallback {
                override fun onSuccess(response: GetSchedularDetailsResponseModel?) {
                    setProgressVisibility(View.GONE)
                    if (response?.status == WebApi.Status.SUCCESS)
                        setSchedularAdapter(response)
                    else {
                        viewDataBinding.rvScheduleTask.visibility = View.GONE
                        viewDataBinding.tvNoDataFound.apply {
                            visibility = View.VISIBLE
                            text = response?.message
                        }
                    }
                }


                override fun onFail(throwable: Throwable?) {
                    setProgressVisibility(View.GONE)
                    LogUtility.logInfo(AppController.TAG, "onFailure ${throwable?.message}")
                }
            })
        } catch (e: NoInternetConnectionException) {
            setProgressVisibility(View.GONE)
            Toast.makeText(context, e.message, Toast.LENGTH_SHORT).show()
        }
    }

    private fun startWork(scheduleDetails: ScheduleDetails, location: Location) {
        setProgressVisibility(View.VISIBLE)
        try {
            injectedViewModel.startWork(getStartWorkParams(scheduleDetails, location), object : StartWorkCallback {
                override fun onSuccess(response: StartWorkResponse?) {
                    setProgressVisibility(View.GONE)
                    if (response?.status == WebApi.Status.SUCCESS) {
                        ToastUtility.showToast(getString(R.string.success_work_start))
                    } else {
                        ToastUtility.showToast(response?.message)
                    }
                }

                override fun onFail(throwable: Throwable?) {
                    setProgressVisibility(View.GONE)
                    LogUtility.logInfo(AppController.TAG, "onFailure ${throwable?.message}")
                }
            })
        } catch (e: NoInternetConnectionException) {
            setProgressVisibility(View.GONE)
            Toast.makeText(context, e.message, Toast.LENGTH_SHORT).show()
        }
    }

    private fun getScheduleTaskParams(date: String): MultipartBody {
        return MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart(AppConstants.APIKeys.DATE_FROM, date)
                .addFormDataPart(AppConstants.APIKeys.DATE_TO, date)
                .addFormDataPart(AppConstants.APIKeys.TIME_FROM, "")
                .addFormDataPart(AppConstants.APIKeys.TIME_TO, "")
                .build()
    }

    private fun getStartWorkParams(scheduleDetails: ScheduleDetails, location: Location): MultipartBody {
        val empID = UserSessionManager.instance.getEmployeeId()
        return MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart(AppConstants.APIKeys.EMP_ID, empID)
                .addFormDataPart(AppConstants.APIKeys.CLIENT_ID, scheduleDetails.clientid)//this will be filled once available in API
                .addFormDataPart(AppConstants.APIKeys.Activity_ID, scheduleDetails.activityid)
                .addFormDataPart(AppConstants.APIKeys.LATITUDE, location.latitude.toString())
                .addFormDataPart(AppConstants.APIKeys.LONGITUDE, location.longitude.toString())
                .build()
    }

    //region API's and request Params

    //#endregion Utility Methods

}