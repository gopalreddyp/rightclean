package com.rightchoice.rightclean.views.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.rightchoice.rightclean.R;
import com.rightchoice.rightclean.model.repository.webservices.pojo.entities.ClientData;

import java.util.List;

public class ClientsAdapter extends RecyclerView.Adapter<ClientsAdapter.ClientsViewHolder> {

    private List<ClientData> clientsList;
    private ClientProvider clientProvider;

    public ClientsAdapter(List<ClientData> clientsList, ClientProvider provider) {
        this.clientsList = clientsList;
        clientProvider = provider;
    }

    @NonNull
    @Override
    public ClientsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_gridview_layout, parent, false);
        return new ClientsViewHolder(row);
    }

    @Override
    public void onBindViewHolder(@NonNull ClientsViewHolder holder, int position) {
        ClientData current = clientsList.get(position);
        holder.tvClientName.setText(current.getClients_tks_clientname());
        holder.tvClientId.setText(current.getId());
        holder.tvEmail.setText(current.getEmail());
        holder.tvAddress.setText(current.getAddress());
        holder.tvTime.setText(current.getTime());
        holder.tvContactNo.setText(current.getPhone());
    }

    @Override
    public int getItemCount() {
        return clientsList.size();
    }

    class ClientsViewHolder extends RecyclerView.ViewHolder {

        TextView tvClientName, tvClientId, tvAddress, tvTime, tvContactNo, tvEmail;
        LinearLayout llMain;

        public ClientsViewHolder(@NonNull View itemView) {
            super(itemView);

            tvClientName = itemView.findViewById(R.id.clientname);
            tvClientId = itemView.findViewById(R.id.clientid);
            tvAddress = itemView.findViewById(R.id.address);
            tvTime = itemView.findViewById(R.id.httime);
            tvContactNo = itemView.findViewById(R.id.phonenumber);
            tvEmail = itemView.findViewById(R.id.emailid);

            llMain = itemView.findViewById(R.id.ll_main);

            llMain.setOnClickListener(view -> clientProvider.getClientPosition(getAdapterPosition()));
        }
    }

    public interface ClientProvider {
        void getClientPosition(int position);
    }
}
