package com.rightchoice.rightclean.views.activities

import android.view.View
import android.widget.CalendarView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.rightchoice.rightclean.BR
import com.rightchoice.rightclean.R
import com.rightchoice.rightclean.activity_view_models.ScheduleActivityViewModel
import com.rightchoice.rightclean.activity_view_models.navigators.ScheduleActivityNavigator
import com.rightchoice.rightclean.databinding.ActivityScheduleBinding
import com.rightchoice.rightclean.model.repository.webservices.callbacks.GetSchedularDetailsCallback
import com.rightchoice.rightclean.model.repository.webservices.pojo.response.GetSchedularDetailsResponseModel
import com.rightchoice.rightclean.model.utilities.AppConstants
import com.rightchoice.rightclean.model.utilities.LogUtility
import com.rightchoice.rightclean.views.adapters.ScheduleAdapter
import com.techresources.clearmots.model.exceptions.NoInternetConnectionException
import okhttp3.MultipartBody
import okhttp3.RequestBody


class ScheduleActivity: BaseActivity2<ActivityScheduleBinding,ScheduleActivityViewModel>(),ScheduleActivityNavigator {

    private val TAG = "ScheduleActivity"

    override val viewModel = ScheduleActivityViewModel::class.java
    override fun getBindingVariable() = BR.viewModel
    override val layoutId = R.layout.activity_schedule;


    override fun initInstances() {
        injectedViewModel.setNavigator(this)
        viewDataBinding.calendarView.setOnDateChangeListener(CalendarView.OnDateChangeListener { view, year, month, dayOfMonth ->
            viewDataBinding.calendarView.apply {

            }

        })

    }

    private fun getSchedularTasks(toDate: String) {
        val requestBody: RequestBody = MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart(AppConstants.APIKeys.DATE_FROM, "")
                .addFormDataPart(AppConstants.APIKeys.DATE_TO, toDate)
                .addFormDataPart(AppConstants.APIKeys.TIME_FROM, "")
                .addFormDataPart(AppConstants.APIKeys.TIME_TO, "")
                .build()
        setProgressVisibility(View.VISIBLE)

        try {
            injectedViewModel.getSchedularDetails(requestBody, object : GetSchedularDetailsCallback {
                override fun onSuccess(response: GetSchedularDetailsResponseModel?) {
                    setProgressVisibility(View.GONE)
                    setScheduleAdapter(response)
                }

                override fun onFail(throwable: Throwable?) {
                    setProgressVisibility(View.GONE)
                    LogUtility.logInfo(TAG, "onFailure ${throwable?.message}")
                }
            })
        } catch (e: NoInternetConnectionException) {
            setProgressVisibility(View.GONE)
            Toast.makeText(this@ScheduleActivity, e.message, Toast.LENGTH_SHORT).show()
        }
    }
    private fun setScheduleAdapter(scheduleDetails: GetSchedularDetailsResponseModel?) {
        val scheduleAdapter = ScheduleAdapter(scheduleDetails?.result)
        viewDataBinding.rvSchedule.layoutManager = LinearLayoutManager(this,RecyclerView.VERTICAL,false)
        viewDataBinding.rvSchedule.adapter = scheduleAdapter
    }
}