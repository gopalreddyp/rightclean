package com.rightchoice.rightclean.ExecutiveProfilePackage;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.rightchoice.rightclean.DashboardPackage.DashboardActivity;
import com.rightchoice.rightclean.R;
import com.rightchoice.rightclean.model.utilities.ImageUtility;
import com.rightchoice.rightclean.view_models.LoginViewModel;
import com.rightchoice.rightclean.views.activities.BaseActivity;

import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

public class ExecutiveProfile extends BaseActivity {

    private static final String TAG = ExecutiveProfile.class.getSimpleName();

    private CircleImageView profile_pic;
    private TextView tvName, tvId, tvContact;

    private LoginViewModel mLoginViewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_executive_profile);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        initViews();
        initInstanceVariables();

        tvName.setText(mLoginViewModel.getEmployeeName());
        tvId.setText(mLoginViewModel.getEmployeeId());
        tvContact.setText(mLoginViewModel.getPhoneNumber());

        ImageUtility.loadImage(this, mLoginViewModel.getProfilePicURL(), profile_pic);
    }

    private void initInstanceVariables() {
        mLoginViewModel = new LoginViewModel(getApplication());
    }

    private void initViews() {
        profile_pic = findViewById(R.id.executive_profile_photo);
        tvName = findViewById(R.id.tv_name);
        tvId = findViewById(R.id.tv_id);
        tvContact = findViewById(R.id.partneridinfo);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        navigateEnd(DashboardActivity.class);
    }

    private Bitmap getCircularBitmap(Bitmap bitmap) {


        Bitmap output;
        if (bitmap.getWidth() > bitmap.getHeight()) {
            output = Bitmap.createBitmap(bitmap.getHeight(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        } else {
            output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getWidth(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        float r = 0;

        if (bitmap.getWidth() > bitmap.getHeight()) {
            r = bitmap.getHeight() / 2;
        } else {
            r = bitmap.getWidth() / 2;
        }

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawCircle(r, r, r, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return output;


    }
}
