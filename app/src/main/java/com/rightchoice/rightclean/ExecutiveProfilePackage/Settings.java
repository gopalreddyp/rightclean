package com.rightchoice.rightclean.ExecutiveProfilePackage;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.rightchoice.rightclean.CircularProgressdialog.CircularProgressDialogue;
import com.rightchoice.rightclean.DashboardPackage.DashboardActivity;
import com.rightchoice.rightclean.R;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class Settings extends AppCompatActivity {


    private static final String TAG = Settings.class.getSimpleName();
    Button addorchange, resizecam;
    AlertDialog.Builder alertDialog;
    ImageView imageView,imageView1;
    private static int RESULT_LOAD_IMAGE = 1;
    SharedPreferences.Editor editor;
    Bitmap bitmap;
    private Uri mImageUri;
    DisplayMetrics metrics;
    SharedPreferences sharedpreferences1;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        addorchange = findViewById(R.id.addorcancel);
        resizecam = findViewById(R.id.resizecamera);
        imageView = findViewById(R.id.modifiy);
        imageView1 = findViewById(R.id.modifiy1);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        sharedpreferences1 = getSharedPreferences("Companydetails", Context.MODE_PRIVATE);

        String logourl = sharedpreferences1.getString("complogopath",null);



        final Target mTarget = new Target()
        {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom loadedFrom)
            {
                Log.d("DEBUG", "onBitmapLoaded");
                //progress_bar.setVisibility(View.GONE);
                imageView.setImageBitmap(bitmap);  //getCircularBitmap(bitmap)
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                Log.d("DEBUG", "onBitmapFailed");
                imageView.setImageResource(R.drawable.right_clean_logo);
            }


            @Override
            public void onPrepareLoad(Drawable drawable) {
                Log.d("DEBUG", "onPrepareLoad");
            }
        };
        Picasso.get().load(logourl).into(mTarget);



        metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        Log.e(TAG, " Normal widthPixels: " + metrics.widthPixels + " -- metrics: " + metrics.heightPixels);


        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String mImageUri = preferences.getString("image", "default");
        assert mImageUri != null;
        imageView1.setVisibility(View.INVISIBLE);
        if (!mImageUri.equals("default")) {
            Uri urii = Uri.parse(mImageUri);
            try {
              /*  bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(urii));
                imageView1.setImageBitmap(bitmap);
              */  Picasso.get().load(mImageUri)
                        .error(R.color.white)
                        .into(imageView1);
            } catch (Exception ignored)
            {

            }

        } else
        {
            imageView1.setImageResource(R.color.white);
        }



        addorchange.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ObsoleteSdkInt")
            @Override
            public void onClick(View view) {
                /*Intent intent;
                if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {

                    intent = new Intent();
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    intent.setType("image/*");
                    startActivityForResult(intent, Build.VERSION_CODES.KITKAT);

                } else {
                    intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                    intent.setType("image/*");
                    startActivityForResult(intent, Build.VERSION_CODES.KITKAT);
                }*/

                Snackbar.make(findViewById(android.R.id.content),"currently this feature is not enable !",Snackbar.LENGTH_SHORT).show();

            }
        });



        resizecam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {

                Snackbar.make(findViewById(android.R.id.content),"currently this feature is not enable !",Snackbar.LENGTH_LONG).show();

                //showCustomdialog();
            }
        });

    }



    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(Settings.this, DashboardActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onDestroy() {
        CircularProgressDialogue.dismissdialog();
        super.onDestroy();
    }

/*
    private void showCustomdialog()
    {
        final String names[] = {"High", "Meduim", "Low", "Default", "Cancel"};
        alertDialog = new AlertDialog.Builder(Settings.this);
        LayoutInflater inflater = getLayoutInflater();
        View convertView = (View) inflater.inflate(R.layout.custom_alert_dialog, null);
        alertDialog.setView(convertView);
        ListView lv = (ListView) convertView.findViewById(R.id.lv);
        */
/*ArrayAdapter<String> adapter = new  ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,names);
         *//*

        final AlertDialog alert = alertDialog.create();
        MyAdapter myadapter = new MyAdapter(getApplication(), R.layout.alertlist_item_text, names);

        lv.setAdapter(myadapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                final int width ;
                final int height ;
                switch (i)
                {
                    case 0:
                        width = metrics.widthPixels;
                        height = metrics.heightPixels;
                        editor = getSharedPreferences("CAMERARESIZE", MODE_PRIVATE).edit();
                        editor.putInt("width",width);
                        editor.putInt("height",height);
                        editor.apply();
                        alert.cancel();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run()
                            {
                                Snackbar.make(findViewById(android.R.id.content),"Changes Apply from Next Start!",Snackbar.LENGTH_LONG).show();
                            }
                        },1000);

                    break;

                    case 1:
                        width = metrics.widthPixels/2;
                        height = metrics.heightPixels/2;
                        editor = getSharedPreferences("CAMERARESIZE", MODE_PRIVATE).edit();
                        editor.putInt("width",width);
                        editor.putInt("height",height);
                        editor.apply();
                        alert.cancel();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Snackbar.make(findViewById(android.R.id.content),"Changes Apply from Next Start!",Snackbar.LENGTH_LONG).show();
                            }
                        },1000);
                    break;

                    case 2:
                        width = metrics.widthPixels/3;
                        height = metrics.heightPixels/3;
                        editor = getSharedPreferences("CAMERARESIZE", MODE_PRIVATE).edit();
                        editor.putInt("width",width);
                        editor.putInt("height",height);
                        editor.apply();
                        alert.cancel();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run()
                            {
                                Snackbar.make(findViewById(android.R.id.content),"Changes Apply from Next Start!",Snackbar.LENGTH_LONG).show();
                            }
                        },1000);
                        break;

                    case 3:
                        //320,240
                        width = 320;
                        height = 240;
                        editor = getSharedPreferences("CAMERARESIZE", MODE_PRIVATE).edit();
                        editor.putInt("width",width);
                        editor.putInt("height",height);
                        editor.apply();
                        alert.cancel();
                        new Handler().postDelayed(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                Snackbar.make(findViewById(android.R.id.content),"Changes Apply from Next Start!",Snackbar.LENGTH_LONG).show();
                            }
                        },1000);
                     break;
                    case 4:
                        alert.cancel();
                     break;

                }
*/
/*
                if (i == 4) {
                    alert.cancel();

                } else {

                    circularprogresssdialog.showDialog(Settings.this, "", "");
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            circularprogresssdialog.dismissdialog();
                            alert.cancel();
                            startActivity(new Intent(Settings.this, StartCamera.class));
                            finish();

                        }
                    }, 1000);

                }*//*

            }
        });
        alert.show();
    }
*/


    private class ViewHolder {

        TextView tvSname;

    }

    private class MyAdapter extends ArrayAdapter<String> {

        LayoutInflater inflater;

        Context myContext;

        List<String> newList;

        public MyAdapter(Context context, int resource, String[] list) {

            super(context, resource, list);

            // TODO Auto-generated constructor stub

            myContext = context;

            newList = Arrays.asList(list);

            inflater = LayoutInflater.from(context);

        }

        @Override

        public View getView(final int position, View view, ViewGroup parent) {

            final ViewHolder holder;

            if (view == null) {

                holder = new ViewHolder();

                view = inflater.inflate(R.layout.alertlist_item_text, null);

                holder.tvSname = (TextView) view.findViewById(R.id.item);

                view.setTag(holder);

            } else {

                holder = (ViewHolder) view.getTag();

            }

            holder.tvSname.setText(newList.get(position).toString());

            return view;

        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode,  Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK ) {


            mImageUri = data.getData();
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("image", String.valueOf(mImageUri));
            editor.apply();
            imageView1.setImageURI(mImageUri);
            imageView1.invalidate();




/*

            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();
            imageView1.setVisibility(View.VISIBLE);
            imageView1.setImageBitmap(BitmapFactory.decodeFile(picturePath));
*/

        }
    }



    }

