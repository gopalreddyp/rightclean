package com.techresources.clearmots.model.exceptions

import android.content.Context
import com.rightchoice.rightclean.R


class DataNullabilityException(private val mContext: Context) : BaseException() {
    override val message: String
        get() = mContext.getString(R.string.error_data_not_found)
}