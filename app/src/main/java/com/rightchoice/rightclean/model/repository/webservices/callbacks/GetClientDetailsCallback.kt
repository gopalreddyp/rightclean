package com.rightchoice.rightclean.model.repository.webservices.callbacks

import com.rightchoice.rightclean.model.repository.webservices.pojo.response.GetClientsDetailsResponseModel

interface GetClientDetailsCallback {

    fun onSuccess(response: GetClientsDetailsResponseModel?)
    fun onFail(throwable: Throwable?)
}