package com.rightchoice.rightclean.model.repository.webservices.pojo.requests;

public class LoginRequestModel extends BaseRequestModel {

    private String username, password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
