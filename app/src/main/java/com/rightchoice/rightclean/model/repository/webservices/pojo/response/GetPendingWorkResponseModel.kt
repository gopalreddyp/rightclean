package com.rightchoice.rightclean.model.repository.webservices.pojo.response

import com.rightchoice.rightclean.model.repository.webservices.pojo.entities.PendingWorkModel

data class GetPendingWorkResponseModel(
        var isWorkPending: Boolean,
        var result: MutableList<PendingWorkModel>
) : BaseResponseModel()