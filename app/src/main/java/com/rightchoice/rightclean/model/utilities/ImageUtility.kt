package com.rightchoice.rightclean.model.utilities

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.rightchoice.rightclean.R

object ImageUtility {

    @JvmStatic
    fun loadImage(context: Context, imageUrl: String, imageView: ImageView) {
        Glide.with(context)
                .load(imageUrl)
                .error(R.drawable.right_clean_logo)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(imageView)
    }

}