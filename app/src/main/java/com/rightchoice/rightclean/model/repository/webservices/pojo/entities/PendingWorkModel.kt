package com.rightchoice.rightclean.model.repository.webservices.pojo.entities

data class PendingWorkModel (
        var transactionid: String,
        var transactionno: String,
        var employeeId: String,
        var tags: String,
        var clientId: String,
        var punchIn: String,
        var punchOut: String,
        var servicelist: String,
        var hours: String,
)