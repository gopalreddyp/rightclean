package com.rightchoice.rightclean.model.repository.webservices.pojo.response

data class ForgotPasswordResponseModel (
        var username: String,
        var emailId: String
        ) : BaseResponseModel()