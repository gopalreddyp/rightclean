package com.rightchoice.rightclean.model.repository.webservices.pojo.entities

data class UserDataEntity(
        var employeesid: String,
        var email: String,
        var phone: String,
        var address: String,
        var regKey: String,
        var employeestatus: String
)