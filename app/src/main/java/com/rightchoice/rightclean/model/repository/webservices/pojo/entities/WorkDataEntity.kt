package com.rightchoice.rightclean.model.repository.webservices.pojo.entities

class WorkDataEntity(
        var transactionid: String,
        var transactionno: String,
        var transaction_tks_employees: String,
        var tags: String,
        var cf_1054:String,
        var cf_1170: String,
        var cf_1172: String,
        var cf_1185: String,
        var cf_1342: String
)