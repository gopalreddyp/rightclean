package com.techresources.clearmots.model.exceptions

class FragmentCommunicatorNotFoundException : BaseException() {
    override val message: String
        get() = "CallFragmentCommunicator is not implemented by the calling Activity. For calling functionality, this interface MUST be implemented."
}