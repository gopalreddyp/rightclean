package com.rightchoice.rightclean.model.repository.webservices.pojo.response

data class EndWorkResponseModel (
        var id: Int,
        var servicelist: String
        ) : BaseResponseModel()