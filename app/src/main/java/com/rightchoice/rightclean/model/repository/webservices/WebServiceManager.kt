package com.rightchoice.rightclean.model.repository.webservices

import android.content.Context
import com.google.gson.GsonBuilder
import com.rightchoice.rightclean.BuildConfig
import com.rightchoice.rightclean.model.repository.webservices.callbacks.*
import com.rightchoice.rightclean.model.repository.webservices.pojo.response.*
import com.rightchoice.rightclean.model.repository.webservices.web_apis.WebApi
import com.rightchoice.rightclean.model.utilities.AppConstants
import com.rightchoice.rightclean.model.utilities.LogUtility
import com.rightchoice.rightclean.model.utilities.LogUtility.logInfo
import com.rightchoice.rightclean.model.utilities.NetworkUtility.isConnectedToInternet
import com.techresources.clearmots.model.exceptions.NoInternetConnectionException
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class WebServiceManager(context: Context) {

    private val mRetrofit: Retrofit
    private val mContext: Context = context

    companion object {
        private val TAG = WebServiceManager::class.java.simpleName

        private const val HEADER_CONTENT_TYPE = "Content-Type"
        private const val HEADER_CONTENT_VALUE = "application/json"
        private const val HEADER_KEY_AUTHORIZATION = "Authorization"
        private const val USER_NAME = "admin"
        private const val PASSWORD = "r7nid3s1DH30pN"
    }

    init {
        if (!isConnectedToInternet(context)) throw NoInternetConnectionException(mContext)
        val gson = GsonBuilder()
                .setLenient()
                .create()
        mRetrofit = Retrofit.Builder()
                .baseUrl(AppConstants.BASE_URL)
                .client(getClient())
              //  .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
    }

    private fun getClient(): OkHttpClient {
        return OkHttpClient().newBuilder()
                .connectTimeout(20, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(20, TimeUnit.SECONDS)
                .addInterceptor(basicAuthInterceptor())
                //.addNetworkInterceptor(networkInterceptor())
                .build()
    }

    private fun basicAuthInterceptor(): Interceptor {
        return Interceptor {
            val requestBuilder = it.request()
                    .newBuilder()
                requestBuilder.addHeader(HEADER_KEY_AUTHORIZATION, Credentials.basic(USER_NAME, PASSWORD))
          //  requestBuilder.addHeader(HEADER_CONTENT_TYPE, "application/x-www-form-urlencoded")
            it.proceed(requestBuilder.build())
        }
    }

    private fun networkInterceptor(): HttpLoggingInterceptor {
        val loggingInterceptor = HttpLoggingInterceptor()
        if (BuildConfig.DEBUG) {
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        } else {
            loggingInterceptor.level = HttpLoggingInterceptor.Level.NONE
        }

        return loggingInterceptor
    }

    fun attemptLogin(requestModel: RequestBody, callback: LoginResponseCallback) {
        val api = mRetrofit.create(WebApi::class.java)
        logInfo(TAG, requestModel.toString())
        val call = api.attemptLogin(requestModel)
        call?.enqueue(object : Callback<LoginResponseModel?> {
            override fun onResponse(call: Call<LoginResponseModel?>, response: Response<LoginResponseModel?>) {
                logInfo(TAG, "onResponse() attemptLogin $response")
                callback.onSuccess(response.body())
            }

            override fun onFailure(call: Call<LoginResponseModel?>, t: Throwable) {
                logInfo(TAG, "onFailure attemptLogin " + t.message)
                callback.onFail(t)
            }
        })
    }

    fun getCompanyDetails(requestBody: RequestBody, callback: GetCompanyDetailsCallback) {
        val api = mRetrofit.create(WebApi::class.java)
        logInfo(TAG, requestBody.toString())
        val call = api.getCompanyDetails(requestBody)
        call?.enqueue(object : Callback<GetCompanyDetailsResponseModel?> {
            override fun onFailure(We: Call<GetCompanyDetailsResponseModel?>, t: Throwable) {
                logInfo(TAG, "onFailure getCompanyDetails " + t.message)
                callback.onFail(t)
            }

            override fun onResponse(call: Call<GetCompanyDetailsResponseModel?>, response: Response<GetCompanyDetailsResponseModel?>) {
                logInfo(TAG, "onResponse() getCompanyDetails $response" + response.body().toString())
                callback.onSuccess(response.body())
            }
        })
    }

    fun getForgotPasswordEmail(requestBody: RequestBody, callback: ForgotPasswordCallback) {
        val api = mRetrofit.create(WebApi::class.java)
        logInfo(TAG, requestBody.toString())
        val call = api.getForgotPasswordEmail(requestBody)
        call?.enqueue(object : Callback<ForgotPasswordResponseModel?> {
            override fun onFailure(We: Call<ForgotPasswordResponseModel?>, t: Throwable) {
                logInfo(TAG, "onFailure getForgotPasswordEmail " + t.message)
                callback.onFail(t)
            }

            override fun onResponse(call: Call<ForgotPasswordResponseModel?>, response: Response<ForgotPasswordResponseModel?>) {
                logInfo(TAG, "onResponse() getForgotPasswordEmail $response" + response.body().toString())
                callback.onSuccess(response.body())
            }
        })
    }

    fun getChangePasswordDetails(requestBody: RequestBody, callback: ChangePasswordCallback) {
        val api = mRetrofit.create(WebApi::class.java)
        logInfo(TAG, requestBody.toString())
        val call = api.getChangePasswordDetails(requestBody)
        call?.enqueue(object : Callback<ChangePasswordResponseModel?> {
            override fun onFailure(We: Call<ChangePasswordResponseModel?>, t: Throwable) {
                logInfo(TAG, "onFailure getForgotPasswordEmail " + t.message)
                callback.onFail(t)
            }

            override fun onResponse(call: Call<ChangePasswordResponseModel?>, response: Response<ChangePasswordResponseModel?>) {
                logInfo(TAG, "onResponse() getForgotPasswordEmail $response" + response.body().toString())
                callback.onSuccess(response.body())
            }
        })
    }


    fun getSchedularDetails(requestBody: RequestBody, callback: GetSchedularDetailsCallback) {
        val api = mRetrofit.create(WebApi::class.java)
        logInfo(TAG, requestBody.toString())
        val call = api.getSchedularDetails(requestBody)
        call.enqueue(object : Callback<GetSchedularDetailsResponseModel?> {
            override fun onFailure(We: Call<GetSchedularDetailsResponseModel?>, t: Throwable) {
                logInfo(TAG, "onFailure getSchedularDetails " + t.message)
                callback.onFail(t)
            }

            override fun onResponse(call: Call<GetSchedularDetailsResponseModel?>, response: Response<GetSchedularDetailsResponseModel?>) {
                logInfo(TAG, "onResponse() getSchedularDetails $response" + response.body().toString())
                callback.onSuccess(response.body())
            }
        })
    }

    fun startWork(requestBody: RequestBody, callback: StartWorkCallback) {
        val api = mRetrofit.create(WebApi::class.java)
        logInfo(TAG, requestBody.toString())
        val call = api.startWork(requestBody)
        call.enqueue(object : Callback<StartWorkResponse?> {
            override fun onFailure(We: Call<StartWorkResponse?>, t: Throwable) {
                logInfo(TAG, "onFailure startWork " + t.message)
                callback.onFail(t)
            }

            override fun onResponse(call: Call<StartWorkResponse?>, response: Response<StartWorkResponse?>) {
                logInfo(TAG, "onResponse() startWork $response" + response.body().toString())
                callback.onSuccess(response.body())
            }
        })
    }

    fun getClientsDetail(requestBody: RequestBody, callback: GetClientDetailsCallback) {
        val api = mRetrofit.create(WebApi::class.java)
        logInfo(TAG, requestBody.toString())
        val call = api.getClientsDetails(requestBody)
        call?.enqueue(object : Callback<GetClientsDetailsResponseModel?> {
            override fun onFailure(We: Call<GetClientsDetailsResponseModel?>, t: Throwable) {
                logInfo(TAG, "onFailure getClientsDetail " + t.message)
                callback.onFail(t)
            }

            override fun onResponse(call: Call<GetClientsDetailsResponseModel?>, response: Response<GetClientsDetailsResponseModel?>) {
                logInfo(TAG, "onResponse() getClientsDetail $response")
                callback.onSuccess(response.body())
            }
        })
    }

    fun selectedModule(requestBody: RequestBody, callback: SelectModuleCallback) {
        val api = mRetrofit.create(WebApi::class.java)
        logInfo(TAG, requestBody.toString())
        val call = api.getSelectedModule(requestBody)
        call?.enqueue(object : Callback<SelectModuleResponseModel?> {
            override fun onFailure(We: Call<SelectModuleResponseModel?>, t: Throwable) {
                logInfo(TAG, "onFailure getSelectedModule " + t.message)
                callback.onFail(t)
            }

            override fun onResponse(call: Call<SelectModuleResponseModel?>, response: Response<SelectModuleResponseModel?>) {
                logInfo(TAG, "onResponse() getSelectedModule $response")
                callback.onSuccess(response.body())
            }
        })
    }

    fun getPendingWork(requestBody: RequestBody, callback: GetPendingWorkCallback) {
        val api = mRetrofit.create(WebApi::class.java)
        logInfo(TAG, requestBody.toString())
        val call = api.getPendingWork(requestBody)
        call?.enqueue(object : Callback<GetPendingWorkResponseModel?> {
            override fun onFailure(We: Call<GetPendingWorkResponseModel?>, t: Throwable) {
                logInfo(TAG, "onFailure getPendingWork " + t.message)
                callback.onFail(t)
            }

            override fun onResponse(call: Call<GetPendingWorkResponseModel?>, response: Response<GetPendingWorkResponseModel?>) {
                logInfo(TAG, "onResponse() getPendingWork $response")
                callback.onSuccess(response.body())
            }
        })
    }

    fun sendEmployeeLocation(requestBody: RequestBody, callback: SendEmployeeLocationsCallback) {
        val api = mRetrofit.create(WebApi::class.java)
        logInfo(TAG, requestBody.toString())
        val call = api.sendEmployeeLocation(requestBody)
        call?.enqueue(object : Callback<EmployeeLocationResponseModel?> {
            override fun onFailure(We: Call<EmployeeLocationResponseModel?>, t: Throwable) {
                logInfo(TAG, "onFailure sendEmployeeLocation " + t.message)
                callback.onFail(t)
            }

            override fun onResponse(call: Call<EmployeeLocationResponseModel?>, response: Response<EmployeeLocationResponseModel?>) {
                logInfo(TAG, "onResponse() sendEmployeeLocation $response")
                callback.onSuccess(response.body())
            }
        })
    }

    fun endWork(id: RequestBody?, punchOut: RequestBody?, empId: RequestBody?,
                      clientId: RequestBody?, serviceList: RequestBody?, imageFile: MultipartBody.Part?,
                      responseCallback: EndWorkResponseCallback) {
        val api = mRetrofit.create(WebApi::class.java)
        val call: Call<EndWorkResponseModel?>? = api.upload(id, punchOut, empId, clientId, serviceList, imageFile)
        call!!.enqueue(object : Callback<EndWorkResponseModel?> {
            override fun onResponse(call: Call<EndWorkResponseModel?>, response: Response<EndWorkResponseModel?>) {
                LogUtility.logInfo(TAG, "onResponse() endWork $response")
                responseCallback.onSuccess(response.body())
            }

            override fun onFailure(call: Call<EndWorkResponseModel?>, t: Throwable) {
                LogUtility.logInfo(TAG, "onFailure endWork " + t.message)
                responseCallback.onFail(t)
            }
        })
    }
}