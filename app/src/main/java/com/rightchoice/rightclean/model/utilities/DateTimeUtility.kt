package com.rightchoice.rightclean.model.utilities

import java.text.SimpleDateFormat
import java.util.*

object DateTimeUtility {

    object DateTimeFormat {
        const val FORMAT_FOR_TASKS_LISt = "yyyy-MM-dd"
        const val FORMAT_FOR_EVENTS_LIST_HEADING = "EEEE, dd MMMM"
    }

    fun getCurrentDate(dateFormat: String): String {
        val date = Calendar.getInstance().time
        val df = SimpleDateFormat(dateFormat, Locale.getDefault())
        return df.format(date)
    }

    fun getNextOrPreviousDate(dateFormat: String, dayToManipulate: Short): String {
        val calender = Calendar.getInstance()
        calender.add(Calendar.DAY_OF_YEAR, dayToManipulate.toInt())
        val df = SimpleDateFormat(dateFormat, Locale.getDefault())
        return df.format(calender.time)
    }

}