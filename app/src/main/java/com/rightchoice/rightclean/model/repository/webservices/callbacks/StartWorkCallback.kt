package com.rightchoice.rightclean.model.repository.webservices.callbacks

import com.rightchoice.rightclean.model.repository.webservices.pojo.response.StartWorkResponse

interface StartWorkCallback {
    fun onSuccess(response: StartWorkResponse?)
    fun onFail(throwable: Throwable?)
}