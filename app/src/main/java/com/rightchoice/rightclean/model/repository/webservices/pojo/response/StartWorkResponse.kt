package com.rightchoice.rightclean.model.repository.webservices.pojo.response

data class StartWorkResponse(
        var empid: String,
        var clientid: String,
        var id: String
): BaseResponseModel()
