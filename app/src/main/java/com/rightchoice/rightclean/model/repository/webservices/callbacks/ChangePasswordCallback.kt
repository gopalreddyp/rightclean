package com.rightchoice.rightclean.model.repository.webservices.callbacks

import com.rightchoice.rightclean.model.repository.webservices.pojo.response.ChangePasswordResponseModel

interface ChangePasswordCallback {
    fun onSuccess(response: ChangePasswordResponseModel?)
    fun onFail(throwable: Throwable?)
}