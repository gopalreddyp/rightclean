package com.rightchoice.rightclean.model.repository.webservices.pojo.requests

data class GetCompanyDetailsRequestModel (
        var id: String
) : BaseRequestModel()
