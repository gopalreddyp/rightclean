package com.rightchoice.rightclean.model.utilities

import com.google.gson.Gson
import com.rightchoice.rightclean.model.repository.shared_preferences.SharedPreferenceManager
import com.rightchoice.rightclean.model.repository.webservices.pojo.response.LoginResponseModel
import com.rightchoice.rightclean.utilities.PreferenceManager
import javax.inject.Inject

class UserSessionManager {

    @Inject
    lateinit var preferenceManager: PreferenceManager

    companion object {
        val instance = UserSessionManager()
    }

    private fun getLoginUserInfo(): LoginResponseModel {
        val loginResponseString = preferenceManager.getString(SharedPreferenceManager.SharedPrefKeys.KEY_LOG_IN_RESPONSE)
        return Gson().fromJson(loginResponseString,  LoginResponseModel::class.java)
    }

    fun getEmployeeId(): String {
        return getLoginUserInfo().data.userData.employeesid
    }
}