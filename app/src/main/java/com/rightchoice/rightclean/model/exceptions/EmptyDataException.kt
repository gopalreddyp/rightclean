package com.rightchoice.rightclean.model.exceptions

import com.techresources.clearmots.model.exceptions.BaseException

class EmptyDataException: BaseException() {

    override fun toString(): String {
        return "Data was empty"
    }
}