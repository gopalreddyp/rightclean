package com.rightchoice.rightclean.model.repository.webservices.callbacks

import com.rightchoice.rightclean.model.repository.webservices.pojo.response.ForgotPasswordResponseModel

interface ForgotPasswordCallback {
    fun onSuccess(response: ForgotPasswordResponseModel?)
    fun onFail(throwable: Throwable?)
}