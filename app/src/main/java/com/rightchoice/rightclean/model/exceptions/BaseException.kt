package com.techresources.clearmots.model.exceptions

abstract class BaseException : Exception() {
    override val message: String
        get() = "Base Exception"
}