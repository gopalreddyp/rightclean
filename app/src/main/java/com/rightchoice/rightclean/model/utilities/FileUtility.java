package com.rightchoice.rightclean.model.utilities;

import android.content.Context;
import android.os.Environment;

import java.io.File;

public class FileUtility {

    public static final File getPictureDirectory(Context context) {
        File mainPictureDirectory = new File(Environment.getExternalStorageDirectory(), "Profile Pictures");

        if (!mainPictureDirectory.exists()) {
            if (!mainPictureDirectory.mkdirs()) {
                return null;
            }
        }
        return mainPictureDirectory;
    }

   /* public static String getBackupDirectory() {
        if (backupDirectory == null) {
            File _externalStorageDirectory = Environment.getExternalStorageDirectory();
            if (_externalStorageDirectory == null) {
                return null;
            }

            try {
                backupDirectory = _externalStorageDirectory.getCanonicalPath();
            } catch (IOException e) {
                Log.e(TAG, "", e);
            }

            if (backupDirectory == null) {
                return null;
            }

            backupDirectory = toEndWithFileSeperator(backupDirectory) + "com.yocto.wenote" + File.separator + "backup" + File.separator;
        }

        return backupDirectory;
    }*/
}
