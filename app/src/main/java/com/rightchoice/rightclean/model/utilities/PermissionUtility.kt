package com.rightchoice.rightclean.model.utilities

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

object PermissionUtility {
    fun checkIfPermissionGranted(context: Context, permission: String): Boolean {
        return ContextCompat.checkSelfPermission(context,
                permission) == PackageManager.PERMISSION_GRANTED
    }

    fun requestPermission(activity: Activity, permission: String) {
        ActivityCompat.requestPermissions(activity, arrayOf(permission),
                AppConstants.PermissionsCode.LOCATION_CODE)
    }
}