package com.rightchoice.rightclean.model.utilities

import android.content.pm.PackageManager
import com.rightchoice.rightclean.JsonNetworkPackage.AppController

object AppUtility {

    @JvmStatic
    fun checkIfCameraAvailable(): Boolean {
        return AppController.instance.packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY) &&
                AppController.instance.packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA_FRONT)
    }
}