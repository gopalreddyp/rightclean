package com.techresources.clearmots.model.exceptions

class BundleKeyNotFound : BaseException() {
    override val message: String
        get() = "Requested key was not found in Bundle"
}