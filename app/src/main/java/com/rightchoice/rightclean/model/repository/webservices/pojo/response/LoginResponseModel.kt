package com.rightchoice.rightclean.model.repository.webservices.pojo.response

data class LoginResponseModel (
        var data: LoginData
) : BaseResponseModel()

data class LoginData(
        var name: String,
        var image: String,
        var userType: String,
        var userData: UserData
)

data class UserData(
        var employeesid: String,
        var email: String,
        var phone: String,
        var address: String,
        var regKey: String,
        var employeestatus: String,
)

