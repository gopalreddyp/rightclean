package com.rightchoice.rightclean.model.repository.webservices.pojo.response

data class GetSchedularDetailsResponseModel(
        var result: List<ScheduleDetails>
) : BaseResponseModel()

data class ScheduleDetails(
        var event_status: String,
        var activitytype: String,
        var activityid: String,
        var date_start: String,
        var due_date: String,
        var time_start: String,
        var time_end: String,
        var post_code: String,
        var city: String,
        var business_name: String,
        var clientid: String,
        var sendnotification: Int,
        var duration_hours: Int,
        var duration_minutes: Int,
        var eventstatus: String,
        var priority: String,
        var location: String,
        var notime: Int,
        var visibility: String,
        var country: String,
        var state: String,
        var street: String,
): BaseResponseModel()

