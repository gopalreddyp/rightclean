package com.rightchoice.rightclean.model.repository.webservices.pojo.response

import com.google.gson.annotations.SerializedName

open class BaseResponseModel () {
    @SerializedName("status")
    lateinit var status: String

    @SerializedName("message")
    lateinit var message: String

}

