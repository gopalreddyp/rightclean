package com.rightchoice.rightclean.model.repository.webservices.callbacks

import com.rightchoice.rightclean.model.repository.webservices.pojo.response.GetCompanyDetailsResponseModel

interface GetCompanyDetailsCallback {
    fun onSuccess(response: GetCompanyDetailsResponseModel?)
    fun onFail(throwable: Throwable?)
}