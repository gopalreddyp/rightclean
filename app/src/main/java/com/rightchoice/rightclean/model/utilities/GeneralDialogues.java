package com.rightchoice.rightclean.model.utilities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.rightchoice.rightclean.R;


public class GeneralDialogues {

    public static void show2ButtonsDialogue(Context context, final DialogueClickListener2 dialogueClickListener,
                                     String message, String textPositiveButton, String textNegativeButton) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton(textNegativeButton == null ? context.getString(R.string.yes): textPositiveButton, (dialog, id) -> dialogueClickListener.onPositiveButtonClicked(dialog))
                .setNegativeButton(textNegativeButton == null ? context.getString(R.string.no):textNegativeButton, (dialog, id) -> dialogueClickListener.onNegativeButtonClicked(dialog));
        AlertDialog alert = builder.create();
        alert.show();
    }

    public static void show1ButtonsDialogue(Context context, final DialogueClickListener1 dialogueClickListener,
                                     String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message)
                .setCancelable(false)
                .setTitle(R.string.title_warning)
                .setPositiveButton(context.getString(R.string.ok), (dialog, id) -> dialogueClickListener.onPositiveButtonClicked(dialog));
        AlertDialog alert = builder.create();
        alert.show();
    }

    public static void showCompanyIdDialogue(Activity activity, DialogueClickListenerForCompany listener) {
        View mView = activity.getLayoutInflater().inflate(R.layout.dialogue_company_id, null);
        EditText etCompanyId = mView.findViewById(R.id.compidedt);
        Button btnCancel = mView.findViewById(R.id.btn_cancel);
        Button btnOk = mView.findViewById(R.id.btn_okay);

        if (AppConstants.Switches.shouldEnableCredentials)
            etCompanyId.setText("800700");

        AlertDialog.Builder alert = new AlertDialog.Builder(activity);
        alert.setCancelable(false);
        alert.setView(mView);
        AlertDialog alertDialog = alert.create();
        alertDialog.setCanceledOnTouchOutside(false);

        btnCancel.setOnClickListener(v -> {
           listener.onNegativeButtonClicked(alertDialog);
        });
        btnOk.setOnClickListener(v -> {
            String companyId = etCompanyId.getText().toString();
            if (companyId.length() < AppConstants.ThreshHolds.COMPANY_ID) {
                etCompanyId.setError(activity.getString(R.string.error_enter_valid_company_id));
            } else {
                listener.onPositiveButtonClicked(alertDialog, companyId);
            }
        });
        alertDialog.show();
    }

    public interface DialogueClickListenerForCompany {
        void onPositiveButtonClicked(DialogInterface dialogInterface, String companyId);
        void onNegativeButtonClicked(DialogInterface dialogInterface);
    }

    public interface DialogueClickListener2 {
        void onPositiveButtonClicked(DialogInterface dialogInterface);
        void onNegativeButtonClicked(DialogInterface dialogInterface);
    }

    public interface DialogueClickListener1 {
        void onPositiveButtonClicked(DialogInterface dialogInterface);
    }
}
