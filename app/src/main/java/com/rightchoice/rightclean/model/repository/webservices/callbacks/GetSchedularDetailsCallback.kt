package com.rightchoice.rightclean.model.repository.webservices.callbacks

import com.rightchoice.rightclean.model.repository.webservices.pojo.response.GetSchedularDetailsResponseModel

interface GetSchedularDetailsCallback {
    fun onSuccess(response: GetSchedularDetailsResponseModel?)
    fun onFail(throwable: Throwable?)
}