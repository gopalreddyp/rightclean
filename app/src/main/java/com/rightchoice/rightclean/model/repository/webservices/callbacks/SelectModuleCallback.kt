package com.rightchoice.rightclean.model.repository.webservices.callbacks

import com.rightchoice.rightclean.model.repository.webservices.pojo.response.SelectModuleResponseModel

interface SelectModuleCallback {
    fun onSuccess(response:SelectModuleResponseModel?)
    fun onFail(throwable: Throwable?)
}