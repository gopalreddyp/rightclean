package com.rightchoice.rightclean.model.repository.webservices.web_apis

import com.rightchoice.rightclean.model.repository.webservices.pojo.response.*
import com.rightchoice.rightclean.model.utilities.AppConstants
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

interface WebApi {

    @POST(AppConstants.APIMethods.SIGN_IN)
    fun attemptLogin(@Body request: RequestBody): Call<LoginResponseModel?>?

    @POST(AppConstants.APIMethods.GET_COMPANY_DETAILS)
    fun getCompanyDetails(@Body request: RequestBody): Call<GetCompanyDetailsResponseModel?>?

    @POST(AppConstants.APIMethods.SELECT_MODULE)
    fun getSelectedModule(@Body request: RequestBody): Call<SelectModuleResponseModel?>?

    @POST(ApiMethods.GET_CLIENTS_DETAILS)
    fun getClientsDetails(@Body request: RequestBody): Call<GetClientsDetailsResponseModel?>?

    @POST(ApiMethods.GET_PENDING_WORK)
    fun getPendingWork(@Body request: RequestBody): Call<GetPendingWorkResponseModel?>?

    @POST(ApiMethods.EMPLOYEE_TRACKING)
    fun sendEmployeeLocation(@Body request: RequestBody): Call<EmployeeLocationResponseModel?>?

    @POST(AppConstants.APIMethods.GET_SCHEDULAR_DETAILS)
    fun getSchedularDetails(@Body request: RequestBody): Call<GetSchedularDetailsResponseModel?>

    @POST(AppConstants.APIMethods.START_WORK)
    fun startWork(@Body request: RequestBody): Call<StartWorkResponse?>

    @POST(AppConstants.APIMethods.FORGOT_PASSWORD)
    fun getForgotPasswordEmail(@Body request: RequestBody): Call<ForgotPasswordResponseModel?>?

    @POST(AppConstants.APIMethods.CHANGE_PASSWORD)
    fun getChangePasswordDetails(@Body request: RequestBody): Call<ChangePasswordResponseModel?>?
    @Multipart
    @POST(ApiMethods.END_WORK)
    fun upload(
            @Part("id") id: RequestBody?,
            @Part("punchout") punchout: RequestBody?,
            @Part("empid") empid: RequestBody?,
            @Part("clientid") clientid: RequestBody?,
            @Part("servicelist") servicelist: RequestBody?,
            @Part imageFile: MultipartBody.Part?
    ): Call<EndWorkResponseModel?>?

    object ApiMethods {
        const val GET_CLIENTS_DETAILS = "https://dev-reception.rightchoice.io/reception/Free/200826165138QueenslandCleaningClub/include/Webservices/RightCleanClients.php"
        const val GET_PENDING_WORK = "https://dev-reception.rightchoice.io/reception/Free/200826165138QueenslandCleaningClub/include/Webservices/RightCleanPendingwork.php"
        const val RIGHT_CLEAN_LINK = "https://rightchoice.io/rightclean/"
        const val START_WORK = "https://dev-reception.rightchoice.io/reception/Free/200826165138QueenslandCleaningClub/include/Webservices/RightCleanStartwork.php"
        const val RIGHT_CLEAN_SERVICES = "https://dev-reception.rightchoice.io/reception/Free/200826165138QueenslandCleaningClub/include/Webservices/RightCleanServices.php"
        const val END_WORK = "https://dev-reception.rightchoice.io/reception/Free/200826165138QueenslandCleaningClub/include/Webservices/RightCleanStopwork.php"
        const val EMPLOYEE_TRACKING = "https://dev-reception.rightchoice.io/reception/Free/200826165138QueenslandCleaningClub/api/trackingapi.php"
    }

    object Status {
        const val SUCCESS = "success"
        const val ERROR = "error"
    }
}