package com.rightchoice.rightclean.model.utilities

import android.Manifest

object AppConstants {
   // const val BASE_URL = "https://rightchoice.io/rightclean/"
    const val BASE_URL = "https://newcrm.rightchoice.io/cleanApi/"
    const val BASE_URL_MAIN_ROUTE = "https://newcrm.rightchoice.io/"

    object APIMethods {
        const val GET_COMPANY_DETAILS = "getcompanydetails.php"
        const val GET_SCHEDULAR_DETAILS = "RightCleanscheduler.php"
        const val START_WORK = "RightCleanStartwork.php"
        const val SIGN_IN = "RightCleanSignin.php"
        const val FORGOT_PASSWORD = BASE_URL+"forgotPasswordApi.php"
        const val CHANGE_PASSWORD = BASE_URL+"/ChangePassword.php"
        const val SELECT_MODULE = BASE_URL_MAIN_ROUTE + "emailApi/GetModuleName.php"
    }

    object ThreshHolds {
        const val COMPANY_ID = 6
    }

    object APIKeys {
        const val IMAGE = "image"
        const val PUNCH_IN = "punchin"
        const val PUNCH_OUT = "punchout"
        const val ID = "id"
        const val USER_NAME = "username"
        const val PASSWORD = "password"
        const val EMP_ID = "empid"
        const val Activity_ID = "activityid"
        const val EMP_ID_LOCATION = "employee_id"
        const val CLIENT_ID = "clientid"
        const val CLIENT_ID_LOCATION = "client_id"
        const val SERVICE_LIST = "servicelist"
        const val LATITUDE = "lat"
        const val LONGITUDE = "lng"
        const val DATE_FROM = "dateFrom"
        const val DATE_TO = "dateTo"
        const val TIME_FROM = "timeFrom"
        const val TIME_TO = "timeTo"
        const val RESULT = "result"
        const val EMAIL_ID = "emailId"
        const val EMAIL = "email"
        const val CONFIRM_PASSWORD = "confirm_password"
    }

    object Switches {
        const val shouldEnableCredentials = false
    }

    object BundleKeys {
        const val CLIENT_ID = "client_id"
        const val TRANSACTION_ID = "transaction_id"
    }

    object PermissionsCode {
        const val LOCATION_CODE = 1_000
    }

    object RequestCode {
        const val LOCATION_REQUEST_CODE = 2_000
    }

    object Permissions {
        const val LOCATION_PERMISSION = Manifest.permission.ACCESS_FINE_LOCATION
    }

    object ScheduleWorkStatus {
        const val NOT_STARTED = "Not Started"
        const val IN_PROGRESS = "In Progress"
        const val DONE = "Done"
    }
}