package com.rightchoice.rightclean.model.repository.shared_preferences

import android.app.Application
import android.content.Context
import android.content.SharedPreferences

class SharedPreferenceManager(application: Application) {

    object SharedPrefKeys {
        const val KEY_IS_USER_LOGGED_IN = "key_is_user_logged_in"
        const val KEY_COMPANY_LOGO = "key_company_logo"
        const val KEY_COMPANY_INFO = "key_company_info"
        const val KEY_LOG_IN_RESPONSE = "key_login_response"
    }

    private val mSharedPreferences: SharedPreferences = application.getSharedPreferences(application.packageName, Context.MODE_PRIVATE)

    fun clearPreferences(){
        mSharedPreferences.edit().clear().apply()
    }

    /*
     * @param key is unique identity to stored data
     * @param value is the data you want to store against key
     * */
    fun setString(key: String, value: String) {
        mSharedPreferences.edit().putString(key, value).apply()
    }

    /*
     * @param key This parameter is used as a key store to fetch the data against it.
     * @return  This method returns require String value if the key not found then it returns 'null' as
     * default value
     * */
    fun getString(key: String): String? {
        return mSharedPreferences.getString(key, null)
    }

    /*
    * @param key is unique identity to stored data
    * @param value is the data you want to store against key
    * */
    fun setInt(key: String?, value: Int) {
        mSharedPreferences.edit().putInt(key, value).apply()
    }

    /*
    * @param key This parameter is used as a key store to fetch the data against it.
    * @return  This method returns require Integer value if the key not found then it returns '-1' as
    * default value
    * */
    fun getInt(key: String?): Int {
        return mSharedPreferences.getInt(key, -1)
    }

    /*
     * @param key is unique identity to stored data
     * @param value is the data you want to store against key
     * */
    fun setBoolean(key: String?, value: Boolean) {
        mSharedPreferences.edit().putBoolean(key, value).apply()
    }

    /*
     * @param key This parameter is used as a key store to fetch the data against it.
     * @return  This method returns require boolean value if the key not found then it returns 'false' as
     * default value
     * */
    fun getBoolean(key: String?): Boolean {
        return mSharedPreferences.getBoolean(key, false)
    }

}