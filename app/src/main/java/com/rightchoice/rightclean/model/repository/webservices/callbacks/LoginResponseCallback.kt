package com.rightchoice.rightclean.model.repository.webservices.callbacks

import com.rightchoice.rightclean.model.repository.webservices.pojo.response.LoginResponseModel

interface LoginResponseCallback {
    fun onSuccess(response: LoginResponseModel?)
    fun onFail(throwable: Throwable?)
}