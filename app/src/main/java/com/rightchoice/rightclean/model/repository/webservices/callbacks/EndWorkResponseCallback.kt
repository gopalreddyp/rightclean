package com.rightchoice.rightclean.model.repository.webservices.callbacks

import com.rightchoice.rightclean.model.repository.webservices.pojo.response.EndWorkResponseModel

interface EndWorkResponseCallback {
    fun onSuccess(response: EndWorkResponseModel?)
    fun onFail(throwable: Throwable?)
}