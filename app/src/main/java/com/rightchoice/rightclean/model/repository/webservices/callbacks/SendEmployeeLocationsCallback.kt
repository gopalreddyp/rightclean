package com.rightchoice.rightclean.model.repository.webservices.callbacks

import com.rightchoice.rightclean.model.repository.webservices.pojo.response.EmployeeLocationResponseModel

interface SendEmployeeLocationsCallback {

    fun onSuccess(response: EmployeeLocationResponseModel?)
    fun onFail(throwable: Throwable?)
}