package com.rightchoice.rightclean.model.utilities

import android.view.Gravity
import android.widget.Toast
import com.rightchoice.rightclean.JsonNetworkPackage.AppController

object ToastUtility {

    @JvmStatic
    fun showSmallDurationToast(message: String?) {
        Toast.makeText(AppController.instance, message, Toast.LENGTH_SHORT).show()
    }


    fun showLongDurationToast(message: String?) {
        Toast.makeText(AppController.instance, message, Toast.LENGTH_LONG).show()
    }

    @JvmStatic
    fun showToast(message: String?) {
        val toast = Toast.makeText(AppController.instance, message, Toast.LENGTH_LONG)
        toast.setGravity(Gravity.CENTER, 0, 0)
        toast.show()
    }
}