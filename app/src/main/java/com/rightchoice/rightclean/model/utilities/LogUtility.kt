package com.rightchoice.rightclean.model.utilities

import android.util.Log

/*
* This class contains Log related methods
* */
object LogUtility {
    private const val SHOULD_SHOW_DEBUG_LOG = true
    private const val SHOULD_SHOW_ERROR_LOG = true

    /*
    * this method prints out the LOG INFO MESSAGES
    * */
    @JvmStatic
    fun logInfo(tag: String?, message: String) {
        if (SHOULD_SHOW_DEBUG_LOG) Log.d(tag, message)
    }

    /*
     * this method prints out the LOG INFO MESSAGES along with exception
     * */
    fun logInfo(tag: String?, message: String, exception: Exception) {
        if (SHOULD_SHOW_DEBUG_LOG) Log.d(tag, message + "Exception is " + exception.message)
    }

    /*
     * this method prints out the LOG ERROR MESSAGES
     * */
    fun logError(tag: String?, message: String) {
        if (SHOULD_SHOW_ERROR_LOG) Log.d(tag, message)
    }

    /*
     * this method prints out the LOG ERROR MESSAGES along with exception
     * */
    fun logError(tag: String?, message: String, exception: Exception) {
        if (SHOULD_SHOW_ERROR_LOG) Log.d(tag, message + "Exception is " + exception.message)
    }
}