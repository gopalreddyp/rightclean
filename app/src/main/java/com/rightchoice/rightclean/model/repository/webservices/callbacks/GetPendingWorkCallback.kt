package com.rightchoice.rightclean.model.repository.webservices.callbacks

import com.rightchoice.rightclean.model.repository.webservices.pojo.response.GetPendingWorkResponseModel

interface GetPendingWorkCallback {
    fun onSuccess(response: GetPendingWorkResponseModel?)
    fun onFail(throwable: Throwable?)
}