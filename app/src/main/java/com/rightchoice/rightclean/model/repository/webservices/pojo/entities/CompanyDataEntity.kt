package com.rightchoice.rightclean.model.repository.webservices.pojo.entities

data class CompanyDataEntity(
        var id: String,
        var url: String,
        var name: String,
        var logo: String
)