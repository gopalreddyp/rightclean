package com.techresources.clearmots.model.exceptions

import android.content.Context
import com.rightchoice.rightclean.R

class NoInternetConnectionException(private val mContext: Context) : BaseException() {

    override val message: String
        get() = mContext.getString(R.string.error_no_internet_connection)
}