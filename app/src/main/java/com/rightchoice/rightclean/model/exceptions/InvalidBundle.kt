package com.techresources.clearmots.model.exceptions

class InvalidBundle : BaseException() {
    override val message: String
        get() = "Bundle was null"
}