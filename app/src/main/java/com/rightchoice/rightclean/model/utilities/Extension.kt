package com.rightchoice.rightclean.model.utilities

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.text.TextUtils
import android.view.Gravity
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import android.widget.VideoView
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.rightchoice.rightclean.model.exceptions.EmptyDataException

class Extension {

    val Any.TAG: String
        get() {
            val tag = javaClass.simpleName
            return if (tag.length <= 23) tag else tag.substring(0, 23)
        }

    fun String.isEmailValid(): Boolean {
        return !TextUtils.isEmpty(this) && android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches()
    }

    fun Context.toast(text: CharSequence, duration: Int = Toast.LENGTH_SHORT): Toast {
        return Toast.makeText(this, text, duration).apply {
            setGravity(Gravity.CENTER, 0, 0)
            show()
        }
    }

    fun Uri?.openInBrowser(context: Context) {
        this ?: return // Do nothing if uri is null

        var url = this.toString()

        if (!url.startsWith("http://") && !url.startsWith("https://"))
            url = "http://$url"
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        ContextCompat.startActivity(context, browserIntent, null)
    }

    fun Context.dpToPx(dp: Int): Int {
        return (dp * resources.displayMetrics.density).toInt()
    }

    fun Context.pxToDp(px: Int): Int {
        return (px / resources.displayMetrics.density).toInt()
    }



    fun TextView.setDrawableTop(drawableID: Int) {
        setCompoundDrawablesWithIntrinsicBounds(0, drawableID, 0, 0)
    }

    fun ImageView.loadImage(iv: ImageView, url: String) {
        if (url.isNotBlank()) {
            Glide.with(iv.context)
                    .load(url)
                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                    // .placeholder(R.drawable.ic_profile_placeholder)
                    .into(iv)
        } else {
            throw EmptyDataException()
        }
    }

    fun VideoView.startVideo(uri: Uri) {
        setVideoURI(uri)
        start()
    }

}