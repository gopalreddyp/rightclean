package com.rightchoice.rightclean.model.repository.webservices.pojo.response

data class ChangePasswordResponseModel (
        var email: String,
        var password: String,
        var confirm_password: String
        ) : BaseResponseModel()