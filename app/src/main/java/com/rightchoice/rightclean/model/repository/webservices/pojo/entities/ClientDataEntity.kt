package com.rightchoice.rightclean.model.repository.webservices.pojo.entities

data class ClientDataEntity(
        var clientsData: MutableList<ClientData>
)

data class ClientData(
        var clientsno: String,
        var clients_tks_clientname: String,
        var assigned_user_id: String,
        var createdtime: String,
        var modifiedtime: String,
        var source: String,
        var starred: String,
        var tags: String,
        var email: String,
        var phone: String,
        var address: String,
        var frequency: String,
        var day: String,
        var hours: String,
        var access: String,
        var products: String,
        var time: String,
        var description: String,
        var id:String
)