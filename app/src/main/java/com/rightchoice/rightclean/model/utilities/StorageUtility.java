package com.rightchoice.rightclean.model.utilities;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

public class StorageUtility {

  /*  public static File saveImage(Context context, Bitmap myBitmap) throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        String timeStamp = new SimpleDateFormat(DateTimeUtility.FORMAT_Y_M_D_H_M_S).format(new Date());

        File imageFile = new File(FileUtility.getPictureDirectory(context) + File.separator +
                "IMG_" + timeStamp + ".jpg");

        FileOutputStream fo = new FileOutputStream(imageFile);
        fo.write(bytes.toByteArray());
        *//*MediaScannerConnection.scanFile(getContext(),
                new String[]{file1.getPath()},
                new String[]{"image/jpeg"}, null);*//*
        fo.close();

        return imageFile;
    }*/

    public static File getFileFromPath() throws Exception {
        File f = new File(Environment.getExternalStorageDirectory().toString());


        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        Bitmap bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(),
                bitmapOptions);
        String path = android.os.Environment
                .getExternalStorageDirectory()
                + File.separator
                + "Phoenix" + File.separator + "default";

        File actualBitmapFile = new File(path, System.currentTimeMillis() + ".jpg");
        if (!actualBitmapFile.exists()) {
            actualBitmapFile.createNewFile();
        }
        OutputStream outFile = new FileOutputStream(actualBitmapFile);
        bitmap.compress(Bitmap.CompressFormat.JPEG, 85, outFile);
        outFile.flush();
        outFile.close();
        return actualBitmapFile;
    }

    public static File getFileFromBitmap (Context context, Bitmap bitmap) throws Exception {
        String path = android.os.Environment
                .getExternalStorageDirectory()
                + File.separator
                + "Phoenix" + File.separator + "default";

        File actualBitmapFile = new File(path, System.currentTimeMillis() + ".jpg");
        if (!actualBitmapFile.exists()) {
            actualBitmapFile.createNewFile();
        }

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        FileOutputStream fo = new FileOutputStream(actualBitmapFile);
        fo.write(bytes.toByteArray());

        fo.flush();
        fo.close();
        return actualBitmapFile;
    }
}
