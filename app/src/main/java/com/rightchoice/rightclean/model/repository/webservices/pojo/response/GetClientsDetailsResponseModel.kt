package com.rightchoice.rightclean.model.repository.webservices.pojo.response

import com.rightchoice.rightclean.model.repository.webservices.pojo.entities.ClientDataEntity

data class GetClientsDetailsResponseModel(
        var data: ClientDataEntity
) : BaseResponseModel()