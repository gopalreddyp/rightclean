package com.rightchoice.rightclean.model.repository.webservices.pojo.response

data class GetCompanyDetailsResponseModel(
        var result: CompanyResult
) : BaseResponseModel()

data class CompanyResult(
        var id: String,
        var url: String,
        var name: String,
        var logo: String,
)
