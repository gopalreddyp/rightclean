package com.rightchoice.rightclean.model.repository.webservices.pojo.entities

data class LoginDataEntity (
    var name: String,
    var image: String,
    var userType: String,
    var userData: UserDataEntity,
    var companyData: CompanyDataEntity,
    var workData: MutableList<WorkDataEntity>?,
    var clientsData:MutableList<ClientDataEntity>?
)