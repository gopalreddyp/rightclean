package com.rightchoice.rightclean.model.repository.webservices.pojo.response

data class SelectModuleResponseModel(
        var result: List<String>
) : BaseResponseModel()



