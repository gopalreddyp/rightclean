package com.rightchoice.rightclean.CircularProgressdialog;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.rightchoice.rightclean.R;

import java.util.Objects;

public class CircularProgressDialogue extends DialogFragment {

    private static ProgressDialog progressDialog;
    private static AlertDialog alertDialog;


    public static void showDialog(Context context, String message, String title) {

        if (progressDialog != null) {

            if (progressDialog.isShowing()) { //check if dialog is showing.

                //get the Context object that was used to great the dialog
                context = ((ContextWrapper) progressDialog.getContext()).getBaseContext();

                //if the Context used here was an activity AND it hasn't been finished or destroyed
                //then dismiss it
                if (context instanceof Activity) {
                    if (!((Activity) context).isFinishing() && !((Activity) context).isDestroyed())
                        progressDialog.dismiss();
                } else //if the Context used wasnt an Activity, then dismiss it too
                    progressDialog.dismiss();
            }
            progressDialog = null;

        }
        int style;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            style = R.style.Theme_AppCompat_Light_Dialog;
        } else {
            //noinspectiondeprecation
            style = R.style.Theme_AppCompat_Light_Dialog;
        }
        progressDialog = new ProgressDialog(context, style);
        progressDialog.setMessage(message);
        progressDialog.setTitle(title);
        Objects.requireNonNull(progressDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        progressDialog.setCancelable(false);
        progressDialog.show();

    }

    public static void dismissdialog() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }

    }

    public static void showAlertDialgo(Context context, String message, String title) {
        alertDialog = new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Continue with delete operation

                        if (alertDialog != null && alertDialog.isShowing()) {
                            alertDialog.dismiss();

                        }


                    }
                }).show();

    }


}
