package com.rightchoice.rightclean.models

data class HomeModelClass(
     val title:String,
     val homeImages:Int
)