package com.rightchoice.rightclean.models

data class CompletedTasksModelClass(
     val taskNumber:String,
     val taskTitle:String,
     val taskDate : String,
     val taskImage:Int
)