package com.rightchoice.rightclean.models

data class SchedularModelClass (
        val TasksNumber:String,
        val Category:String,
        val Date:String,
        val Timing:String,
        val TotalHours:String
        )


