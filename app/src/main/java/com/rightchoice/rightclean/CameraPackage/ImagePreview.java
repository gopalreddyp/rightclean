package com.rightchoice.rightclean.CameraPackage;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.FaceDetector;
import com.google.android.gms.vision.face.Landmark;
import com.google.android.material.snackbar.Snackbar;
import com.rightchoice.rightclean.CircularProgressdialog.CircularProgressDialogue;
import com.rightchoice.rightclean.DashboardPackage.DashboardActivity;
import com.rightchoice.rightclean.JsonNetworkPackage.MultipartRequest;
import com.rightchoice.rightclean.R;
import com.rightchoice.rightclean.StartCameraPackage.StartCamera;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ImagePreview extends AppCompatActivity {

    private static final String TAG = ImagePreview.class.getSimpleName();
    ImageView person_image;
    String stingimage;
    Bitmap b;
    Bundle bundle;
    boolean isSmiling;
    float rightEye;
    float leftEye;
    Canvas canvas;
    final double SMILING_THRESHOLD = 0.2;
    final double WINK_THRESHOLD = 0.4;
    private FaceDetector detector;
    String lineEnd = "rn";
    String twoHyphens = "–";
    String boundary = "*****";
    private byte[] multipartBody;
    private String mimeType;
    private Bitmap bmp;
    Button register, cancel;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    SharedPreferences sharedpreferences;
    String id,devicecount,url;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_preview);


        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        Log.i(TAG, TAG);
        person_image = findViewById(R.id.person_image);
        register = findViewById(R.id.register);
        cancel = findViewById(R.id.cancel);

        sharedpreferences = getSharedPreferences("LoginCredentials", Context.MODE_PRIVATE);
        id = sharedpreferences.getString("id","");
        url = sharedpreferences.getString("url","");
        devicecount = sharedpreferences.getString("devicecount","");
        Log.i(TAG,url);



        detector = new FaceDetector.Builder(getApplicationContext())
                .setTrackingEnabled(false)
                .setLandmarkType(FaceDetector.ALL_LANDMARKS)
                .setClassificationType(FaceDetector.ALL_CLASSIFICATIONS)
                .setProminentFaceOnly(true)
                .build();

        if (getIntent().hasExtra("image")) {
            b = BitmapFactory.decodeByteArray(
                    getIntent().getByteArrayExtra("image"), 0, Objects.requireNonNull(getIntent().getByteArrayExtra("image")).length);
            ProcessImage(b);
            person_image.setImageBitmap(b);
        }

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                b.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                final byte[] byteArray = stream.toByteArray();
                Intent in1 = new Intent(ImagePreview.this, Admin_confirm.class);
                in1.putExtra("image", byteArray);
                Log.i(TAG,"Image Ready!");
                startActivity(in1);
                finish();


            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent intent = new Intent(ImagePreview.this, StartCamera.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();

            }
        });


        preferences = getApplicationContext().getSharedPreferences("ExecutiveData", MODE_PRIVATE);
        editor = preferences.edit();



    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.cancel_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.cancel) {
            CircularProgressDialogue.showDialog(ImagePreview.this, "", "");
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    CircularProgressDialogue.dismissdialog();
                    Intent intent = new Intent(ImagePreview.this, StartCamera.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
            }, 1000);


            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed(){
        super.onBackPressed();
        Intent i=new Intent(ImagePreview.this, StartCamera.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
    }



    private void ProcessImage(final Bitmap b) {

        if (detector.isOperational() && b != null) {
            Bitmap editedBitmap = Bitmap.createBitmap(b.getWidth(), b
                    .getHeight(), b.getConfig());
            float scale = getResources().getDisplayMetrics().density;
            Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
            paint.setColor(Color.GREEN);
            paint.setTextSize((int) (16 * scale));
            paint.setShadowLayer(1f, 0f, 1f, Color.WHITE);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeWidth(6f);
            Canvas canvas = new Canvas(editedBitmap);
            canvas.drawBitmap(b, 0, 0, paint);
            Frame frame = new Frame.Builder().setBitmap(b).build();
            SparseArray<Face> faces = detector.detect(frame);
            // txtSampleDesc.setText(null);
            for (int index = 0; index < faces.size(); ++index) {
                Face face = faces.valueAt(index);
                canvas.drawRect(
                        face.getPosition().x,
                        face.getPosition().y,
                        face.getPosition().x + face.getWidth(),
                        face.getPosition().y + face.getHeight(), paint);

/*
            canvas.drawText("Face " + (index + 1), face.getPosition().x + face.getWidth(), face.getPosition().y + face.getHeight(), paint);

            txtSampleDesc.setText(txtSampleDesc.getText() + "FACE " + (index + 1) + "\n");
            txtSampleDesc.setText(txtSampleDesc.getText() + "Smile probability:" + " " + face.getIsSmilingProbability() + "\n");
            txtSampleDesc.setText(txtSampleDesc.getText() + "Left Eye Is Open Probability: " + " " + face.getIsLeftEyeOpenProbability() + "\n");
            txtSampleDesc.setText(txtSampleDesc.getText() + "Right Eye Is Open Probability: " + " " + face.getIsRightEyeOpenProbability() + "\n\n");
*/


                Log.i(TAG, "FACE " + (index + 1) + "\n");
                Log.i(TAG, "Smile probability:" + " " + face.getIsSmilingProbability() + "\n");
                Log.i(TAG, "Left Eye Is Open Probability: " + " " + face.getIsLeftEyeOpenProbability() + "\n");
                Log.i(TAG, "Right Eye Is Open Probability: " + " " + face.getIsRightEyeOpenProbability() + "\n\n");

                rightEye = face.getIsRightEyeOpenProbability();
                leftEye = face.getIsLeftEyeOpenProbability();
                isSmiling = face.getIsSmilingProbability() > SMILING_THRESHOLD;

                for (Landmark landmark : face.getLandmarks()) {
                    int cx = (int) (landmark.getPosition().x);
                    int cy = (int) (landmark.getPosition().y);
                    PointF leftEyePoint = null;
                    PointF rightEyePoint = null;
                    PointF leftmouth = null;
                    PointF rightmouth = null;

                    switch (landmark.getType()) {
                        case Landmark.LEFT_EYE:
                            leftEyePoint = landmark.getPosition();
                            Log.i(TAG, "left eye" + leftEyePoint.toString());
                            //drawPoint(canvas, landmark.getPosition());
                            break;
                        case Landmark.RIGHT_EYE:
                            rightEyePoint = landmark.getPosition();
                            Log.i(TAG, "rite eye" + rightEyePoint.toString());
                            //drawPoint(canvas, landmark.getPosition());
                            break;
                        case Landmark.BOTTOM_MOUTH:
                            //drawPoint(canvas, landmark.getPosition());
                            break;
                        case Landmark.LEFT_MOUTH:
                            leftmouth = landmark.getPosition();
                            Log.i(TAG, "leftmouth" + leftmouth.toString());
                            //drawPoint(canvas, landmark.getPosition());
                            break;
                        case Landmark.RIGHT_MOUTH:
                            rightmouth = landmark.getPosition();
                            Log.i(TAG, "rite mouth" + rightmouth.toString());
                            //drawPoint(canvas, landmark.getPosition());
                            break;
                        case Landmark.NOSE_BASE:
                            //drawPoint(canvas, landmark.getPosition());
                            break;
                        case Landmark.LEFT_CHEEK:
                            //drawPoint(canvas, landmark.getPosition());
                            break;
                        case Landmark.RIGHT_CHEEK:
                            //drawPoint(canvas, landmark.getPosition());
                            break;
                        case Landmark.LEFT_EAR:
                            //drawPoint(canvas, landmark.getPosition());
                            break;
                        case Landmark.LEFT_EAR_TIP:
                            //drawPoint(canvas, landmark.getPosition());
                            break;
                        case Landmark.RIGHT_EAR:
                            //drawPoint(canvas, landmark.getPosition());
                            break;
                        case Landmark.RIGHT_EAR_TIP:
                            //drawPoint(canvas, landmark.getPosition());
                            break;
                    }
                    if (leftEyePoint == null && rightEyePoint == null && leftmouth == null && rightmouth == null) {
                        Log.i(TAG, "eyes not found");

                    } else {
                        Log.i(TAG, "eyes  found");

                    }

//                canvas.drawCircle(cx, cy, 1, paint);
                }
            }

            if (faces.size() == 1) {


                Log.i(TAG, "Checking started ");
                if (leftEye > WINK_THRESHOLD || rightEye > WINK_THRESHOLD && isSmiling) {
                    Log.i(TAG, " yes Full face  Crossed ");
                    person_image.setImageBitmap(editedBitmap);
                    Log.i(TAG, "No of Faces Detected: " + " " + String.valueOf(faces.size()));
                    Imageuploadtoserver(editedBitmap);

                } else {
                    Log.i(TAG, "Back to 1");
                    //getIntent().removeExtra("image");
                    getIntent().setAction("");
                    setIntent(null);
                    CircularProgressDialogue.showDialog(ImagePreview.this, "", "");
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            CircularProgressDialogue.dismissdialog();
                            Intent intent = new Intent(ImagePreview.this, AccessCam.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            Snackbar.make(findViewById(android.R.id.content), "Face Not Detected Properlly!", Snackbar.LENGTH_LONG).show();

                        }
                    }, 2000);

                }

            } else {


                Log.i(TAG,"Scan Failed: Found nothing to scan");
                Log.i(TAG,"No Face Found");
                Log.i(TAG,"Back to 1");
                //getIntent().removeExtra("image");
                getIntent().setAction("");
                setIntent(null);
                new AlertDialog.Builder(ImagePreview.this)
                        .setTitle("Alert")
                        .setMessage("RightClean will Not allow Multiple Faces")
                        .setCancelable(false)
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(ImagePreview.this,AccessCam.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }
                        }).show();
            }

        } else {
            Log.i(TAG, "Could not set up the detector!");
        }
    }



    private void Imageuploadtoserver(final Bitmap editedBitmap) { //https://internal.my360crm.com/website/190507150303DEMOCRM/include/Webservices/AWS.php   http://13.236.116.157:8000/post/

        CircularProgressDialogue.showDialog(ImagePreview.this,"","");
        MultipartRequest multipartRequest = new MultipartRequest(Request.Method.POST,url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                CircularProgressDialogue.dismissdialog();

                try {

                    JSONObject obj = new JSONObject(new String(response.data));
                    //Toast.makeText(getApplicationContext(), obj.getString("status"), Toast.LENGTH_SHORT).show();

                    String Status = obj.getString("status");
                    Log.i(TAG, obj.toString());
                    if (Status.equals("error")) {
                        String Result = obj.getString("result");
                        Log.i(TAG, Result);
                        switch (Result) {
                            case "false":
                                register.setVisibility(View.VISIBLE);
                                cancel.setVisibility(View.VISIBLE);

                                break;
                            case "deleted user":
                                Snackbar.make(findViewById(android.R.id.content), Result + " Please Contact Admin", Snackbar.LENGTH_LONG).show();
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Intent intent = new Intent(ImagePreview.this, StartCamera.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                    }
                                }, 2000);


                                break;
                            case "inactive user":
                                Snackbar.make(findViewById(android.R.id.content), Result + " Please Contact Admin", Snackbar.LENGTH_LONG).show();
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Intent intent = new Intent(ImagePreview.this, StartCamera.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                    }
                                }, 2000);


                                break;
                            case "please recognize your image":
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Intent intent = new Intent(ImagePreview.this, StartCamera.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                    }
                                }, 1000);

                                break;
                            case "Image not found":
                                Snackbar.make(findViewById(android.R.id.content), "Image not found Contact Admin", Snackbar.LENGTH_LONG).show();
                                CircularProgressDialogue.showDialog(ImagePreview.this, "", "");
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        CircularProgressDialogue.dismissdialog();
                                        Intent intent = new Intent(ImagePreview.this, StartCamera.class);
                                        startActivity(intent);
                                        finish();

                                    }
                                }, 2000);

                                break;
                        }

                    } else if (Status.equals("success")) {

                        final String id = obj.getString("id");
                        final String emp = obj.getString("Emp");
                        final String url = obj.getString("url");

                        editor.putString("status", Status);
                        editor.putString("id", id);
                        editor.putString("name", emp);
                        editor.putString("url", url);
                        editor.commit();

                        Log.i(TAG, "id" + id);
                        Log.i(TAG, "Emp" + emp);
                        Log.i(TAG, "url" + url);

                        Snackbar.make(findViewById(android.R.id.content),emp+" Welcome to RightClean!",Snackbar.LENGTH_LONG).show();

                        CircularProgressDialogue.showDialog(ImagePreview.this,"","");
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                CircularProgressDialogue.dismissdialog();
                                UPloadTonextScreen(b, id, emp, url);

                            }
                        },2000);


                    }



                } catch (JSONException e) {
                    e.printStackTrace();

                }


            }
        }, new Response.ErrorListener() {
            @Override //Something Went Wrong We are Working...
            public void onErrorResponse(VolleyError error) {
                CircularProgressDialogue.dismissdialog();
                Log.e(TAG,"Something error "+error.getMessage());

                try {
                    NetworkResponse response = error.networkResponse;
                    VolleyError responseError = new VolleyError( new String(error.networkResponse.data));
                    Log.i(TAG,"res" +String.valueOf(responseError));
                    String errorMsg = "";

                    String errorString = new String(response.data);
                    Log.i("log error", errorString);
                    int mstatuscode = response.statusCode;
                    Log.i(TAG, String.valueOf(mstatuscode));
                    if(mstatuscode == HttpURLConnection.HTTP_NOT_FOUND)
                    {
                        Snackbar.make(findViewById(android.R.id.content), mstatuscode +" Server Not Found",Snackbar.LENGTH_LONG).show();
                    } else if(mstatuscode == HttpURLConnection.HTTP_INTERNAL_ERROR)
                    {
                        Snackbar.make(findViewById(android.R.id.content), mstatuscode +" Internal Server Error",Snackbar.LENGTH_LONG).show();

                    }else  if(response.statusCode == 401)
                    {
                        Snackbar.make(findViewById(android.R.id.content), mstatuscode +" Unauthorized Error",Snackbar.LENGTH_LONG).show();

                    } else
                    {
                        Snackbar.make(findViewById(android.R.id.content), "Bad Internet try again!",Snackbar.LENGTH_LONG).show();
                    }
                }catch (Exception e )
                {
                    e.printStackTrace();
                    Log.i(TAG, "cause"+String.valueOf(e.getCause()));
                    Log.i(TAG, "Local"+String.valueOf(e.getLocalizedMessage()));
                    Log.i(TAG, "Message"+String.valueOf(e.getMessage()));
                    Log.i(TAG, "Message"+String.valueOf(e.toString()));

                }
                Snackbar.make(findViewById(android.R.id.content),"Something went Wrong..",Snackbar.LENGTH_LONG).show();


            }
        })
        {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<>();
                //params.put("tags", tags);
                return params;
            }


            @Override
            protected Map<String, MultipartRequest.DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();
                params.put("image", new MultipartRequest.DataPart(imagename + ".jpeg", getFileDataFromDrawable(editedBitmap)));
                return params;
            }


        };multipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,DefaultRetryPolicy.DEFAULT_MAX_RETRIES ,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(this).add(multipartRequest);

    }

    private void UPloadTonextScreen(Bitmap b, String id, String emp, String url) {

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
        this.b.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        final byte[] byteArray = stream.toByteArray();

        Intent in1 = new Intent(ImagePreview.this, DashboardActivity.class);
            in1.putExtra("id",id);
            in1.putExtra("Emp",emp);
            in1.putExtra("url", url);
            in1.putExtra("image", byteArray);
            startActivity(in1);
            finish();

    }


    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    private void drawPoint(Canvas canvas, PointF point) {

        Paint paint = new Paint();
        paint.setColor(Color.RED);
        paint.setStrokeWidth(8);
        paint.setStyle(Paint.Style.STROKE);
        float x = point.x;
        float y = point.y;
        canvas.drawCircle(x, y, 1, paint);
    }

    public byte[] getFileDataFromDrawable(Bitmap bitmap) {

        if (bitmap != null) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
            Log.i(TAG, byteArrayOutputStream.toByteArray().toString());
            return byteArrayOutputStream.toByteArray();

        } else {
            Log.i(TAG, "Bitmap is Empty ");

            return null;
        }
    }



    @Override
    protected void onDestroy() {
        finish();
        b = null;
//        getIntent().removeExtra("image");
        Log.i(TAG, "Data Removed ");
        CircularProgressDialogue.dismissdialog();
        super.onDestroy();
        Runtime.getRuntime().gc();
        System.gc();
    }


    private Bitmap getCircularBitmap(Bitmap bitmap) {

        Bitmap output;
        if (bitmap.getWidth() > bitmap.getHeight()) {
            output = Bitmap.createBitmap(bitmap.getHeight(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        } else {
            output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getWidth(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        float r = 0;

        if (bitmap.getWidth() > bitmap.getHeight()) {
            r = bitmap.getHeight() / 2;
        } else {
            r = bitmap.getWidth() / 2;
        }

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawCircle(r, r, r, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return output;
    }
}
