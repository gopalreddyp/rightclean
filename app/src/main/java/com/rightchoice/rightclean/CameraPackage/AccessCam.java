package com.rightchoice.rightclean.CameraPackage;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.Tracker;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.FaceDetector;
import com.google.android.material.snackbar.Snackbar;
import com.rightchoice.rightclean.CircularProgressdialog.CircularProgressDialogue;
import com.rightchoice.rightclean.R;
import com.rightchoice.rightclean.StartCameraPackage.StartCamera;
import com.rightchoice.rightclean.camera.CameraSourcePreview;
import com.rightchoice.rightclean.camera.GraphicOverlay;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Objects;

import static com.rightchoice.rightclean.CameraPackage.Image.rotateImage;
import static com.rightchoice.rightclean.CameraPackage.Image.saveBitmapToJpg;

public class AccessCam extends AppCompatActivity {

    private static final String TAG = AccessCam.class.getSimpleName();
    private CameraSource mCameraSource = null;
    Bitmap finalBitmap;
    private CameraSourcePreview mPreview;
    private GraphicOverlay mGraphicOverlay;
    private FaceDetector detector;
    private static final int RC_HANDLE_GMS = 9001;
    // permission request codes need to be < 256
    private static final int RC_HANDLE_CAMERA_PERM = 2;
    private long lastImageTakenTime = 0;
    private int width,height;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_access_cam);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mPreview = (CameraSourcePreview) findViewById(R.id.preview);
        mGraphicOverlay = (GraphicOverlay) findViewById(R.id.faceOverlay);

        // Check for the camera permission before accessing the camera.  If the
        // permission is not granted yet, request permission.


        SharedPreferences prefs = getSharedPreferences("CAMERARESIZE", MODE_PRIVATE);
        width = prefs.getInt("width", 320);
        height = prefs.getInt("height", 240);


        int rc = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (rc == PackageManager.PERMISSION_GRANTED)
        {
            createCameraSource();
        } else {
            requestCameraPermission();
        }

    }

    /**
     * Handles the requesting of the camera permission.  This includes
     * showing a "Snackbar" message of why the permission is needed then
     * sending the request.
     */
    private void requestCameraPermission() {
        Log.w(TAG, "Camera permission is not granted. Requesting permission");

        final String[] permissions = new String[]{Manifest.permission.CAMERA};

        if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CAMERA)) {
            ActivityCompat.requestPermissions(this, permissions, RC_HANDLE_CAMERA_PERM);
            return;
        }

        final Activity thisActivity = this;

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityCompat.requestPermissions(thisActivity, permissions,
                        RC_HANDLE_CAMERA_PERM);
            }
        };

        Snackbar.make(mGraphicOverlay, R.string.permission_camera_rationale,
                Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.ok, listener)
                .show();
    }

    /**
     * Creates and starts the camera.  Note that this uses a higher resolution in comparison
     * to other detection examples to enable the barcode detector to detect small barcodes
     * at long distances.
     */
    private void createCameraSource() {


        Context context = getApplicationContext();
        FaceDetector detector = new FaceDetector.Builder(context)
                .setLandmarkType(FaceDetector.ALL_LANDMARKS)
                .setClassificationType(FaceDetector.ALL_CLASSIFICATIONS)//default is All_classification
                .setProminentFaceOnly(true)
                .setMinFaceSize(0.35f)
                .setMode(1)
                .build();

        detector.setProcessor(
                new MultiProcessor.Builder<Face>(new GraphicFaceTrackerFactory())
                        .build());
        if (!detector.isOperational())
        {
            // Note: The first time that an app using face API is installed on a device, GMS will
            // download a native library to the device in order to do detection.  Usually this
            // completes before the app is run for the first time.  But if that download has not yet
            // completed, then the above call will not detect any faces.
            //
            // isOperational() can be used to check if the required native library is currently
            // available.  The detector will automatically become operational once the library
            // download completes on device.
            Log.w(TAG, "Face detector dependencies are not yet available.");
        }


        mCameraSource = new CameraSource.Builder(context, detector)
                .setRequestedPreviewSize(width,height) //640,480 width,height
                .setFacing(CameraSource.CAMERA_FACING_FRONT)//CAMERA_FACING_BACK CAMERA_FACING_FRONT
                .setRequestedFps(30.0f)
                .build();

    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.cancel_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        if (item.getItemId() == R.id.cancel) {
            CircularProgressDialogue.showDialog(AccessCam.this, "", "");
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    CircularProgressDialogue.dismissdialog();
                    startActivity(new Intent(AccessCam.this, StartCamera.class));
                    finish();
                }
            }, 1000);

            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    /**
     * Restarts the camera.
     */
    @Override
    protected void onResume() {
        super.onResume();
        startCameraSource();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(AccessCam.this,StartCamera.class);
        startActivity(intent);
        finish();
    }

    /**
     * Stops the camera.
     */
    @Override
    protected void onPause() {
        super.onPause();
        mPreview.stop();
    }

    /**
     * Releases the resources associated with the camera source, the associated detector, and the
     * rest of the processing pipeline.
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCameraSource != null) {
            mCameraSource.release();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode != RC_HANDLE_CAMERA_PERM) {
            Log.d(TAG, "Got unexpected permission result: " + requestCode);
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            return;
        }

        if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "Camera permission granted - initialize the camera source");
            // we have permission, so create the camerasource
            createCameraSource();
            return;
        }

        Log.e(TAG, "Permission not granted: results len = " + grantResults.length +
                " Result code = " + (grantResults.length > 0 ? grantResults[0] : "(empty)"));

        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("RightClean")
                .setMessage(R.string.no_camera_permission)
                .setPositiveButton(R.string.ok, listener)
                .show();
    }

    //==============================================================================================
    // Camera Source Preview
    //==============================================================================================

    /**
     * Starts or restarts the camera source, if it exists.  If the camera source doesn't exist yet
     * (e.g., because onResume was called before the camera source was created), this will be called
     * again when the camera source is created.
     */
    private void startCameraSource() {

        // check that the device has play services available.
        int code = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(
                getApplicationContext());
        if (code != ConnectionResult.SUCCESS) {
            Dialog dlg =
                    GoogleApiAvailability.getInstance().getErrorDialog(this, code, RC_HANDLE_GMS);
            dlg.show();
        }

        if (mCameraSource != null) {
            try {
                mPreview.start(mCameraSource, mGraphicOverlay);
            } catch (IOException e) {
                Log.e(TAG, "Unable to start camera source.", e);
                mCameraSource.release();
                mCameraSource = null;
            }
        }
    }

    //==============================================================================================
    // Graphic Face Tracker
    //==============================================================================================

    /**
     * Factory for creating a face tracker to be associated with a new face.  The multiprocessor
     * uses this factory to create face trackers as needed -- one for each individual.
     */
    public class GraphicFaceTrackerFactory implements MultiProcessor.Factory<Face> {
        @Override
        public Tracker<Face> create(Face face) {
            return new GraphicFaceTracker(mGraphicOverlay);
        }
    }

    /**
     * Face tracker for each detected individual. This maintains a face graphic within the app's
     * associated face overlay.
     */
    public class GraphicFaceTracker extends Tracker<Face> {
        private GraphicOverlay mOverlay;
        private FaceGraphic mFaceGraphic;
        int isEyesClosedCount = 0;

        public GraphicFaceTracker(GraphicOverlay overlay) {
            mOverlay = overlay;
            mFaceGraphic = new FaceGraphic(overlay);
        }

        /**
         * Start tracking the detected face instance within the face overlay.
         */

        @Override
        public void onNewItem(int faceId, Face item) {
            mFaceGraphic.setId(faceId);
            Log.i(TAG, "face id " + item);
        }

        /**
         * Update the position/characteristics of the face within the overlay.
         */

        @Override
        public void onUpdate(FaceDetector.Detections<Face> detectionResults, Face face) {
            final double SMILING_THRESHOLD = 0;
            final double WINK_THRESHOLD = 0.4;
            float leftEye = face.getIsLeftEyeOpenProbability();
            float rightEye = face.getIsRightEyeOpenProbability();


            Log.i(TAG, "left eye " + String.valueOf(leftEye));
            Log.i(TAG, "right eye " + String.valueOf(rightEye));
            Log.i(TAG, "Calcu " + String.valueOf(Math.abs(leftEye - rightEye)));

            boolean isSmiling = face.getIsSmilingProbability() > SMILING_THRESHOLD;
            boolean oneShotOnly = System.currentTimeMillis() - lastImageTakenTime > 2000;

            if (face.getIsLeftEyeOpenProbability() > 0.4 || face.getIsRightEyeOpenProbability() > 0.4) {
                if (isSmiling) {
                    if (oneShotOnly) {
                        if (Math.abs(leftEye - rightEye) > WINK_THRESHOLD) {
                            // saveCurrentImage(); // Take a picture
                            Log.i(TAG, "Capturing " + String.valueOf(Math.abs(leftEye - rightEye)));
                            saveCurrentImage();
/*
                            mOverlay.add(mFaceGraphic);
                            mFaceGraphic.updateFace(face);
*/
                        }
                    }

                }
            }

            mOverlay.add(mFaceGraphic);
            mFaceGraphic.updateFace(face);

        }

        /**
         * Hide the graphic when the corresponding face was not detected.  This can happen for
         * intermediate frames temporarily (e.g., if the face was momentarily blocked from
         * view).
         */
        @Override
        public void onMissing(FaceDetector.Detections<Face> detectionResults) {

            mOverlay.remove(mFaceGraphic);

        }

        /**
         * Called when the face is assumed to be gone for good. Remove the graphic annotation from
         * the overlay.
         */
        @Override
        public void onDone() {
            mOverlay.remove(mFaceGraphic);
        }

    }

    private void saveCurrentImage() {

        if (mCameraSource != null) {
            lastImageTakenTime = System.currentTimeMillis();
            mCameraSource.takePicture(null, mPicture);
        }

    }

    private CameraSource.PictureCallback mPicture = new CameraSource.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] bytes) {
            int orientation = Exif.getOrientation(bytes);
            Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
            switch (orientation) {
                case 90:
                    bitmap = rotateImage(bitmap, 90);

                    break;
                case 180:
                    bitmap = rotateImage(bitmap, 180);

                    break;
                case 270:
                    bitmap = rotateImage(bitmap, 270);

                    break;
                case 0:
                    // if orientation is zero we don't need to rotate this

                default:
                    break;

            }

            String path = Environment.getExternalStorageDirectory().toString() + File.separator + "My360Access";
            String IamgeName = "full_image_" + System.currentTimeMillis() + ".jpeg";
//            Snackbar.make(findViewById(android.R.id.content), "Image Taken", Snackbar.LENGTH_LONG).show();
            saveBitmapToJpg(bitmap, path, IamgeName, bitmap.getWidth(), bitmap.getHeight());
            /*new MyAsyncTask(DetectionActivitygs.this).execute(path +  File.separator +  IamgeName , path +  File.separator + "faceDatabase"
                    , path +  File.separator + "faceDatabase");
*/
            Bitmap converetdImage = getResizedBitmap(bitmap, 500);
            //ProcessImage(converetdImage);
            //comment for now

            //converetdImage   = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.srinvas_madhu);
            UPloadtoNextScreen(converetdImage);


        }

    };


    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public void UPloadtoNextScreen(final Bitmap bmp) {
        Log.i(TAG, "Next Screen" + bmp);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        final byte[] byteArray = stream.toByteArray();

        Intent in1 = new Intent(AccessCam.this, ImagePreview.class);
        in1.putExtra("image", byteArray);
        startActivity(in1);
        finish();

        Log.i(TAG,"Access Completerd");


    }



}
