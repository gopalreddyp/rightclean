package com.rightchoice.rightclean.CameraPackage;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;
import com.rightchoice.rightclean.CircularProgressdialog.CircularProgressDialogue;
import com.rightchoice.rightclean.JsonNetworkPackage.MultipartRequest;
import com.rightchoice.rightclean.R;
import com.rightchoice.rightclean.StartCameraPackage.StartCamera;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;

public class Admin_confirm extends AppCompatActivity {


    private static final String TAG = Admin_confirm.class.getSimpleName();
    TextView cancel,next,main_msg,err_msg,user_profilename;
    ImageView detected_face;
    EditText pinview;
    String passcode;
    ImageView anim;
    Animation animFadeOut;
    String  name;
    String  type;
    private static final int SELECTED_PIC = 1;
    Bundle bundle;
    Button register,cancelbtn;
    private Bitmap b;
    SharedPreferences sharedpreferences;
    String id,devicecount,url;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_confirm);

        detected_face = findViewById(R.id.user_profile_photo);
        pinview = findViewById(R.id.pinview);
        main_msg = findViewById(R.id.main_msg);
        err_msg = findViewById(R.id.err_msg);
        anim = findViewById(R.id.animaimage);
        user_profilename = findViewById(R.id.user_profile_name);
        register = findViewById(R.id.register);
        cancelbtn = findViewById(R.id.cancel);


        bundle = getIntent().getExtras();
        assert bundle != null;
        if(getIntent().hasExtra("image")) {
            b = BitmapFactory.decodeByteArray(
                    getIntent().getByteArrayExtra("image"),0,getIntent()
                            .getByteArrayExtra("image").length);
            detected_face.setImageBitmap(getCircularBitmap(b));

        }
        animFadeOut = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.animation);

        sharedpreferences = getSharedPreferences("LoginCredentials",Context.MODE_PRIVATE);
        id = sharedpreferences.getString("id","");
        url = sharedpreferences.getString("url","");
        devicecount = sharedpreferences.getString("devicecount","");
        Log.i(TAG,url);



        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                passcode = pinview.getText().toString();
                if(passcode.length() == 6)
                {
                    Validateuserwithpasscode(passcode,b);
                    Log.i(TAG,passcode);
                    Log.i(TAG,"Reg "+passcode);


                } else
                {
                    pinview.setError("Invalid Please enter 6 digits code");
                }

            }
        });



    }


    @Override
    protected void onDestroy() {
        CircularProgressDialogue.dismissdialog();
        super.onDestroy();
    }

    private void Validateuserwithpasscode(final String passcode, final Bitmap b) {

        CircularProgressDialogue.showDialog(Admin_confirm.this,"","");
        MultipartRequest multipartRequest = new MultipartRequest(Request.Method.POST, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                CircularProgressDialogue.dismissdialog();
                try {
                    JSONObject obj = new JSONObject(new String(response.data));
                    Log.i(TAG, obj.toString());
                    String Status = obj.getString("status");
                    if (Status.equals("success")) {
                        String Name = obj.getString("name");
                        Log.i(TAG,Name);
                        Snackbar.make(findViewById(android.R.id.content),"Hi "+Name +" Successfully Verified!" ,Snackbar.LENGTH_LONG).show();
                        err_msg.setVisibility(View.VISIBLE);
                        err_msg.setText(Name);
                        anim.setVisibility(View.VISIBLE);
                        anim.setImageResource(R.drawable.rite);
                        anim.startAnimation(animFadeOut);
/*
                        registercode.setVisibility(View.VISIBLE);
                        registerbutton.setVisibility(View.VISIBLE);
                        cancel.setVisibility(View.VISIBLE);
*/

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                startActivity(new Intent(Admin_confirm.this, StartCamera.class));
                                finish();
                            }
                        },2000);

                    } else if(Status.equals("error"))
                    {
                        String result = obj.getString("result");
                        Log.i(TAG,result);
                        Snackbar.make(findViewById(android.R.id.content),result,Snackbar.LENGTH_LONG).show();

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                startActivity(new Intent(Admin_confirm.this,StartCamera.class));
                                finish();
                            }
                        },2000);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override //Something Went Wrong We are Working...
            public void onErrorResponse(VolleyError error) {
                CircularProgressDialogue.dismissdialog();
                NetworkResponse response = error.networkResponse;
                String errorMsg = "";
                if(response != null && response.data != null){
                    String errorString = new String(response.data);
                    Log.i("log error", errorString);
                }
                int mstatuscode = response.statusCode;
                Log.i(TAG, String.valueOf(mstatuscode));

                if(mstatuscode == HttpURLConnection.HTTP_NOT_FOUND)
                {

                    Snackbar.make(findViewById(android.R.id.content), mstatuscode +" Server Not Found",Snackbar.LENGTH_LONG).show();
                } else if(mstatuscode == HttpURLConnection.HTTP_INTERNAL_ERROR)
                {
                    Snackbar.make(findViewById(android.R.id.content), mstatuscode +" Internal Server Error",Snackbar.LENGTH_LONG).show();

                }else
                {
                    Snackbar.make(findViewById(android.R.id.content), "Bad Internet try again!",Snackbar.LENGTH_LONG).show();
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run()
                    {
                        startActivity(new Intent(Admin_confirm.this, StartCamera.class));
                        finish();
                    }
                },3000);

            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<>();
                //params.put("tags", tags);
                return params;
            }


            /*
             * Here we are passing image by renaming it with a unique name
             *
             */

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();
                params.put(passcode, new MultipartRequest.DataPart(imagename + ".jpeg", getFileDataFromDrawable(b)));
                Log.i(TAG,passcode);
                return params;
            }

        };multipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,DefaultRetryPolicy.DEFAULT_MAX_RETRIES ,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(this).add(multipartRequest);

    }


    private byte[] getFileDataFromDrawable(Bitmap bitmap) {
        if(bitmap != null)
        {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
            Log.i(TAG,byteArrayOutputStream.toByteArray().toString());
            return byteArrayOutputStream.toByteArray();

        } else
        {
            Log.i(TAG,"Bitmap is Empty ");

            return null;
        }


    }

    private Bitmap getCircularBitmap(Bitmap bitmap) {

        Bitmap output;
        if (bitmap.getWidth() > bitmap.getHeight()) {
            output = Bitmap.createBitmap(bitmap.getHeight(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        } else {
            output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getWidth(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        float r = 0;

        if (bitmap.getWidth() > bitmap.getHeight()) {
            r = bitmap.getHeight() / 2;
        } else {
            r = bitmap.getWidth() / 2;
        }

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawCircle(r, r, r, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return output;
    }}
