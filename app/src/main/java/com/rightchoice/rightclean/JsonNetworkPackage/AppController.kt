package com.rightchoice.rightclean.JsonNetworkPackage

import DaggerApplicationComponent
import android.text.TextUtils
import androidx.multidex.MultiDexApplication
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.ImageLoader
import com.android.volley.toolbox.Volley
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

class AppController : MultiDexApplication(), HasAndroidInjector {
    private var mRequestQueue: RequestQueue? = null
    private var mImageLoader: ImageLoader? = null

    @JvmField
    @Inject
    var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>? = null
    override fun onCreate() {
        super.onCreate()
        instance = this
        DaggerApplicationComponent.builder().create(this).inject(this)
    }

    //#region Volley
    val requestQueue: RequestQueue?
        get() {
            if (mRequestQueue == null) {
                mRequestQueue = Volley.newRequestQueue(applicationContext)
            }
            return mRequestQueue
        }
    val imageLoader: ImageLoader?
        get() {
            requestQueue
            if (mImageLoader == null) {
                mImageLoader = ImageLoader(mRequestQueue,
                        LruBitmapCache())
            }
            return mImageLoader
        }

    fun <T> addToRequestQueue(req: Request<T>, tag: String?) {
        // set the default tag if tag is empty
        req.tag = if (TextUtils.isEmpty(tag)) TAG else tag
        requestQueue!!.add(req)
    }

    fun <T> addToRequestQueue(req: Request<T>) {
        req.tag = TAG
        requestQueue!!.add(req)
    }

    fun cancelPendingRequests(tag: Any?) {
        if (mRequestQueue != null) {
            mRequestQueue!!.cancelAll(tag)
        }
    }

    //#endregion Volley

    override fun androidInjector(): AndroidInjector<Any> {
        return dispatchingAndroidInjector!!
    }

    companion object {
        val TAG = AppController::class.java
                .simpleName


        lateinit var instance: AppController
            private set
    }
}