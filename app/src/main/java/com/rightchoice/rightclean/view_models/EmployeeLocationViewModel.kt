package com.rightchoice.rightclean.view_models

import android.app.Application
import android.content.Context
import android.location.LocationManager
import com.rightchoice.rightclean.JsonNetworkPackage.AppController
import com.rightchoice.rightclean.model.repository.webservices.WebServiceManager
import com.rightchoice.rightclean.model.repository.webservices.callbacks.SendEmployeeLocationsCallback
import okhttp3.RequestBody


class EmployeeLocationViewModel(application: Application) : BaseViewModel(application) {

    fun sendEmployeeLocation(requestBody: RequestBody, sendEmployeeLocationsCallback: SendEmployeeLocationsCallback) {
        val webServiceManager = WebServiceManager(getApplication())
        webServiceManager.sendEmployeeLocation(requestBody, sendEmployeeLocationsCallback)
    }

    fun checkIfLocationIsOn() : Boolean {
        val locationManager:LocationManager = AppController.instance.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        var isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
        var isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)

        if (!isGPSEnabled && !isNetworkEnabled) {
            return false
        }
        return true
    }
}