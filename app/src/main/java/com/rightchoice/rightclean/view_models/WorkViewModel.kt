package com.rightchoice.rightclean.view_models

import android.app.Application
import com.rightchoice.rightclean.model.repository.webservices.WebServiceManager
import com.rightchoice.rightclean.model.repository.webservices.callbacks.GetClientDetailsCallback
import com.rightchoice.rightclean.model.repository.webservices.callbacks.GetPendingWorkCallback
import okhttp3.RequestBody

class WorkViewModel(application: Application) : BaseViewModel(application) {

    fun getClientsDetails(requestBody: RequestBody, getClientDetailsCallback: GetClientDetailsCallback) {
        var webServiceManager = WebServiceManager(getApplication())
        webServiceManager.getClientsDetail(requestBody, getClientDetailsCallback)
    }

    fun getPendingWork(requestBody: RequestBody, getPendingWorkCallback: GetPendingWorkCallback) {
        var webServiceManager = WebServiceManager(getApplication())
        webServiceManager.getPendingWork(requestBody, getPendingWorkCallback)
    }
}