package com.rightchoice.rightclean.view_models

import android.app.Application
import com.google.gson.Gson
import com.rightchoice.rightclean.model.repository.shared_preferences.SharedPreferenceManager
import com.rightchoice.rightclean.model.repository.webservices.WebServiceManager
import com.rightchoice.rightclean.model.repository.webservices.callbacks.GetCompanyDetailsCallback
import com.rightchoice.rightclean.model.repository.webservices.pojo.response.GetCompanyDetailsResponseModel
import okhttp3.RequestBody

class CompanyViewModel(application: Application) : BaseViewModel(application) {

    fun getCompanyDetails(requestBody: RequestBody, getCompanyDetailsCallback: GetCompanyDetailsCallback) {
        var webServiceManager = WebServiceManager(getApplication())
        webServiceManager.getCompanyDetails(requestBody, getCompanyDetailsCallback)
    }

    fun saveCompanyDetails(getCompanyDetailsResponseModel: GetCompanyDetailsResponseModel) {
        getSharedPreferences().setString(SharedPreferenceManager.SharedPrefKeys.KEY_COMPANY_INFO,
                Gson().toJson(getCompanyDetailsResponseModel))
    }

    fun getCompanyDetails() : GetCompanyDetailsResponseModel {
        return Gson().fromJson(getSharedPreferences().
        getString(SharedPreferenceManager.SharedPrefKeys.KEY_COMPANY_INFO), GetCompanyDetailsResponseModel::class.java)
    }

}