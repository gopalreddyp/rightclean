package com.rightchoice.rightclean.view_models

import android.app.Application
import com.google.gson.Gson
import com.rightchoice.rightclean.model.repository.shared_preferences.SharedPreferenceManager
import com.rightchoice.rightclean.model.repository.webservices.WebServiceManager
import com.rightchoice.rightclean.model.repository.webservices.callbacks.LoginResponseCallback
import com.rightchoice.rightclean.model.repository.webservices.pojo.response.LoginResponseModel
import okhttp3.RequestBody

class LoginViewModel(application: Application) : BaseViewModel(application) {

    fun attemptLogin (loginRequestModel: RequestBody, loginResponseCallback: LoginResponseCallback) {
        var webServiceManager = WebServiceManager(getApplication())
        webServiceManager.attemptLogin(loginRequestModel, loginResponseCallback)
    }

    fun saveLoginResponse(loginResponseModel: LoginResponseModel) {
        getSharedPreferences().setString(SharedPreferenceManager.SharedPrefKeys.KEY_LOG_IN_RESPONSE,
                Gson().toJson(loginResponseModel))
    }

    fun getLoginResponse() : LoginResponseModel {
        return Gson().fromJson(getSharedPreferences().
        getString(SharedPreferenceManager.SharedPrefKeys.KEY_LOG_IN_RESPONSE), LoginResponseModel::class.java)
    }

    fun getEmployeeId() : String {
        return getLoginResponse().data.userData.employeesid
    }

    fun getEmployeeEmail() : String{
        return getLoginResponse().data.userData.email
    }

    fun getProfilePicURL(): String {
        return getLoginResponse().data.image
    }

    fun getEmployeeName() :String {
        return getLoginResponse().data.name
    }

    fun getPhoneNumber(): String {
        return getLoginResponse().data.userData.phone
    }
}