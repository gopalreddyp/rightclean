package com.rightchoice.rightclean.view_models

import android.R
import android.app.Application
import android.text.TextUtils
import android.view.View
import androidx.lifecycle.AndroidViewModel
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.rightchoice.rightclean.model.repository.shared_preferences.SharedPreferenceManager
import com.rightchoice.rightclean.model.repository.webservices.pojo.response.GetCompanyDetailsResponseModel

abstract class BaseViewModel(application: Application) : AndroidViewModel(application) {

    private var sharedPreferenceManager: SharedPreferenceManager? = null


    fun getSharedPreferences(): SharedPreferenceManager {
        if (sharedPreferenceManager == null)
            sharedPreferenceManager = SharedPreferenceManager(getApplication())
        return sharedPreferenceManager as SharedPreferenceManager
    }

    fun getLogoURL() : String {
        var getCompanyDetailsResponseModel = Gson().fromJson(getSharedPreferences().
        getString(SharedPreferenceManager.SharedPrefKeys.KEY_COMPANY_INFO), GetCompanyDetailsResponseModel::class.java)
        return getCompanyDetailsResponseModel.result.logo
    }

}