package com.rightchoice.rightclean.view_models

import android.app.Application
import com.rightchoice.rightclean.model.repository.webservices.WebServiceManager
import com.rightchoice.rightclean.model.repository.webservices.callbacks.EndWorkResponseCallback
import okhttp3.MultipartBody
import okhttp3.RequestBody

class EndWorkViewModel(application: Application) : BaseViewModel(application) {

    fun endWord(id: RequestBody?, punchOut: RequestBody?, empId: RequestBody?,
                clientId: RequestBody?, serviceList: RequestBody?, imageFile: MultipartBody.Part?,
                responseCallback: EndWorkResponseCallback) {
        var webServiceManager = WebServiceManager(getApplication())
        webServiceManager.endWork(id, punchOut, empId, clientId, serviceList, imageFile, responseCallback)
    }
}