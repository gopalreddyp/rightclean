package com.rightchoice.rightclean.activity_view_models

import com.google.gson.Gson
import com.rightchoice.rightclean.activity_view_models.navigators.ProfileActivityNavigator
import com.rightchoice.rightclean.model.repository.shared_preferences.SharedPreferenceManager
import com.rightchoice.rightclean.model.repository.webservices.pojo.response.LoginResponseModel
import javax.inject.Inject

class ProfileActivityViewModel @Inject constructor() :
        BaseViewModel<ProfileActivityNavigator>() {

    fun getLoginResponse(): LoginResponseModel {
        return Gson().fromJson(preferenceManager.getString(SharedPreferenceManager.SharedPrefKeys.KEY_LOG_IN_RESPONSE), LoginResponseModel::class.java)
    }

}