package com.rightchoice.rightclean.activity_view_models

import com.rightchoice.rightclean.JsonNetworkPackage.AppController
import com.rightchoice.rightclean.activity_view_models.navigators.ForgotPasswordActivityNavigator
import com.rightchoice.rightclean.model.repository.webservices.WebServiceManager
import com.rightchoice.rightclean.model.repository.webservices.callbacks.ForgotPasswordCallback
import com.techresources.clearmots.model.exceptions.NoInternetConnectionException
import okhttp3.RequestBody
import javax.inject.Inject

class ForgotPasswordActivityViewModel @Inject constructor() :
        BaseViewModel<ForgotPasswordActivityNavigator>() {

    @Throws(NoInternetConnectionException::class)
    fun getForgotPasswordDetails(requestBody: RequestBody, forgotPasswordCallback: ForgotPasswordCallback) {
        WebServiceManager(AppController.instance).getForgotPasswordEmail(requestBody, forgotPasswordCallback)
    }

    fun submit(){
        getNavigator()?.submit()
    }

    fun goToSignInActivity(){
        getNavigator()?.goToSignInActivity()
    }

}