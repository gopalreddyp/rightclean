package com.rightchoice.rightclean.activity_view_models.navigators

interface CompanyIDActivityNavigator: BaseNavigator {

    fun getLoginScreen()
    fun showHidePasswordData()
}