package com.rightchoice.rightclean.activity_view_models

import com.rightchoice.rightclean.JsonNetworkPackage.AppController
import com.rightchoice.rightclean.activity_view_models.navigators.ChangePasswordActivityNavigator
import com.rightchoice.rightclean.model.repository.webservices.WebServiceManager
import com.rightchoice.rightclean.model.repository.webservices.callbacks.ChangePasswordCallback
import com.techresources.clearmots.model.exceptions.NoInternetConnectionException
import okhttp3.RequestBody
import javax.inject.Inject

class ChangePasswordActivityViewModel @Inject constructor() :
        BaseViewModel<ChangePasswordActivityNavigator>() {

    @Throws(NoInternetConnectionException::class)
    fun getChangePasswordDetails(requestBody: RequestBody, changePasswordCallback: ChangePasswordCallback) {
        WebServiceManager(AppController.instance).getChangePasswordDetails(requestBody, changePasswordCallback)
    }

}