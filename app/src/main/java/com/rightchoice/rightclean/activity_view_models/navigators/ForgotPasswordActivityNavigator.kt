package com.rightchoice.rightclean.activity_view_models.navigators

interface ForgotPasswordActivityNavigator: BaseNavigator {

    fun submit()
    fun goToSignInActivity()
}