package com.rightchoice.rightclean.activity_view_models

import com.rightchoice.rightclean.JsonNetworkPackage.AppController
import com.rightchoice.rightclean.activity_view_models.navigators.EmailFragmentNavigator
import com.rightchoice.rightclean.model.repository.webservices.WebServiceManager
import com.rightchoice.rightclean.model.repository.webservices.callbacks.SelectModuleCallback
import okhttp3.RequestBody
import javax.inject.Inject

class EmailFragmentViewModel @Inject constructor() :
        BaseViewModel<EmailFragmentNavigator>() {

    fun closeFragment(){
        getNavigator()?.closeFragment()
    }

    fun selectModule(requestBody: RequestBody, getSelectModuleCallback: SelectModuleCallback) {
        WebServiceManager(AppController.instance).selectedModule(requestBody, getSelectModuleCallback)
    }
}