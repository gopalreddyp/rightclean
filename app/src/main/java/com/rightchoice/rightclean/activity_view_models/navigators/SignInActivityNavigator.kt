package com.rightchoice.rightclean.activity_view_models.navigators

interface SignInActivityNavigator: BaseNavigator {

    fun login()
    fun getForgotPasswordActivity()
}