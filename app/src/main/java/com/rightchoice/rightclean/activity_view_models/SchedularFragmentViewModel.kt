package com.rightchoice.rightclean.activity_view_models

import com.rightchoice.rightclean.JsonNetworkPackage.AppController
import com.rightchoice.rightclean.activity_view_models.navigators.SchedularFragmentNavigator
import com.rightchoice.rightclean.model.repository.webservices.WebServiceManager
import com.rightchoice.rightclean.model.repository.webservices.callbacks.GetSchedularDetailsCallback
import com.rightchoice.rightclean.model.repository.webservices.callbacks.StartWorkCallback
import com.techresources.clearmots.model.exceptions.NoInternetConnectionException
import okhttp3.RequestBody
import javax.inject.Inject

class SchedularFragmentViewModel @Inject constructor() :
        BaseViewModel<SchedularFragmentNavigator>() {

    @Throws(NoInternetConnectionException::class)
    fun getSchedularDetails(requestBody: RequestBody, getSchedularDetailsCallback: GetSchedularDetailsCallback) {
        WebServiceManager(AppController.instance).getSchedularDetails(requestBody, getSchedularDetailsCallback)
    }

    @Throws(NoInternetConnectionException::class)
    fun startWork(requestBody: RequestBody, callback: StartWorkCallback) {
        WebServiceManager(AppController.instance).startWork(requestBody, callback)
    }

    fun getFormattedMonth(month:Int): String {
        var actualMonth = month
        actualMonth++
        return String.format("%02d", actualMonth)
    }
}