package com.rightchoice.rightclean.activity_view_models.navigators

interface BaseNavigator {
    fun setProgressVisibility(visibility: Int) {}
}