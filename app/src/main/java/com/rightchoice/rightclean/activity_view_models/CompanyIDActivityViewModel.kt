package com.rightchoice.rightclean.activity_view_models

import com.rightchoice.rightclean.JsonNetworkPackage.AppController
import com.rightchoice.rightclean.activity_view_models.navigators.CompanyIDActivityNavigator
import com.rightchoice.rightclean.model.repository.webservices.WebServiceManager
import com.rightchoice.rightclean.model.repository.webservices.callbacks.GetCompanyDetailsCallback
import com.techresources.clearmots.model.exceptions.NoInternetConnectionException
import okhttp3.RequestBody
import javax.inject.Inject

class CompanyIDActivityViewModel @Inject constructor() :
        BaseViewModel<CompanyIDActivityNavigator>() {

    fun getLoginScreen() {
        getNavigator()?.getLoginScreen()
    }

    fun showHidePasswordData() {
        getNavigator()?.showHidePasswordData()
    }

    @Throws(NoInternetConnectionException::class)
    fun getCompanyDetails(requestBody: RequestBody, getCompanyDetailsCallback: GetCompanyDetailsCallback) {
        WebServiceManager(AppController.instance).getCompanyDetails(requestBody, getCompanyDetailsCallback)
    }


}