package com.rightchoice.rightclean.activity_view_models

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.rightchoice.rightclean.di.scopes.PerApplication
import com.rightchoice.rightclean.utilities.MyLogger
import com.rightchoice.rightclean.utilities.extensions.safeGet
import javax.inject.Inject
import javax.inject.Provider

@Suppress("UNCHECKED_CAST")
@PerApplication
class ViewModelFactory @Inject constructor(
    private val creators: Map<Class<out ViewModel>,
            @JvmSuppressWildcards Provider<ViewModel>>
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {

        var creator: Provider<out ViewModel>? = creators[modelClass]
        if (creator == null) {
            for ((key, value) in creators) {
                if (modelClass.isAssignableFrom(key)) {
                    creator = value
                    break
                }
            }
        }
        if (creator == null) {
            throw IllegalArgumentException("Unknown ViewModel: '$modelClass'")
        }

        try {
            return creator.get() as T
        } catch (e: Exception) {
            MyLogger.debug(ViewModelFactory::class.java.simpleName, e.message.safeGet())
            throw  RuntimeException(e)
        }
    }
}
