package com.rightchoice.rightclean.activity_view_models.navigators

interface EmailFragmentNavigator: BaseNavigator {

    fun closeFragment()

}