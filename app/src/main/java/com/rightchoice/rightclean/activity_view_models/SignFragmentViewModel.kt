package com.rightchoice.rightclean.activity_view_models

import com.rightchoice.rightclean.activity_view_models.navigators.SignFragmentNavigator
import javax.inject.Inject

class SignFragmentViewModel @Inject constructor() :
        BaseViewModel<SignFragmentNavigator>() {

}