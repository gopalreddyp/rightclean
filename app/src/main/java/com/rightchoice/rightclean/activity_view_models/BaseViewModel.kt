package com.rightchoice.rightclean.activity_view_models

import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import com.rightchoice.rightclean.model.repository.shared_preferences.SharedPreferenceManager
import com.rightchoice.rightclean.model.repository.webservices.pojo.response.CompanyResult
import com.rightchoice.rightclean.utilities.PreferenceManager
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import java.lang.ref.WeakReference
import javax.inject.Inject

abstract class BaseViewModel <N> : ViewModel() {

    @Inject
    lateinit var preferenceManager: PreferenceManager

    lateinit var navigator: WeakReference<N>

    private var compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun onCleared() {
        with(compositeDisposable) {
            if (isDisposed.not()) {
                dispose()
                compositeDisposable = CompositeDisposable()
            }
        }
        super.onCleared()
    }

    fun getNavigator(): N? {
        return navigator.get()
    }

    fun setNavigator(navigator: N) {
        this.navigator = WeakReference(navigator)
    }

    fun addDisposable(disposable: Disposable?) {

        disposable?.let {
            if (it.isDisposed.not()) {
                compositeDisposable.add(it)
            }
        }
    }

    fun saveCompanyDetails(details: CompanyResult) {
        preferenceManager.setString(SharedPreferenceManager.SharedPrefKeys.KEY_COMPANY_INFO,
                Gson().toJson(details))
    }

    fun getCompanyDetails(): CompanyResult {
        return Gson().fromJson(preferenceManager.getString(SharedPreferenceManager.SharedPrefKeys.KEY_COMPANY_INFO), CompanyResult::class.java)
    }

}