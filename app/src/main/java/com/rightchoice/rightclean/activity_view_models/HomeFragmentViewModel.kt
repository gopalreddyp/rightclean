package com.rightchoice.rightclean.activity_view_models

import com.rightchoice.rightclean.activity_view_models.navigators.HomeFragmentNavigator
import javax.inject.Inject

class HomeFragmentViewModel @Inject constructor() :
        BaseViewModel<HomeFragmentNavigator>() {

    fun setDropDown(){
                getNavigator()?.setDropDown()
            }

    fun getEmailFrgament(){
        getNavigator()?.getEmailFragment()
    }

    fun getSchedularFragment(){
        getNavigator()?.getSchedularFragment()
    }

/*
    fun getPayFragment(){
        getNavigator()?.getPayFragment()
    }

    fun getCompletedTaskFragment(){
        getNavigator()?.getCompletedTaskFragment()
    }

    fun getActivityFragment(){
        getNavigator()?.getActivityFragment()
    }


    fun getCallFragment(){
        getNavigator()?.getCallFragment()
    }*/
}