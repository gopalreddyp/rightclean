package com.rightchoice.rightclean.activity_view_models

import com.rightchoice.rightclean.JsonNetworkPackage.AppController
import com.rightchoice.rightclean.activity_view_models.navigators.ScheduleActivityNavigator
import com.rightchoice.rightclean.model.repository.webservices.WebServiceManager
import com.rightchoice.rightclean.model.repository.webservices.callbacks.GetSchedularDetailsCallback
import com.techresources.clearmots.model.exceptions.NoInternetConnectionException
import okhttp3.RequestBody
import javax.inject.Inject

class ScheduleActivityViewModel @Inject constructor() :
        BaseViewModel<ScheduleActivityNavigator>() {

    @Throws(NoInternetConnectionException::class)
    fun getSchedularDetails(requestBody: RequestBody,getSchedularDetailsCallback: GetSchedularDetailsCallback) {
        WebServiceManager(AppController.instance).getSchedularDetails(requestBody, getSchedularDetailsCallback)
    }

}