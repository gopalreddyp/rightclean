package com.rightchoice.rightclean.activity_view_models

import com.rightchoice.rightclean.activity_view_models.navigators.DashboardActivityNavigator
import javax.inject.Inject

class DashboardActivityViewModel @Inject constructor() :
        BaseViewModel<DashboardActivityNavigator>() {

}