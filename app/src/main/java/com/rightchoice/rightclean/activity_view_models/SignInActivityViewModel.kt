package com.rightchoice.rightclean.activity_view_models

import com.google.gson.Gson
import com.rightchoice.rightclean.JsonNetworkPackage.AppController
import com.rightchoice.rightclean.activity_view_models.navigators.SignInActivityNavigator
import com.rightchoice.rightclean.model.repository.shared_preferences.SharedPreferenceManager
import com.rightchoice.rightclean.model.repository.webservices.WebServiceManager
import com.rightchoice.rightclean.model.repository.webservices.callbacks.LoginResponseCallback
import com.rightchoice.rightclean.model.repository.webservices.pojo.response.LoginResponseModel
import com.techresources.clearmots.model.exceptions.NoInternetConnectionException
import okhttp3.RequestBody
import javax.inject.Inject

class SignInActivityViewModel @Inject constructor() :
        BaseViewModel<SignInActivityNavigator>() {

    @Throws(NoInternetConnectionException::class)
    fun attemptLogin(requestBody: RequestBody, callback: LoginResponseCallback) {
        WebServiceManager(AppController.instance).attemptLogin(requestBody, callback)
    }

    fun login() {
        getNavigator()?.login()
    }

    fun saveLoginResponse(loginResponseModel: LoginResponseModel) {
        preferenceManager.setString(SharedPreferenceManager.SharedPrefKeys.KEY_LOG_IN_RESPONSE,
                Gson().toJson(loginResponseModel))
    }

    fun getForgotPasswordActivity(){
        getNavigator()?.getForgotPasswordActivity()
    }

}