package com.rightchoice.rightclean.DashboardPackage;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;

import com.rightchoice.rightclean.views.activities.MainActivity;
import com.rightchoice.rightclean.R;

import java.util.Calendar;

import static android.app.Notification.EXTRA_NOTIFICATION_ID;
import static android.app.NotificationManager.IMPORTANCE_DEFAULT;

public  class ShowNotification extends BroadcastReceiver {
    private final static String TAG = "ShowNotification";
    private static final String CHANNEL_ID = TAG;
    private static String ACTION_SNOOZE ="" ;
    int hour   ;
    int minute ;
    int second ;
    AlarmManager alarmManager;
    SharedPreferences sharedPreferences;
    private String clientidno,empid,personname,modifiedtime;


    @Override
    public void onReceive(Context context, Intent intent)
    {
        sharedPreferences = context.getSharedPreferences("CLIENTNDEMPID", Context.MODE_PRIVATE);
        clientidno = sharedPreferences.getString("clientid","");
        empid = sharedPreferences.getString("empid","");
        personname = sharedPreferences.getString("personname","");
        modifiedtime = sharedPreferences.getString("time","");
        Log.i(TAG,"Modified TIme "+modifiedtime);

        String puremin = modifiedtime.replaceAll("[^0-9.]", "");

        alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        Intent notificationIntent = new Intent(context, MainActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(notificationIntent);

        PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);


        Intent finishintent = new Intent(context,EndClean.class);
        PendingIntent finishpendingIntent = PendingIntent.getActivity(context, 0, finishintent, 0);



        Intent extendintent = new Intent(context,Details.class);
        PendingIntent extendpendingIntent = PendingIntent.getActivity(context, 0, extendintent, 0);


        Intent snoozeIntent = new Intent(context, this.getClass());
        snoozeIntent.setAction(ACTION_SNOOZE);
        snoozeIntent.putExtra(EXTRA_NOTIFICATION_ID, 0);
        PendingIntent snoozePendingIntent = PendingIntent.getBroadcast(context, 0, snoozeIntent, 0);


        Calendar cal = Calendar.getInstance();
        //cal.setTimeInMillis(System.currentTimeMillis());
        cal.add(Calendar.MINUTE, Integer.parseInt(puremin));
        alarmManager.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), snoozePendingIntent);


        Notification.Builder builder = new Notification.Builder(context);
        Notification notification = builder.setContentTitle("Hi "+personname)
                .setContentText("Work Completed ?")
                .setTicker("New Message Alert!")
                .setSmallIcon(R.drawable.right_clean_logo)
                .setContentIntent(pendingIntent)
                .addAction(R.drawable.right_clean_logo, "Extend Time", snoozePendingIntent)
                .addAction(R.drawable.right_clean_logo, "Finish", finishpendingIntent)
                .build();


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder.setChannelId(CHANNEL_ID);
        }

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    CHANNEL_ID,
                    "NotificationDemo",
                    IMPORTANCE_DEFAULT
            );
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }
        }

        if (notificationManager != null) {
            notificationManager.notify(0, notification);
        }


    }

    public void DismissNotification(Context context )
    {

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    CHANNEL_ID, "RightClean", IMPORTANCE_DEFAULT
            );
            if (notificationManager != null)
            {
                notificationManager.cancelAll();
            }
        }

        if (notificationManager != null)
        {
            notificationManager.cancelAll();
        }

    }


}
