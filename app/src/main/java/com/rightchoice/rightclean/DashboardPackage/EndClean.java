package com.rightchoice.rightclean.DashboardPackage;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;
import com.rightchoice.rightclean.CircularProgressdialog.CircularProgressDialogue;
import com.rightchoice.rightclean.R;
import com.rightchoice.rightclean.model.repository.webservices.callbacks.EndWorkResponseCallback;
import com.rightchoice.rightclean.model.repository.webservices.pojo.response.EndWorkResponseModel;
import com.rightchoice.rightclean.model.repository.webservices.web_apis.WebApi;
import com.rightchoice.rightclean.model.utilities.AppConstants;
import com.rightchoice.rightclean.model.utilities.LogUtility;
import com.rightchoice.rightclean.model.utilities.StorageUtility;
import com.rightchoice.rightclean.view_models.EmployeeLocationViewModel;
import com.rightchoice.rightclean.view_models.EndWorkViewModel;
import com.rightchoice.rightclean.view_models.LoginViewModel;
import com.rightchoice.rightclean.views.activities.BaseActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class EndClean extends BaseActivity implements View.OnClickListener {

    private static final String TAG = EndClean.class.getSimpleName();

    EndExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    Button btnEndWork;
    ListView listView;

    ArrayList dataModels;
    private CustomAdapter adapter;
    private LoginViewModel mLoginViewModel;
    private EmployeeLocationViewModel mLocationViewModel;
    private EndWorkViewModel mEndWorkViewModel;
    List<String> listDataHeader, mServicesList;
    HashMap<String, List<String>> listDataChild;
    List<String> childitems;
    private File mImageFile;

    String mClientId, mEmpId, mCurrentTime, mTransactionId, time, mServicesDone, mImage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end_clean);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        initViews();
        initInstanceVariables();
        setListeners();

        Bundle bundle = getIntent().getExtras();
        mClientId = bundle.getString(AppConstants.BundleKeys.CLIENT_ID);
        mTransactionId = bundle.getString(AppConstants.BundleKeys.TRANSACTION_ID);
        mEmpId = mLoginViewModel.getEmployeeId();

        btnEndWork.setVisibility(View.INVISIBLE);

        expListView.setIndicatorBounds(expListView.getRight() - 100, expListView.getWidth());

        getServices();
        listAdapter = new EndExpandableListAdapter(this, listDataHeader, listDataChild, (isSelected, subServiceName) -> {
            if (mServicesList.contains(subServiceName)) {
                mServicesList.remove(mServicesList.indexOf(subServiceName));
            } else {
                mServicesList.add(subServiceName);
            }
        });

        //startLocationService();
    }

    private void startLocationService() {
        Intent intent = new Intent(getApplicationContext(), MyLocationServices.class);
        intent.putExtra(AppConstants.BundleKeys.CLIENT_ID, mClientId);
        startService(intent);
    }

    private void setListeners() {
        btnEndWork.setOnClickListener(this);

    }

    private void initViews() {
        expListView = findViewById(R.id.end_expandable_lv);
        btnEndWork = findViewById(R.id.endendbutton);
        listView = findViewById(R.id.listView);
    }

    private void initInstanceVariables() {
        mLoginViewModel = new LoginViewModel(getApplication());
        mLocationViewModel = new EmployeeLocationViewModel(getApplication());
        mEndWorkViewModel = new EndWorkViewModel(getApplication());
        dataModels = new ArrayList();
        mServicesList = new ArrayList<>();

        mImage = "";
    }

    private void getServices() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();
        dataModels = new ArrayList();

        CircularProgressDialogue.showDialog(EndClean.this, "", "");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, WebApi.ApiMethods.RIGHT_CLEAN_SERVICES, response -> {
            CircularProgressDialogue.dismissdialog();
            Log.i(TAG, response);

            try {
                JSONObject jsonObject = new JSONObject(response);
                String status = jsonObject.getString("status");
                if (status.equals("success")) {
                    btnEndWork.setVisibility(View.VISIBLE);
                    JSONArray jsonArray = jsonObject.getJSONArray("result");
                    if (jsonArray.length() > 0) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            JSONArray jsonArray1 = jsonObject1.getJSONArray("Services");
                            if (jsonArray1.length() > 0) {
                                btnEndWork.setVisibility(View.VISIBLE);
                                for (int j = 0; j < jsonArray1.length(); j++) {
                                    JSONObject jsonObject2 = jsonArray1.getJSONObject(j);

                                    String mainservicestypeid = jsonObject2.getString("mainservicestypeno");
                                    String mainservicestypename = jsonObject2.getString("mainservicestype_tks_servicena");
                                    String msmainservicestype_tks_descripti = jsonObject2.getString("mainservicestype_tks_descripti");
                                    String msassigned_user_id = jsonObject2.getString("assigned_user_id");
                                    String mscreatedtime = jsonObject2.getString("createdtime");
                                    String msmodifiedtime = jsonObject2.getString("modifiedtime");
                                    String mssource = jsonObject2.getString("source");
                                    String msstarred = jsonObject2.getString("starred");
                                    String mstags = jsonObject2.getString("tags");
                                    String msid = jsonObject2.getString("id");

                                    listDataHeader.add(mainservicestypename);


                                    Log.i(TAG, "(-----------------------)");
                                    Log.i(TAG, "mainservicestypeid " + mainservicestypeid);
                                    Log.i(TAG, "mainservicestypename " + mainservicestypename);
                                    Log.i(TAG, "(-----------------------)");

                                    childitems = new ArrayList<String>();
                                    JSONObject jsonObject3 = jsonObject2.getJSONObject("SubServices");
                                    //boolean success = jsonObject3.getBoolean("success");


                                    JSONArray jsonArray2 = jsonObject3.getJSONArray("List");
                                    for (int k = 0; k < jsonArray2.length(); k++) {
                                        JSONObject jsonObject4 = jsonArray2.getJSONObject(k);

                                        String subservicesno = jsonObject4.getString("subservicesno");
                                        String subservices_tks_subservicesnam = jsonObject4.getString("subservices_tks_subservicesnam");
                                        String assigned_user_id = jsonObject4.getString("assigned_user_id");
                                        String createdtime = jsonObject4.getString("createdtime");
                                        String modifiedtime = jsonObject4.getString("modifiedtime");
                                        String source = jsonObject4.getString("source");
                                        String starred = jsonObject4.getString("starred");
                                        String tags = jsonObject4.getString("tags");
                                        String id = jsonObject4.getString("id");

                                        childitems.add(subservices_tks_subservicesnam);

                                        listDataChild.put(listDataHeader.get(j), childitems);
                                        expListView.setAdapter(listAdapter);
                                    }
                                }
                            }
                        }
                    } else {
                        btnEndWork.setVisibility(View.INVISIBLE);
                        Toast.makeText(EndClean.this, " Clients Data Not Found!Please Select Client's ", Toast.LENGTH_LONG).show();
                    }

                } else if (status.equals("error")) {
                    String errorresult = jsonObject.getString("result");
                    Snackbar.make(findViewById(android.R.id.content), errorresult, Snackbar.LENGTH_LONG).show();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }


        }, error -> {

            CircularProgressDialogue.dismissdialog();
            Log.e(TAG, "Something error " + error.getMessage());

            try {
                NetworkResponse response = error.networkResponse;
                VolleyError responseError = new VolleyError(new String(error.networkResponse.data));
                Log.i(TAG, "res" + String.valueOf(responseError));
                String errorMsg = "";
                String errorString = new String(response.data);
                Log.i("log error", errorString);
                int mstatuscode = response.statusCode;
                Log.i(TAG, String.valueOf(mstatuscode));
                if (mstatuscode == HttpURLConnection.HTTP_NOT_FOUND) {
                    Snackbar.make(findViewById(android.R.id.content), mstatuscode + " Server Not Found", Snackbar.LENGTH_LONG).show();
                } else if (mstatuscode == HttpURLConnection.HTTP_INTERNAL_ERROR) {
                    Snackbar.make(findViewById(android.R.id.content), mstatuscode + " Internal Server Error", Snackbar.LENGTH_LONG).show();

                } else if (response.statusCode == 401) {
                    Snackbar.make(findViewById(android.R.id.content), mstatuscode + " Unauthorized Error", Snackbar.LENGTH_LONG).show();

                } else {
                    Snackbar.make(findViewById(android.R.id.content), "Bad Internet try again!", Snackbar.LENGTH_LONG).show();
                }
            } catch (Exception e) {

            }
            Snackbar.make(findViewById(android.R.id.content), "Something went Wrong..!", Snackbar.LENGTH_LONG).show();

        }) {

            @Override
            protected Map<String, String> getParams() {
                Bundle bundle = getIntent().getExtras();
                Map<String, String> params = new HashMap<>();
                params.put(AppConstants.APIKeys.CLIENT_ID, bundle.getString(AppConstants.BundleKeys.CLIENT_ID));
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(this).add(stringRequest);
    }


   /* private void endWork(final String empid, final String clientidno, final String currentTime, final String finalnitlist, final String someid) {

        CircularProgressDialogue.showDialog(EndClean.this, "", "");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, WebApi.ApiMethods.END_WORK, response -> {
            CircularProgressDialogue.dismissdialog();
            Log.i(TAG, response);
            try {
                JSONObject jsonObject = new JSONObject(response);
                String status = jsonObject.getString("status");
                if (status.equals("success")) {
                    String ServiceList = jsonObject.getString("servicelist");
                    ShowNotification showNotification = new ShowNotification();
                    showNotification.DismissNotification(EndClean.this);

                    btnEndWork.setVisibility(View.INVISIBLE);
                    String timeStamp = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss").format(Calendar.getInstance().getTime());
                    Toast.makeText(EndClean.this, "Your Clean Service Ended at " + timeStamp, Toast.LENGTH_SHORT).show();

                    Snackbar.make(findViewById(android.R.id.content), " Punch out ! Work Finished!", Snackbar.LENGTH_INDEFINITE).show();
                    new Handler().postDelayed(() -> Snackbar.make(findViewById(android.R.id.content), " Image Loaded successfully!", Snackbar.LENGTH_INDEFINITE).show(),3000);
                    new Handler().postDelayed(() -> {
                        navigateEnd(DashboardActivity.class);
                    }, 5000);

                } else if (status.equals("error")) {
                    String errorresult = jsonObject.getString("result");
                    Snackbar.make(findViewById(android.R.id.content), errorresult, Snackbar.LENGTH_LONG).show();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }, error -> {

            CircularProgressDialogue.dismissdialog();
            Log.e(TAG, "Something error " + error.getMessage());

            try {
                NetworkResponse response = error.networkResponse;
                VolleyError responseError = new VolleyError(new String(error.networkResponse.data));
                Log.i(TAG, "res" + String.valueOf(responseError));
                String errorMsg = "";

                String errorString = new String(response.data);
                Log.i("log error", errorString);
                int mstatuscode = response.statusCode;
                Log.i(TAG, String.valueOf(mstatuscode));
                if (mstatuscode == HttpURLConnection.HTTP_NOT_FOUND) {
                    Snackbar.make(findViewById(android.R.id.content), mstatuscode + " Server Not Found", Snackbar.LENGTH_LONG).show();
                } else if (mstatuscode == HttpURLConnection.HTTP_INTERNAL_ERROR) {
                    Snackbar.make(findViewById(android.R.id.content), mstatuscode + " Internal Server Error", Snackbar.LENGTH_LONG).show();

                } else if (response.statusCode == 401) {
                    Snackbar.make(findViewById(android.R.id.content), mstatuscode + " Unauthorized Error", Snackbar.LENGTH_LONG).show();

                } else {
                    Snackbar.make(findViewById(android.R.id.content), "Bad Internet try again!", Snackbar.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.i(TAG, "cause" + String.valueOf(e.getCause()));
                Log.i(TAG, "Local" + String.valueOf(e.getLocalizedMessage()));
                Log.i(TAG, "Message" + String.valueOf(e.getMessage()));
                Log.i(TAG, "Message" + String.valueOf(e.toString()));

            }
            Snackbar.make(findViewById(android.R.id.content), "Something went Wrong..Please Check internet connection!", Snackbar.LENGTH_LONG).show();


        }) {


            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(AppConstants.APIKeys.CLIENT_ID, clientidno);
                params.put(AppConstants.APIKeys.EMP_ID, empid);
                params.put(AppConstants.APIKeys.PUNCH_OUT, currentTime);
                params.put(AppConstants.APIKeys.SERVICE_LIST, finalnitlist);
                params.put(AppConstants.APIKeys.ID, someid);
                params.put(AppConstants.APIKeys.IMAGE, mImage);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(this).add(stringRequest);

    }*/

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onDestroy() {
        CircularProgressDialogue.dismissdialog();
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                File f = new File(Environment.getExternalStorageDirectory().toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals("temp.jpg")) {
                        f = temp;
                        break;
                    }
                }

                try {
                    Bitmap bitmap;
                    BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                    bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(),
                            bitmapOptions);
                    String path = android.os.Environment
                            .getExternalStorageDirectory()
                            + File.separator
                            + "Phoenix" + File.separator + "default";
                    f.delete();
                    OutputStream outFile = null;
                    mImageFile = new File(path, System.currentTimeMillis() + ".jpg");
                    if (!mImageFile.exists()) {
                        mImageFile.createNewFile();
                    }
                    try {
                        outFile = new FileOutputStream(mImageFile);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 85, outFile);
                        //mImage = getStringImage(bitmap);
                        mImageFile = StorageUtility.getFileFromBitmap(this, bitmap);
                        outFile.flush();
                        outFile.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == 2) {
                try {
                    Uri selectedImage = data.getData();
                    InputStream inputStream = getContentResolver().openInputStream(selectedImage);
                    Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                   // mImage = getStringImage(bitmap);
                    mImageFile = StorageUtility.getFileFromBitmap(this, bitmap);
                } catch (Exception e) {
                    LogUtility.logInfo(TAG, "Exception occurred while making bitmap.");
                }
            }
        }
    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.endendbutton:
                if ((mServicesList.size() > 0)) {
                    mCurrentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
                    Log.i(TAG, "Array" + mServicesList);
                    mServicesDone = Arrays.toString(mServicesList.toArray()).replace("[", "").replace("]", "");

                    mServicesList.clear();

                    //endWork(mEmpId, mClientId, mCurrentTime, mServicesDone, mTransactionId);
                    endCleanWork(mEmpId, mClientId, mCurrentTime, mServicesDone, mTransactionId);

                    ShowNotification showNotification = new ShowNotification();
                    showNotification.DismissNotification(EndClean.this);

                } else {
                    Toast.makeText(EndClean.this, "Please Select atleast one Service", Toast.LENGTH_SHORT).show();
                    ShowNotification showNotification = new ShowNotification();
                    showNotification.DismissNotification(EndClean.this);
                }

                break;
        }
    }

    private void endCleanWork(String empId, String clientID, String currentTime, String serviceDone, String transactionId) {
        CircularProgressDialogue.showDialog(this, null, null);
        RequestBody requestFile;
        MultipartBody.Part imageFile = null;
        if (mImageFile != null) {
            requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), mImageFile);
            imageFile = MultipartBody.Part.createFormData("image", mImageFile.getName(), requestFile);
        }

        RequestBody requestBodyEmpId = RequestBody.create(okhttp3.MultipartBody.FORM, empId);
        RequestBody requestBodyClientId = RequestBody.create(okhttp3.MultipartBody.FORM, clientID);
        RequestBody requestBodyPunchOutTime = RequestBody.create(okhttp3.MultipartBody.FORM, currentTime);
        RequestBody requestBodyServiceDone = RequestBody.create(okhttp3.MultipartBody.FORM, serviceDone);
        RequestBody requestBodyTransactionId = RequestBody.create(okhttp3.MultipartBody.FORM, transactionId);

        mEndWorkViewModel.endWord(requestBodyTransactionId, requestBodyPunchOutTime, requestBodyEmpId, requestBodyClientId,
                requestBodyServiceDone, imageFile, new EndWorkResponseCallback() {
                    @Override
                    public void onSuccess(@org.jetbrains.annotations.Nullable EndWorkResponseModel response) {
                        CircularProgressDialogue.dismissdialog();
                        if (response != null) {
                            if (response.status.equalsIgnoreCase(WebApi.Status.SUCCESS)) {

                                btnEndWork.setVisibility(View.INVISIBLE);
                                String timeStamp = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss").format(Calendar.getInstance().getTime());
                                Toast.makeText(EndClean.this, "Your Clean Service Ended at " + timeStamp, Toast.LENGTH_SHORT).show();

                                Snackbar.make(findViewById(android.R.id.content), " Punch out ! Work Finished!", Snackbar.LENGTH_INDEFINITE).show();
                                new Handler().postDelayed(() -> Snackbar.make(findViewById(android.R.id.content), " Image Loaded successfully!", Snackbar.LENGTH_INDEFINITE).show(), 3000);
                                new Handler().postDelayed(() -> {
                                    navigateEnd(DashboardActivity.class);
                                }, 5000);

                            } else {
                                // String errorresult = jsonObject.getString("result");
                                Snackbar.make(findViewById(android.R.id.content), "Something went wrong", Snackbar.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFail(@org.jetbrains.annotations.Nullable Throwable throwable) {
                        CircularProgressDialogue.dismissdialog();
                        LogUtility.logInfo(TAG, throwable.getMessage());
                    }
                });
    }
}
