package com.rightchoice.rightclean.DashboardPackage

import android.content.Intent
import android.graphics.*
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.databinding.library.baseAdapters.BR
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.FragmentManager
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.Volley
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.rightchoice.rightclean.BuildConfig
import com.rightchoice.rightclean.CircularProgressdialog.CircularProgressDialogue
import com.rightchoice.rightclean.ExecutiveProfilePackage.ExecutiveProfile
import com.rightchoice.rightclean.ExecutiveProfilePackage.Settings
import com.rightchoice.rightclean.JsonNetworkPackage.MultipartRequest
import com.rightchoice.rightclean.R
import com.rightchoice.rightclean.StartCameraPackage.StartCamera
import com.rightchoice.rightclean.activity_view_models.DashboardActivityViewModel
import com.rightchoice.rightclean.activity_view_models.navigators.DashboardActivityNavigator
import com.rightchoice.rightclean.databinding.ActivityDashboardBinding
import com.rightchoice.rightclean.model.utilities.AppConstants
import com.rightchoice.rightclean.model.utilities.ImageUtility
import com.rightchoice.rightclean.views.activities.BaseActivity2
import com.rightchoice.rightclean.views.activities.ProfileActivity
import com.rightchoice.rightclean.views.fragments.HomeFragment
import kotlinx.android.synthetic.main.activity_filter.*
import kotlinx.android.synthetic.main.content_dashboard.*
import org.json.JSONException
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL
import java.util.*

class DashboardActivity : BaseActivity2<ActivityDashboardBinding,DashboardActivityViewModel>(),DashboardActivityNavigator, NavigationView.OnNavigationItemSelectedListener {

    override val viewModel = DashboardActivityViewModel::class.java

    override fun getBindingVariable() = BR.viewModel

    override val layoutId = R.layout.activity_dashboard;

    override fun initInstances() {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectedViewModel.setNavigator(this)

        initViews()
        setSupportActionBar(toolbar)

        clientmodelarraylist = ArrayList()

        setDrawer()
    }


    companion object {
        private val TAG = DashboardActivity::class.java.simpleName
    }

    //#region Views
    private lateinit var ivProfile: ImageView
    private lateinit var tvName: TextView
    private lateinit var tvEmail: TextView
    private lateinit var tvHome : TextView
    private lateinit var toolbar: Toolbar
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var navigationView: NavigationView
    private lateinit var view: View
    //#endregion Views

    private val dashboardViewModel = DashboardViewModel();

    var client_detail_data_models: ArrayList<Client_Detail_Data_Model>? = null
    private var clients_grid_adapter: Clients_Grid_Adapter? = null
    var listview: ListView? = null
    private var clientmodelarraylist: ArrayList<Client_Detail_Data_Model>? = null
    var id: String? = null
    var url: String? = null


    /*   private fun getClientsDetails(email: String?) {
           CircularProgressDialogue.showDialog(this, "", "")
           val requestBody: RequestBody = MultipartBody.Builder()
                   .setType(MultipartBody.FORM)
                   .addFormDataPart(AppConstants.APIKeys.USER_NAME, email)
                   .build()

           mWorkViewModel.getClientsDetails(requestBody, object : GetClientDetailsCallback {
               override fun onSuccess(response: GetClientsDetailsResponseModel?) {
                   CircularProgressDialogue.dismissdialog()
                   if (response != null) {
                       if (response.status.equals(WebApi.Status.SUCCESS, true)) {
                           populateData(response)
                       }
                   } else LogUtility.logInfo(TAG, "Response was found NULL!")
               }

               override fun onFail(throwable: Throwable?) {
                   CircularProgressDialogue.dismissdialog()
                   LogUtility.logInfo(TAG, throwable?.message.toString())
               }
           })
       }*/

/*    private fun populateData(response: GetClientsDetailsResponseModel) {
        rvClients.adapter = ClientsAdapter(response.data.clientsData) {
            val clientData: ClientData = response.data.clientsData[it]
            getPendingWork(clientData.id, mSignInActivityViewModel.getEmployeeId())
        }
    }

    private fun getPendingWork(clientID: String?, empID: String?) {
        CircularProgressDialogue.showDialog(this@DashboardActivity, "", "")
        val requestBody: RequestBody = MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart(AppConstants.APIKeys.CLIENT_ID, clientID)
                .addFormDataPart(AppConstants.APIKeys.EMP_ID, empID)
                .build()

        mWorkViewModel.getPendingWork(requestBody, object : GetPendingWorkCallback {
            override fun onSuccess(response: GetPendingWorkResponseModel?) {
                CircularProgressDialogue.dismissdialog()
                if (response != null) {
                    if (response.status.equals(WebApi.Status.SUCCESS)) {
                        var bundle = Bundle()
                        bundle.putString(AppConstants.BundleKeys.CLIENT_ID, clientID)
                        if (response.isWorkPending) {
                            bundle.putString(AppConstants.BundleKeys.TRANSACTION_ID, response.result[0].transactionid)
                            navigate(EndClean::class.java, bundle)
                        } else {
                            navigate(Details::class.java, bundle)
                        }
                    } else {
                        Snackbar.make(findViewById(android.R.id.content), response.result.toString(), Snackbar.LENGTH_LONG).show()
                    }
                } else {
                    LogUtility.logInfo(TAG, "Response was found NULL!")
                }
            }

            override fun onFail(throwable: Throwable?) {
                CircularProgressDialogue.dismissdialog()
                LogUtility.logInfo(TAG, throwable?.message.toString())
            }

        })
    }*/

    override fun onDestroy() {
        CircularProgressDialogue.dismissdialog()
        super.onDestroy()
    }

    private fun getbitmapfromimageview(profilepic: String): Bitmap? {
        return try {
            val url = URL(profilepic)
            val connection = url.openConnection() as HttpURLConnection
            connection.doInput = true
            connection.connect()
            val input = connection.inputStream
            BitmapFactory.decodeStream(input)
        } catch (e: IOException) {
            // Log exception
            null
        }
    }

    private fun UPloadtoserver(b: Bitmap) {
        CircularProgressDialogue.showDialog(this@DashboardActivity, "", "")
        val multipartRequest: MultipartRequest = object : MultipartRequest(Method.POST, url , Response.Listener { response ->
            CircularProgressDialogue.dismissdialog()
            try {
                client_detail_data_models = ArrayList()
                val obj = JSONObject(String(response.data))
                //Toast.makeText(getApplicationContext(), obj.getString("status"), Toast.LENGTH_SHORT).show();
                val Status = obj.getString("status")
                Log.i(TAG, obj.toString())
                if (Status == "error") {
                    val Result = obj.getString("result")
                    Log.i(TAG, Result)
                    when (Result) {
                        "false" -> {
                            Snackbar.make(findViewById(android.R.id.content), "New User !Please Contact Admin For More Details", Snackbar.LENGTH_LONG).show()
                            Handler().postDelayed({
                                val intent = Intent(this@DashboardActivity, StartCamera::class.java)
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                startActivity(intent)
                            }, 2000)
                        }
                        "deleted user" -> {
                            Snackbar.make(findViewById(android.R.id.content), "$Result Please Contact Admin", Snackbar.LENGTH_LONG).show()
                            Handler().postDelayed({
                                val intent = Intent(this@DashboardActivity, StartCamera::class.java)
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                startActivity(intent)
                            }, 2000)
                        }
                        "inactive user" -> {
                            Snackbar.make(findViewById(android.R.id.content), "$Result Please Contact Admin", Snackbar.LENGTH_LONG).show()
                            Handler().postDelayed({
                                val intent = Intent(this@DashboardActivity, StartCamera::class.java)
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                startActivity(intent)
                            }, 2000)
                        }
                        "please recognize your image" -> Handler().postDelayed({
                            val intent = Intent(this@DashboardActivity, StartCamera::class.java)
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                            startActivity(intent)
                        }, 1000)
                        "Image not found" -> {
                            Snackbar.make(findViewById(android.R.id.content), "Image not found Contact Admin", Snackbar.LENGTH_LONG).show()
                            CircularProgressDialogue.showDialog(this@DashboardActivity, "", "")
                            Handler().postDelayed({
                                CircularProgressDialogue.dismissdialog()
                                val intent = Intent(this@DashboardActivity, StartCamera::class.java)
                                startActivity(intent)
                                finish()
                            }, 2000)
                        }
                    }
                } else if (Status == "success") {
                    val id = obj.getString("id")
                    val emp = obj.getString("Emp")
                    val url = obj.getString("url")
                    Log.i(TAG, "id$id")
                    Log.i(TAG, "Emp$emp")
                    Log.i(TAG, "url$url")
                    val jsonObject = obj.getJSONObject("Client")
                    val status = jsonObject.getBoolean("success")
                    if (status) {
                        val jsonArray = jsonObject.getJSONArray("result")
                        if (jsonArray.length() > 0) {
                            for (i in 0 until jsonArray.length()) {
                                val jsonObject1 = jsonArray.getJSONObject(i)
                                val client_detail_data_model = Client_Detail_Data_Model()
                                val clientsno = jsonObject1.getString("clientsno")
                                val clients_tks_clientname = jsonObject1.getString("clients_tks_clientname")
                                Log.i(TAG, clients_tks_clientname)
                                val assigned_user_id = jsonObject1.getString("assigned_user_id")
                                val createdtime = jsonObject1.getString("createdtime")
                                val modifiedtime = jsonObject1.getString("modifiedtime")
                                val source = jsonObject1.getString("source")
                                val starred = jsonObject1.getString("starred")
                                val tags = jsonObject1.getString("tags")
                                /*String cf_906 = jsonObject1.getString("cf_906");*/
                                /*String cf_908 = jsonObject1.getString("cf_908");*/
                                /*String cf_910 = jsonObject1.getString("cf_910");*/
                                /*String cf_912 = jsonObject1.getString("cf_912");*/
                                /*String cf_914 = jsonObject1.getString("cf_914");*/
                                /*String cf_916 = jsonObject1.getString("cf_916");*/
                                /*String cf_918 = jsonObject1.getString("cf_918");*/
                                /*String cf_920 = jsonObject1.getString("cf_918");*/
                                /*String cf_922 = jsonObject1.getString("cf_918");*/
                                /*String cf_924 = jsonObject1.getString("cf_924");*/
                                val cf_1072 = jsonObject1.getString("cf_1072")
                                val cf_1074 = jsonObject1.getString("cf_1074")
                                val cf_1076 = jsonObject1.getString("cf_1076")
                                val cf_1078 = jsonObject1.getString("cf_1078")
                                val cf_1080 = jsonObject1.getString("cf_1080")
                                val cf_1082 = jsonObject1.getString("cf_1082")
                                val cf_1084 = jsonObject1.getString("cf_1084")
                                val cf_1086 = jsonObject1.getString("cf_1086")
                                val cf_1146 = jsonObject1.getString("cf_1146")
                                val cf_1148 = jsonObject1.getString("cf_1148")
                                val id1 = jsonObject1.getString("id")
                                client_detail_data_model.setClentno(clientsno)
                                client_detail_data_model.setClientname(clients_tks_clientname)
                                client_detail_data_model.setAssigneduser(assigned_user_id)
                                client_detail_data_model.setCreatedtime(createdtime)
                                client_detail_data_model.setModifiedtime(modifiedtime)
                                client_detail_data_model.setSource(source)
                                client_detail_data_model.setStarred(starred)
                                client_detail_data_model.setTags(tags)
                                client_detail_data_model.setCf_1072(cf_1072)
                                client_detail_data_model.setCf_1074(cf_1074)
                                client_detail_data_model.setCf_1076(cf_1076)
                                client_detail_data_model.setCf_1078(cf_1078)
                                client_detail_data_model.setCf_1080(cf_1080)
                                client_detail_data_model.setCf_1082(cf_1082)
                                client_detail_data_model.setCf_1084(cf_1084)
                                client_detail_data_model.setCf_1086(cf_1086)
                                client_detail_data_model.setCf_1146(cf_1146)
                                client_detail_data_model.setCf_1148(cf_1148)
                                client_detail_data_model.setId1(id1)
                                Log.i(TAG, "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
                                Log.i(TAG, "Number $clientsno")
                                Log.i(TAG, "Name $clients_tks_clientname")
                                Log.i(TAG, "assigned_user_id $assigned_user_id")
                                Log.i(TAG, "createdtime $createdtime")
                                Log.i(TAG, "id1 $id1")
                                Log.i(TAG, "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
                                clientmodelarraylist!!.add(client_detail_data_model)
                            }
                            setupgridClients()
                        } else {
                            Toast.makeText(this@DashboardActivity, "No Clients! Please add client's", Toast.LENGTH_LONG).show()
                        }
                    } else if (!status) {
                        Snackbar.make(findViewById(android.R.id.content), "Something went wrong..Please Try again Later!", Snackbar.LENGTH_LONG).show()
                    }
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }, Response.ErrorListener { error ->

            //Something Went Wrong We are Working...
            CircularProgressDialogue.dismissdialog()
            Log.e(TAG, "Something error " + error.message)
            try {
                val response = error.networkResponse
                val responseError = VolleyError(String(error.networkResponse.data))
                Log.i(TAG, "res$responseError")
                val errorMsg = ""
                val errorString = String(response.data)
                Log.i("log error", errorString)
                val mstatuscode = response.statusCode
                Log.i(TAG, mstatuscode.toString())
                if (mstatuscode == HttpURLConnection.HTTP_NOT_FOUND) {
                    Snackbar.make(findViewById(android.R.id.content), "$mstatuscode Server Not Found", Snackbar.LENGTH_LONG).show()
                } else if (mstatuscode == HttpURLConnection.HTTP_INTERNAL_ERROR) {
                    Snackbar.make(findViewById(android.R.id.content), "$mstatuscode Internal Server Error", Snackbar.LENGTH_LONG).show()
                } else if (response.statusCode == 401) {
                    Snackbar.make(findViewById(android.R.id.content), "$mstatuscode Unauthorized Error", Snackbar.LENGTH_LONG).show()
                } else {
                    Snackbar.make(findViewById(android.R.id.content), "Bad Internet try again!", Snackbar.LENGTH_LONG).show()
                }
            } catch (e: Exception) {
                e.printStackTrace()
                Log.i(TAG, "cause" + e.cause.toString())
                Log.i(TAG, "Local" + e.localizedMessage.toString())
                Log.i(TAG, "Message" + e.message.toString())
                Log.i(TAG, "Message$e")
            }
            Snackbar.make(findViewById(android.R.id.content), "Something went Wrong..!", Snackbar.LENGTH_LONG).show()
        }) {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                //params.put("tags", tags);
                return HashMap()
            }

            override fun getByteData(): Map<String, DataPart> {
                val params: MutableMap<String, DataPart> = HashMap()
                val imagename = System.currentTimeMillis()
                params["image"] = DataPart("$imagename.jpeg", getFileDataFromDrawable(b))
                return params
            }
        }
        multipartRequest.retryPolicy = DefaultRetryPolicy(
                0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        multipartRequest.setShouldCache(false)
        Volley.newRequestQueue(this).add(multipartRequest)
    }

    private fun setupgridClients() {
        clients_grid_adapter = Clients_Grid_Adapter(this, clientmodelarraylist)
        listview!!.adapter = clients_grid_adapter
    }

    fun getFileDataFromDrawable(bitmap: Bitmap?): ByteArray? {
        return if (bitmap != null) {
            val byteArrayOutputStream = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream)
            Log.i(TAG, byteArrayOutputStream.toByteArray().toString())
            byteArrayOutputStream.toByteArray()
        } else {
            Log.i(TAG, "Bitmap is Empty ")
            null
        }
    }

    private fun getCircularBitmap(bitmap: Bitmap): Bitmap {
        val output: Bitmap
        output = if (bitmap.width > bitmap.height) {
            Bitmap.createBitmap(bitmap.height, bitmap.height, Bitmap.Config.ARGB_8888)
        } else {
            Bitmap.createBitmap(bitmap.width, bitmap.width, Bitmap.Config.ARGB_8888)
        }
        val canvas = Canvas(output)
        val color = -0xbdbdbe
        val paint = Paint()
        val rect = Rect(0, 0, bitmap.width, bitmap.height)
        var r = 0f
        r = if (bitmap.width > bitmap.height) {
            bitmap.height / 2.toFloat()
        } else {
            bitmap.width / 2.toFloat()
        }
        paint.isAntiAlias = true
        canvas.drawARGB(0, 0, 0, 0)
        paint.color = color
        canvas.drawCircle(r, r, r, paint)
        paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_IN)
        canvas.drawBitmap(bitmap, rect, rect, paint)
        return output
    }

    override fun onBackPressed() {
        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.dashboard, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.action_settings) {
            startActivity(Intent(this@DashboardActivity,FilterActivity::class.java))
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.profile) {
            navigateEnd(ExecutiveProfile::class.java)
        } else if (id == R.id.setting) {
            navigateEnd(Settings::class.java)
        } else if (id == R.id.logout) {
            //showLogoutDialog()
        } else if (id == R.id.nav_share) {
            share()
        }
        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        drawer.closeDrawer(GravityCompat.START)
        return true
    }

    private fun share() {
        try {
            val shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "RightClean")
            var shareMessage = "\nLet me recommend you this application\n\n"
            shareMessage = """
                ${shareMessage}https://play.google.com/store/apps/details?id=${BuildConfig.APPLICATION_ID}
                
                
                """.trimIndent()
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
            startActivity(Intent.createChooser(shareIntent, "choose one"))
        } catch (e: Exception) {
            //e.toString();
        }
    }

    /*   private fun showLogoutDialog() {
           GeneralDialogues.show2ButtonsDialogue(this, object : DialogueClickListener2 {
               override fun onPositiveButtonClicked(dialogInterface: DialogInterface) {
                   stopService(Intent(this@DashboardActivity, MyLocationServices::class.java))
                   mWorkViewModel!!.getSharedPreferences().clearPreferences()
                   CircularProgressDialogue.showDialog(this@DashboardActivity, "", "")
                   Handler().postDelayed({
                       CircularProgressDialogue.dismissdialog()
                       navigateEnd(LoginOptionsActivity::class.java)
                   }, 2000)
               }

               override fun onNegativeButtonClicked(dialogInterface: DialogInterface) {
                   dialogInterface.dismiss()
               }
           }, getString(R.string.title_logout), null, null)
       }*/

    //#region Private Methods
    private fun setDrawer() {
        val toggle = ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        navigationView.setNavigationItemSelectedListener(this)
    }

    private fun initViews() {

        val homeFragment = HomeFragment()
        val manager: FragmentManager = supportFragmentManager
        manager.beginTransaction().replace(R.id.dashboard_container,homeFragment).commit()

        val loginDetails = dashboardViewModel.getLoginResponse()

        toolbar = findViewById(R.id.toolbar)
        drawerLayout = findViewById(R.id.drawer_layout)
        navigationView = findViewById(R.id.nav_view)

        view = navigationView.getHeaderView(0)

        tvName = view.findViewById(R.id.tv_user_name)
        tvEmail = view.findViewById(R.id.tv_user_email)
        ivProfile = view.findViewById(R.id.iv_profile)
        tvHome = view.findViewById(R.id.tvHome)

        tvHome.apply {
            setOnClickListener {
                val homeFragment = HomeFragment()
                val manager: FragmentManager = supportFragmentManager
                manager.beginTransaction().replace(R.id.dashboard_container,homeFragment).commit()
                CloseDrawer()
            }
        }

        ivProfile.apply {
            ImageUtility.loadImage(this@DashboardActivity,
                    AppConstants.BASE_URL_MAIN_ROUTE + loginDetails.data.image, this)

            setOnClickListener(View.OnClickListener {
                startActivity(Intent(this@DashboardActivity,ProfileActivity::class.java))
            })
            tvName.text = loginDetails.data.name
            tvEmail.text = loginDetails.data.userData.email
        }

    }

    private fun CloseDrawer() {
        drawerLayout.closeDrawer(Gravity.LEFT)
    }

    //#endregion Private Methods


}