package com.rightchoice.rightclean.DashboardPackage;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Spinner;

import com.rightchoice.rightclean.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

public class FilterActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    private static final String TAG = FilterActivity.class.getSimpleName();
    Spinner spinner ;
    Calendar calendar;
    String fututedate,currentDate;
    DateFormat dateFormat;
    Date today , tomorrow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        Objects.requireNonNull(getSupportActionBar()).hide();

         spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.days, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);


        calendar = Calendar.getInstance();
         today = calendar.getTime();

        calendar.add(Calendar.DAY_OF_YEAR, 1);
         tomorrow = calendar.getTime();

        dateFormat = new SimpleDateFormat("dd/MM/yyyy");



    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, final int position, long id)
    {

        if(position == 0)
        {

        }else if(position == 1)
        {
            /*dateFormat = new SimpleDateFormat("MM/dd/yyyy");
            currentDate = dateFormat.format(calendar.getTime());
            //dateView.setText(currentDate.toString().substring(0, 10));
            */

            String todayAsString = dateFormat.format(today);

            Log.i(TAG,todayAsString);

            spinner.setSelection(position);
        }else if(position == 2)
        {
            String tomorrowAsString = dateFormat.format(tomorrow);
            Log.i(TAG,tomorrowAsString);
            spinner.setSelection(position);
        }else if(position == 3)
        {
            Opencalendar();
            spinner.setSelection(position);


        }else if(position == 4)
        {
            spinner.clearFocus();
            spinner.setSelection(position);
        }


    }

    private void Opencalendar()
    {
        Calendar c=Calendar.getInstance();
        Integer month=c.get(Calendar.MONTH);
        Integer day=c.get(Calendar.DAY_OF_MONTH);
        Integer year=c.get(Calendar.YEAR);

        DatePickerDialog datePickerDialog =new DatePickerDialog(FilterActivity.this, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth)
            {
               // et_from.setText(dayOfMonth+"/"+month+"/"+year);

                fututedate = dayOfMonth+"/"+month+"/"+year ;
                Log.i(TAG,fututedate);


            }
        },year,month,day);
        datePickerDialog.show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent)
    {

    }
}