package com.rightchoice.rightclean.DashboardPackage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.rightchoice.rightclean.R;

import java.util.ArrayList;

public class Clients_Grid_Adapter extends BaseAdapter
{
    private Context context;
    private ArrayList<Client_Detail_Data_Model> client_detail_data_models;

    public Clients_Grid_Adapter(Context context, ArrayList<Client_Detail_Data_Model> client_detail_data_models) {
        this.context = context;
        this.client_detail_data_models = client_detail_data_models;
    }


    @Override
    public int getViewTypeCount() {
        if(getCount() > 0){
            return getCount();
        }else{
            return super.getViewTypeCount();
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;

    }

    @Override
    public int getCount() {
        return client_detail_data_models.size();
    }

    @Override
    public Object getItem(int i) {
        return client_detail_data_models.get(i);

    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertview, ViewGroup parent)
    {
        Clients_Grid_Adapter.ViewHolder holder;

        if(convertview == null)
        {

            holder = new Clients_Grid_Adapter.ViewHolder();
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertview = layoutInflater.inflate(R.layout.custom_gridview_layout, null, true);

            holder.imageView =convertview.findViewById(R.id.defaultimage);
            holder.personname = convertview.findViewById(R.id.clientname);
            holder.empid = convertview.findViewById(R.id.clientid);
            holder.address = convertview.findViewById(R.id.address);
            holder.contactno = convertview.findViewById(R.id.phonenumber);
            holder.emailid = convertview.findViewById(R.id.emailid);
            holder.time = convertview.findViewById(R.id.httime);

            convertview.setTag(holder);



            holder.imageView.setImageResource(R.drawable.ic_person_black_24dp);
            holder.personname.setText("Name: "+ client_detail_data_models.get(position).getClientname());
            holder.empid.setText("Id: "+client_detail_data_models.get(position).getClentno());
            holder.time.setText("Time: "+client_detail_data_models.get(position).getCf_1146());
            holder.address.setText("Address: "+client_detail_data_models.get(position).getCf_1076());
            holder.contactno.setText("Contact No: "+client_detail_data_models.get(position).getCf_1074());
            holder.emailid.setText("Expected Hour: "+client_detail_data_models.get(position).getCf_1082()); //cf_916


        }

        return convertview;


    }

    private class ViewHolder {

        TextView personname,empid,address,contactno,emailid,time;
        ImageView imageView;


    }
}
