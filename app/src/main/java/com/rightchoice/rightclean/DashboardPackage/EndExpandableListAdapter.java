package com.rightchoice.rightclean.DashboardPackage;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.FileProvider;

import com.rightchoice.rightclean.CameraPackage.Image;
import com.rightchoice.rightclean.R;
import com.rightchoice.rightclean.views.activities.MainActivity;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;

import static android.content.Context.MODE_PRIVATE;

public class EndExpandableListAdapter extends BaseExpandableListAdapter implements View.OnClickListener {

    private Context _context;
    private List<String> _listDataHeader;
    private HashMap<String, List<String>> _listDataChild;
    HashSet<String> serviceslist = new HashSet();
    SharedPreferences.Editor editor;

    private List<Integer> parentPositionsList, childPositionsList;
    private SelectedSubServiceProvider mSelectedSubServiceProvider;

    public EndExpandableListAdapter(Context _context, List<String> _listDataHeader, HashMap<String, List<String>> _listDataChild,
                                    SelectedSubServiceProvider provider) {
        this._context = _context;
        this._listDataHeader = _listDataHeader;
        this._listDataChild = _listDataChild;
        parentPositionsList = new ArrayList<>();
        childPositionsList = new ArrayList<>();
        mSelectedSubServiceProvider = provider;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return Objects.requireNonNull(this._listDataChild.get(this._listDataHeader.get(groupPosition)))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final String childText = (String) getChild(groupPosition, childPosition);
        editor = _context.getSharedPreferences("Checklist", MODE_PRIVATE).edit();
        serviceslist.clear();
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.custom_row_item, null);
        }
        ImageView ivSubService = convertView.findViewById(R.id.checkBox);
        ImageButton ibUploadImage = convertView.findViewById(R.id.uploadpic);
        ConstraintLayout clMainContainer = convertView.findViewById(R.id.cl_container);


        final TextView textView = convertView.findViewById(R.id.txtName);
        textView.setText(childText);

        clMainContainer.setOnClickListener(v -> {
            if (parentPositionsList.contains(groupPosition) && childPositionsList.contains(childPosition)) {
                parentPositionsList.remove(parentPositionsList.indexOf(groupPosition));
                childPositionsList.remove(childPositionsList.indexOf(childPosition));
                ivSubService.setImageResource(R.drawable.uncheck);
                mSelectedSubServiceProvider.onSelectedSubService(false, childText);
            } else {
                parentPositionsList.add(groupPosition);
                childPositionsList.add(childPosition);
                ivSubService.setImageResource(R.drawable.checkbox);
                mSelectedSubServiceProvider.onSelectedSubService(true, childText);
            }
        });

        if (parentPositionsList.contains(groupPosition) && childPositionsList.contains(childPosition)) {
            ivSubService.setImageResource(R.drawable.checkbox);
        } else {
            ivSubService.setImageResource(R.drawable.uncheck);
        }

        ibUploadImage.setOnClickListener(view -> {
            selectImage();
        });

        return convertView;
    }


    @Override
    public int getChildrenCount(int groupPosition) {
        return Objects.requireNonNull(this._listDataChild.get(this._listDataHeader.get(groupPosition))).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }


    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.end_custom_service_head, null);
        }

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.end_headings);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);
        ExpandableListView eLV = (ExpandableListView) parent;
        eLV.expandGroup(groupPosition);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


    @Override
    public void onClick(View v) {

    }

    public void selectImage() {
        EndClean activity = (EndClean) _context;
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(_context);
        builder.setTitle("Add Photo!");
        builder.setItems(options, (dialog, item) -> {
            if (options[item].equals("Take Photo")) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File f = new File(Environment.getExternalStorageDirectory(), "temp.jpg");

                Uri imageUri = FileProvider.getUriForFile(
                        _context,
                        "com.rightchoice.rightclean.provider", //(use your app signature + ".provider" )
                        f);

                intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                activity.startActivityForResult(intent, 1);
            } else if (options[item].equals("Choose from Gallery")) {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                activity.startActivityForResult(intent, 2);
            } else if (options[item].equals("Cancel")) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public interface SelectedSubServiceProvider {
        void onSelectedSubService(boolean isSelected, String subServiceName);
    }
}