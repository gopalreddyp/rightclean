package com.rightchoice.rightclean.DashboardPackage;

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.rightchoice.rightclean.model.repository.webservices.callbacks.SendEmployeeLocationsCallback;
import com.rightchoice.rightclean.model.repository.webservices.pojo.response.EmployeeLocationResponseModel;
import com.rightchoice.rightclean.model.utilities.AppConstants;
import com.rightchoice.rightclean.model.utilities.LogUtility;
import com.rightchoice.rightclean.view_models.EmployeeLocationViewModel;
import com.rightchoice.rightclean.view_models.LoginViewModel;

import java.util.Timer;
import java.util.TimerTask;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class MyLocationServices extends Service implements LocationListener {

    private static final String TAG = "MyLocationServices";

    boolean isGPSEnable = false;
    boolean isNetworkEnable = false;
    double latitude, longitude;
    LocationManager locationManager;
    Location location;
    private Handler mHandler = new Handler();
    private Timer mTimer = null;
    long notify_interval = 1000 * 60;
    public static String str_receiver = "com.rightchoice.rightclean.DashboardPackage.Details";
    Intent intent;
    private EmployeeLocationViewModel mEmployeeLocationViewModel;
    private LoginViewModel mLoginViewModel;

    private String mClientId;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }



    @Override
    public void onCreate() {
        super.onCreate();

        mTimer = new Timer();
        mTimer.schedule(new TimerTaskToGetLocation(), 10, notify_interval);
        intent = new Intent(str_receiver);
        mEmployeeLocationViewModel = new EmployeeLocationViewModel(getApplication());
        mLoginViewModel = new LoginViewModel(getApplication());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        mClientId = intent.getStringExtra(AppConstants.BundleKeys.CLIENT_ID);
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onLocationChanged(Location location) {
        LogUtility.logInfo(TAG, "Location Changed "+ location.getLatitude() + " " +location.getLongitude());

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        LogUtility.logInfo(TAG, "onStatusChanged");

    }

    @Override
    public void onProviderEnabled(String provider) {
        LogUtility.logInfo(TAG, "onProviderEnabled");

    }

    @Override
    public void onProviderDisabled(String provider) {
        LogUtility.logInfo(TAG, "onProviderDisabled");
    }


    private void fn_getlocation() {
        locationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        isGPSEnable = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetworkEnable = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (!isGPSEnable && !isNetworkEnable) {

        } else {

            if (isNetworkEnable) {
                location = null;
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, this);
                if (locationManager != null) {
                    location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    if (location != null) {

                        Log.e("latitude", location.getLatitude() + "");
                        Log.e("longitude", location.getLongitude() + "");

                        latitude = location.getLatitude();
                        longitude = location.getLongitude();
                        fn_update(location);
                    }
                }

            }


            if (isGPSEnable) {
                location = null;
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, this);
                if (locationManager != null) {
                    location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    if (location != null) {
                        Log.e("latitude", location.getLatitude() + "");
                        Log.e("longitude", location.getLongitude() + "");
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();
                        fn_update(location);
                    }
                }
            }
        }
    }

    private void fn_update(Location location) {

        intent.putExtra("latutide", location.getLatitude() + "");
        intent.putExtra("longitude", location.getLongitude() + "");
        //sendBroadcast(intent);

        RequestBody body = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart(AppConstants.APIKeys.CLIENT_ID_LOCATION, mClientId)
                .addFormDataPart(AppConstants.APIKeys.EMP_ID_LOCATION, mLoginViewModel.getEmployeeId())
                .addFormDataPart(AppConstants.APIKeys.LATITUDE, String.valueOf(location.getLatitude()))
                .addFormDataPart(AppConstants.APIKeys.LONGITUDE, String.valueOf(location.getLongitude()))
                .build();

        mEmployeeLocationViewModel.sendEmployeeLocation(body, new SendEmployeeLocationsCallback() {
            @Override
            public void onSuccess(@org.jetbrains.annotations.Nullable EmployeeLocationResponseModel response) {
                if (response != null) {
                    LogUtility.logInfo(TAG, "Location sent status " +response.status);
                }
            }

            @Override
            public void onFail(@org.jetbrains.annotations.Nullable Throwable throwable) {
                LogUtility.logInfo(TAG, throwable.getMessage());
            }
        });
    }

    private class TimerTaskToGetLocation extends TimerTask {
        @Override
        public void run() {
            mHandler.post(() -> fn_getlocation());
        }
    }

}
