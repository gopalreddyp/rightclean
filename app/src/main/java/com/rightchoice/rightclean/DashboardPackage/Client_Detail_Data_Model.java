package com.rightchoice.rightclean.DashboardPackage;

public class Client_Detail_Data_Model  {



    String clentno,clientname,assigneduser,createdtime,modifiedtime,source,starred,tags
            ,cf_1072,cf_1074,cf_1076,cf_1078,cf_1080,cf_1082,cf_1084,cf_1086,cf_1146,cf_1148
            ,id1;

    public void setClentno(String clentno) {
        this.clentno = clentno;
    }

    public void setClientname(String clientname) {
        this.clientname = clientname;
    }

    public void setAssigneduser(String assigneduser) {
        this.assigneduser = assigneduser;
    }

    public void setCreatedtime(String createdtime) {
        this.createdtime = createdtime;
    }

    public void setModifiedtime(String modifiedtime) {
        this.modifiedtime = modifiedtime;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public void setStarred(String starred) {
        this.starred = starred;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public void setCf_1072(String cf_1072) {
        this.cf_1072 = cf_1072;
    }

    public void setCf_1074(String cf_1074) {
        this.cf_1074 = cf_1074;
    }

    public void setCf_1076(String cf_1076) {
        this.cf_1076 = cf_1076;
    }

    public void setCf_1078(String cf_1078) {
        this.cf_1078 = cf_1078;
    }

    public void setCf_1080(String cf_1080) {
        this.cf_1080 = cf_1080;
    }

    public void setCf_1082(String cf_1082) {
        this.cf_1082 = cf_1082;
    }

    public void setCf_1084(String cf_1084) {
        this.cf_1084 = cf_1084;
    }

    public void setCf_1086(String cf_1086) {
        this.cf_1086 = cf_1086;
    }

    public void setCf_1146(String cf_1146) {
        this.cf_1146 = cf_1146;
    }

    public void setCf_1148(String cf_1148) {
        this.cf_1148 = cf_1148;
    }

    public void setId1(String id1) {
        this.id1 = id1;
    }


    // Getter's

    public String getClentno() {
        return clentno;
    }

    public String getClientname() {
        return clientname;
    }

    public String getAssigneduser() {
        return assigneduser;
    }

    public String getCreatedtime() {
        return createdtime;
    }

    public String getModifiedtime() {
        return modifiedtime;
    }

    public String getSource() {
        return source;
    }

    public String getStarred() {
        return starred;
    }

    public String getTags() {
        return tags;
    }

    public String getCf_1072() {
        return cf_1072;
    }

    public String getCf_1074() {
        return cf_1074;
    }

    public String getCf_1076() {
        return cf_1076;
    }

    public String getCf_1078() {
        return cf_1078;
    }

    public String getCf_1080() {
        return cf_1080;
    }

    public String getCf_1082() {
        return cf_1082;
    }

    public String getCf_1084() {
        return cf_1084;
    }

    public String getCf_1086() {
        return cf_1086;
    }

    public String getCf_1146() {
        return cf_1146;
    }

    public String getCf_1148() {
        return cf_1148;
    }

    public String getId1() {
        return id1;
    }
}
