package com.rightchoice.rightclean.DashboardPackage;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.rightchoice.rightclean.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;

public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private List<String> _listDataHeader; // header titles
    private HashMap<String, List<String>> _listDataChild;
    HashSet<String> serviceslist = new HashSet();
    private List<Integer> parentPositionsList, childPositionsList;

    public ExpandableListAdapter(Context _context, List<String> _listDataHeader, HashMap<String, List<String>> _listDataChild) {
        this._context = _context;
        this._listDataHeader = _listDataHeader;
        this._listDataChild = _listDataChild;
        parentPositionsList = new ArrayList<>();
        childPositionsList = new ArrayList<>();
    }


    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return Objects.requireNonNull(this._listDataChild.get(this._listDataHeader.get(groupPosition)))
                .get(childPosititon);
    }


    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater inflator = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflator.inflate(R.layout.custom_services_layout, null);
        }


        TextView tvSubService = convertView.findViewById(R.id.tv_sub_service_name);
        ConstraintLayout clMainContainer = convertView.findViewById(R.id.cl_container);
        ImageView ivSubService = convertView.findViewById(R.id.list_item_checkbox);

        tvSubService.setText(childText);

        clMainContainer.setOnClickListener(v -> {
            if (parentPositionsList.contains(groupPosition) && childPositionsList.contains(childPosition)) {
                parentPositionsList.remove(parentPositionsList.indexOf(groupPosition));
                childPositionsList.remove(childPositionsList.indexOf(childPosition));
                ivSubService.setImageResource(R.drawable.uncheck);
            } else {
                parentPositionsList.add(groupPosition);
                childPositionsList.add(childPosition);
                ivSubService.setImageResource(R.drawable.checkbox);
            }
        });

        if (parentPositionsList.contains(groupPosition) && childPositionsList.contains(childPosition)) {
            ivSubService.setImageResource(R.drawable.checkbox);
        } else {
            ivSubService.setImageResource(R.drawable.uncheck);
        }

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return Objects.requireNonNull(this._listDataChild.get(this._listDataHeader.get(groupPosition))).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }


    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.custom_service_head, null);
        }

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.headings);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);
        ExpandableListView eLV = (ExpandableListView) parent;
        eLV.expandGroup(groupPosition);
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
