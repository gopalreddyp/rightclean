package com.rightchoice.rightclean.DashboardPackage;

public class DataModel
{

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String name;
    boolean checked;

    public String header;

    DataModel(String name, boolean checked) {
        this.name = name;
        this.checked = checked;

    }




}
