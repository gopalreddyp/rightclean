package com.rightchoice.rightclean.DashboardPackage

import com.google.gson.Gson
import com.rightchoice.rightclean.JsonNetworkPackage.AppController
import com.rightchoice.rightclean.model.repository.shared_preferences.SharedPreferenceManager
import com.rightchoice.rightclean.model.repository.webservices.pojo.response.LoginResponseModel
import com.rightchoice.rightclean.utilities.PreferenceManager

class DashboardViewModel {

    private val preferenceManager = PreferenceManager(AppController.instance)

    fun getLoginResponse(): LoginResponseModel {
        return Gson().fromJson(preferenceManager.getString(SharedPreferenceManager.SharedPrefKeys.KEY_LOG_IN_RESPONSE), LoginResponseModel::class.java)
    }
}