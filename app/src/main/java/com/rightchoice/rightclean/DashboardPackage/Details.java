package com.rightchoice.rightclean.DashboardPackage;

import android.app.AlarmManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;
import com.rightchoice.rightclean.CircularProgressdialog.CircularProgressDialogue;
import com.rightchoice.rightclean.R;
import com.rightchoice.rightclean.model.repository.webservices.web_apis.WebApi;
import com.rightchoice.rightclean.model.utilities.AppConstants;
import com.rightchoice.rightclean.model.utilities.GeneralDialogues;
import com.rightchoice.rightclean.view_models.EmployeeLocationViewModel;
import com.rightchoice.rightclean.view_models.LoginViewModel;
import com.rightchoice.rightclean.views.activities.BaseActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

public class Details extends BaseActivity implements View.OnClickListener {

    private static final String TAG = Details.class.getSimpleName();

    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    List<String> childitems;

    String clientidno, empid;
    String currentTime;
    Button startendbtn;
    AlarmManager alarmManager;

    Double latitude, longitude;
    Geocoder geocoder;

    private LoginViewModel mLoginViewModel;
    private EmployeeLocationViewModel mLocationViewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        initViews();
        setListeners();
        initInstanceVariables();

        Bundle bundle = getIntent().getExtras();

        clientidno = bundle.getString(AppConstants.BundleKeys.CLIENT_ID);
        empid = mLoginViewModel.getEmployeeId();

        geocoder = new Geocoder(this, Locale.getDefault());

        expListView.setIndicatorBounds(expListView.getRight() - 100, expListView.getWidth());


        getServices();
        listAdapter = new com.rightchoice.rightclean.DashboardPackage.ExpandableListAdapter(this, listDataHeader, listDataChild);

    }

    private void setListeners() {
        startendbtn.setOnClickListener(this);

        expListView.setOnChildClickListener((parent, v, groupPosition, childPosition, id) -> {

            final String childText = listDataChild.get(
                    listDataHeader.get(groupPosition)).get(
                    childPosition);

            return true;
        });
    }

    private void initViews() {
        expListView = findViewById(R.id.expandable_lv);
        startendbtn = findViewById(R.id.startbutton);
        startendbtn.setVisibility(View.INVISIBLE);
    }

    private void initInstanceVariables() {
        mLoginViewModel = new LoginViewModel(getApplication());
        mLocationViewModel = new EmployeeLocationViewModel(getApplication());

        alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
    }


    private void startWork(final String empid, final String clientidno, final String currentTime) {
        CircularProgressDialogue.showDialog(Details.this, "", "");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, WebApi.ApiMethods.START_WORK, response -> {
            CircularProgressDialogue.dismissdialog();
            Log.i(TAG, "Upload work Status " + response);

            try {
                JSONObject jsonObject = new JSONObject(response);
                String status = jsonObject.getString("status");
                if (status.equals("success")) {
                    String empid1 = jsonObject.getString("empid");
                    String clientid = jsonObject.getString("clientid");
                    String someid = jsonObject.getString("id");

                    Log.i(TAG + "Employ", empid1);
                    Log.i(TAG + "client", clientid);

                    startendbtn.setVisibility(View.INVISIBLE);
                    Toast.makeText(Details.this, "Your Clean Service Started at " + currentTime, Toast.LENGTH_LONG).show();

                    Snackbar.make(findViewById(android.R.id.content), " Punch in ! Work Started!", Snackbar.LENGTH_INDEFINITE).show();

                    new Handler().postDelayed(() -> {
                        Bundle bundle = new Bundle();
                        bundle.putString(AppConstants.BundleKeys.CLIENT_ID, clientid);
                        bundle.putString(AppConstants.BundleKeys.TRANSACTION_ID, someid);
                        navigateEnd(EndClean.class, bundle);

                    }, 500);

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }, error -> {

            CircularProgressDialogue.dismissdialog();
            Log.e(TAG, "Something error " + error.getMessage());

            try {
                NetworkResponse response = error.networkResponse;
                VolleyError responseError = new VolleyError(new String(error.networkResponse.data));
                Log.i(TAG, "res" + String.valueOf(responseError));
                String errorMsg = "";

                String errorString = new String(response.data);
                Log.i("log error", errorString);
                int mstatuscode = response.statusCode;
                Log.i(TAG, String.valueOf(mstatuscode));
                if (mstatuscode == HttpURLConnection.HTTP_NOT_FOUND) {
                    Snackbar.make(findViewById(android.R.id.content), mstatuscode + " Server Not Found", Snackbar.LENGTH_LONG).show();
                } else if (mstatuscode == HttpURLConnection.HTTP_INTERNAL_ERROR) {
                    Snackbar.make(findViewById(android.R.id.content), mstatuscode + " Internal Server Error", Snackbar.LENGTH_LONG).show();

                } else if (response.statusCode == 401) {
                    Snackbar.make(findViewById(android.R.id.content), mstatuscode + " Unauthorized Error", Snackbar.LENGTH_LONG).show();

                } else {
                    Snackbar.make(findViewById(android.R.id.content), "Bad Internet try again!", Snackbar.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.i(TAG, "cause" + String.valueOf(e.getCause()));
                Log.i(TAG, "Local" + String.valueOf(e.getLocalizedMessage()));
                Log.i(TAG, "Message" + String.valueOf(e.getMessage()));
                Log.i(TAG, "Message" + String.valueOf(e.toString()));

            }
            Snackbar.make(findViewById(android.R.id.content), "Something went Wrong..Please Check internet connection!", Snackbar.LENGTH_LONG).show();

        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(AppConstants.APIKeys.CLIENT_ID, clientidno);
                params.put(AppConstants.APIKeys.EMP_ID, empid);
                params.put(AppConstants.APIKeys.PUNCH_IN, currentTime);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(this).add(stringRequest);

    }

    public void NotificationDialoag(String time2) {
       /* Log.i(TAG,"Modified TIme "+time2);

        String puremin = time2.replaceAll("[^0-9.]", "");
        Log.i(TAG,"Modified TIme2 "+puremin);


        Intent notificationIntent = new Intent(Details.this, ShowNotification.class);
        PendingIntent broadcast = PendingIntent.getBroadcast(Details.this, 100, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        Calendar cal = Calendar.getInstance();
        //cal.setTimeInMillis(System.currentTimeMillis());
        cal.add(Calendar.MINUTE, Integer.parseInt(puremin));
        alarmManager.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), broadcast);
*/
    }

    private void getServices() {

        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        CircularProgressDialogue.showDialog(Details.this, "", "");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, WebApi.ApiMethods.RIGHT_CLEAN_SERVICES, response -> {
            CircularProgressDialogue.dismissdialog();
            Log.i(TAG, response);

            try {
                JSONObject jsonObject = new JSONObject(response);
                String status = jsonObject.getString("status");
                if (status.equals("success")) {

                    JSONArray jsonArray = jsonObject.getJSONArray("result");


                    if (jsonArray.length() > 0) {

                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            JSONArray jsonArray1 = jsonObject1.optJSONArray("Services");

                            if (jsonArray1.length() > 0) {
                                Log.i(TAG, "Services");
                                startendbtn.setVisibility(View.VISIBLE);

                                for (int j = 0; j < jsonArray1.length(); j++) {

                                    JSONObject jsonObject2 = jsonArray1.getJSONObject(j);

                                    String mainservicestypeid = jsonObject2.getString("mainservicestypeno");
                                    String mainservicestypename = jsonObject2.getString("mainservicestype_tks_servicena");
                                    String msmainservicestype_tks_descripti = jsonObject2.getString("mainservicestype_tks_descripti");
                                    String msassigned_user_id = jsonObject2.getString("assigned_user_id");
                                    String mscreatedtime = jsonObject2.getString("createdtime");
                                    String msmodifiedtime = jsonObject2.getString("modifiedtime");
                                    String mssource = jsonObject2.getString("source");
                                    String msstarred = jsonObject2.getString("starred");
                                    String mstags = jsonObject2.getString("tags");
                                    String msid = jsonObject2.getString("id");


                                    listDataHeader.add(mainservicestypename);


                                    childitems = new ArrayList<String>();
                                    JSONObject jsonObject3 = jsonObject2.getJSONObject("SubServices");
                                    //boolean success = jsonObject3.getBoolean("success");

                                    JSONArray jsonArray2 = jsonObject3.getJSONArray("List");


                                    for (int k = 0; k < jsonArray2.length(); k++) {

                                        JSONObject jsonObject4 = jsonArray2.getJSONObject(k);

                                        String subservicesno = jsonObject4.getString("subservicesno");
                                        String subservices_tks_subservicesnam = jsonObject4.getString("subservices_tks_subservicesnam");
                                        String assigned_user_id = jsonObject4.getString("assigned_user_id");
                                        String createdtime = jsonObject4.getString("createdtime");
                                        String modifiedtime = jsonObject4.getString("modifiedtime");
                                        String source = jsonObject4.getString("source");
                                        String starred = jsonObject4.getString("starred");
                                        String tags = jsonObject4.getString("tags");
                                        String id = jsonObject4.getString("id");

                                        if (jsonArray2.length() > 0) {
                                            childitems.add(subservices_tks_subservicesnam);
                                        }
                                        listDataChild.put(listDataHeader.get(j), childitems);
                                        expListView.setAdapter(listAdapter);
                                    }
                                }
                            } else {
                                startendbtn.setVisibility(View.INVISIBLE);
                                Toast.makeText(Details.this, " Services Not Found!Please Add Services! ", Toast.LENGTH_LONG).show();
                            }

                        }
                    } else {
                        Toast.makeText(Details.this, " Clients Data Not Found!Please Select Client's ", Toast.LENGTH_LONG).show();
                    }


                } else if (status.equals("error")) {
                    String result1 = jsonObject.getString("result");
                    Snackbar.make(findViewById(android.R.id.content), result1, Snackbar.LENGTH_LONG).show();

                }


            } catch (JSONException e) {
                e.printStackTrace();
            }


        }, error -> {

            CircularProgressDialogue.dismissdialog();
            Log.e(TAG, "Something error " + error.getMessage());

            try {
                NetworkResponse response = error.networkResponse;
                VolleyError responseError = new VolleyError(new String(error.networkResponse.data));
                Log.i(TAG, "res" + String.valueOf(responseError));
                String errorMsg = "";

                String errorString = new String(response.data);
                Log.i("log error", errorString);
                int mstatuscode = response.statusCode;
                Log.i(TAG, String.valueOf(mstatuscode));
                if (mstatuscode == HttpURLConnection.HTTP_NOT_FOUND) {
                    Snackbar.make(findViewById(android.R.id.content), mstatuscode + " Server Not Found", Snackbar.LENGTH_LONG).show();
                } else if (mstatuscode == HttpURLConnection.HTTP_INTERNAL_ERROR) {
                    Snackbar.make(findViewById(android.R.id.content), mstatuscode + " Internal Server Error", Snackbar.LENGTH_LONG).show();

                } else if (response.statusCode == 401) {
                    Snackbar.make(findViewById(android.R.id.content), mstatuscode + " Unauthorized Error", Snackbar.LENGTH_LONG).show();

                } else {
                    Snackbar.make(findViewById(android.R.id.content), "Bad Internet try again!", Snackbar.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.i(TAG, "cause" + String.valueOf(e.getCause()));
                Log.i(TAG, "Local" + String.valueOf(e.getLocalizedMessage()));
                Log.i(TAG, "Message" + String.valueOf(e.getMessage()));
                Log.i(TAG, "Message" + String.valueOf(e.toString()));

            }
            Snackbar.make(findViewById(android.R.id.content), "Something went Wrong..!", Snackbar.LENGTH_LONG).show();


        }) {


            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(AppConstants.APIKeys.CLIENT_ID, clientidno);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(this).add(stringRequest);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onDestroy() {
        CircularProgressDialogue.dismissdialog();
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(broadcastReceiver, new IntentFilter(MyLocationServices.str_receiver));

    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
    }


    public BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            latitude = Double.valueOf(Objects.requireNonNull(intent.getStringExtra("latutide")));
            longitude = Double.valueOf(Objects.requireNonNull(intent.getStringExtra("longitude")));

            List<Address> addresses = null;

            try {
                addresses = geocoder.getFromLocation(latitude, longitude, 1);
                String cityName = addresses.get(0).getAddressLine(0);
                String stateName = addresses.get(0).getAddressLine(1);
                String countryName = addresses.get(0).getAddressLine(2);

            } catch (IOException e1) {
                e1.printStackTrace();
                Log.i(TAG, e1.getLocalizedMessage());
            }


        }
    };


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.startbutton:
                if (mLocationViewModel.checkIfLocationIsOn()) {
                    Intent intent = new Intent(getApplicationContext(), MyLocationServices.class);
                    intent.putExtra(AppConstants.BundleKeys.CLIENT_ID, clientidno);
                    startService(intent);

                    currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());

                    startWork(empid, clientidno, currentTime);
                } else {
                    GeneralDialogues.show2ButtonsDialogue(this, new GeneralDialogues.DialogueClickListener2() {
                        @Override
                        public void onPositiveButtonClicked(DialogInterface dialogInterface) {
                            navigate(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                            dialogInterface.dismiss();
                        }

                        @Override
                        public void onNegativeButtonClicked(DialogInterface dialogInterface) {
                            dialogInterface.dismiss();
                        }
                    }, getString(R.string.error_location_off), getString(R.string.ok), getString(R.string.cancel));
                }
                break;
        }
    }
}
