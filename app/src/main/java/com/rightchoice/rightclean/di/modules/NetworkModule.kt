package com.rightchoice.rightclean.di.modules

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.rightchoice.rightclean.di.scopes.PerApplication
import com.rightchoice.rightclean.utilities.GsonProvider
import dagger.Module
import dagger.Provides

@Module
class NetworkModule {

    @Provides
    @PerApplication
    fun provideGson(): Gson {
        return GsonBuilder().setDateFormat(GsonProvider.ISO_8601_DATE_FORMAT).create()
    }

    /*@Provides
    @PerApplication
    @NetworkInfo
    fun provideBaseUrl() = BuildConfig.BASE_URL*/

}