package com.rightchoice.rightclean.di.modules.activity_modules


import com.rightchoice.rightclean.di.modules.ActivityModule
import com.rightchoice.rightclean.di.scopes.PerFragment
import com.rightchoice.rightclean.views.fragments.*
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module(includes = [ActivityModule::class])
abstract class  DashboardActivityModule {

    @PerFragment
    @ContributesAndroidInjector
    abstract fun provideHomeFragment(): HomeFragment

    @PerFragment
    @ContributesAndroidInjector
    abstract fun provideEmailFragment(): EmailFragment

    @PerFragment
    @ContributesAndroidInjector
    abstract fun providePayFragment(): PayFragment

    @PerFragment
    @ContributesAndroidInjector
    abstract fun provideCompleteFragment(): CompleteFragment

    @PerFragment
    @ContributesAndroidInjector
    abstract fun provideActivitiesFragment(): ActivitiesFragment

    @PerFragment
    @ContributesAndroidInjector
    abstract fun provideSchedularFragment(): SchedularFragment

    @PerFragment
    @ContributesAndroidInjector
    abstract fun provideCallFragment(): CallFragment

    @PerFragment
    @ContributesAndroidInjector
    abstract fun provideContactsSearchFragment(): ContactsSearchFragment
}