package com.rightchoice.rightclean.di.modules.view_models

import androidx.lifecycle.ViewModel
import com.rightchoice.rightclean.activity_view_models.ChangePasswordActivityViewModel
import com.rightchoice.rightclean.di.keys.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface ChangePasswordActivityViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(ChangePasswordActivityViewModel::class)
    fun bindChangePasswordActivityViewModel(viewModel: ChangePasswordActivityViewModel): ViewModel




}