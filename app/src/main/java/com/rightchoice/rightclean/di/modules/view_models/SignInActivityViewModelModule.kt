package com.rightchoice.rightclean.di.modules.view_models

import androidx.lifecycle.ViewModel
import com.rightchoice.rightclean.activity_view_models.SignInActivityViewModel
import com.rightchoice.rightclean.di.keys.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface SignInActivityViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(SignInActivityViewModel::class)
    fun bindSignInActivityViewModel(viewModel: SignInActivityViewModel): ViewModel


}