
import com.rightchoice.rightclean.JsonNetworkPackage.AppController
import com.rightchoice.rightclean.di.modules.*
import com.rightchoice.rightclean.di.modules.activity_modules.RemoteModule
import com.rightchoice.rightclean.di.modules.view_models.*
import com.rightchoice.rightclean.di.scopes.PerApplication
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule

@PerApplication
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ActivityBindingModule::class,
        ApplicationModule::class,
        NetworkModule::class,
        DataModule::class,
        RemoteModule::class,
        ViewModelModule::class,
        ServiceModule::class,
        CompanyIDActivityViewModelModule::class,
        SignInActivityViewModelModule::class,
        ScheduleActivityViewModelModule::class,
        ProfileActivityViewModelModule::class,
    DashboardActivityViewModelModule::class,
    ForgotPasswordActivityViewModelModule::class,
    ]
)
interface ApplicationComponent : AndroidInjector<AppController> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<AppController>()
}