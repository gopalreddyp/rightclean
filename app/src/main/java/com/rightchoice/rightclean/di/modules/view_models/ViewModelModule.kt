package com.rightchoice.rightclean.di.modules.view_models

import androidx.lifecycle.ViewModelProvider
import com.rightchoice.rightclean.activity_view_models.ViewModelFactory
import dagger.Binds
import dagger.Module

@Module
interface ViewModelModule {

    @Binds
    fun bindsViewModelFactory(viewModelFactory: ViewModelFactory): ViewModelProvider.Factory

}