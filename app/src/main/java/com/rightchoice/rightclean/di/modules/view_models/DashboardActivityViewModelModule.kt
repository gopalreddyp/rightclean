package com.rightchoice.rightclean.di.modules.view_models

import androidx.lifecycle.ViewModel
import com.rightchoice.rightclean.activity_view_models.*
import com.rightchoice.rightclean.di.keys.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface DashboardActivityViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(DashboardActivityViewModel::class)
    fun bindDashboardActivityViewModel(viewModel: DashboardActivityViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HomeFragmentViewModel::class)
    fun bindUserHomeFragmentViewModel(viewModel: HomeFragmentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EmailFragmentViewModel::class)
    fun bindUserEmailFragmentViewModel(viewModel: EmailFragmentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PayFragmentViewModel::class)
    fun bindUserPayFragmentViewModel(viewModel: PayFragmentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CompleteFragmentViewModel::class)
    fun bindUserCompleteFragmentViewModel(viewModel: CompleteFragmentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ActivitiesFragmentViewModel::class)
    fun bindUserActivitiesFragmentViewModel(viewModel: ActivitiesFragmentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SchedularFragmentViewModel::class)
    fun bindUserSchedularFragmentViewModel(viewModel: SchedularFragmentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CallFragmentViewModel::class)
    fun bindUserCallFragmentViewModel(viewModel: CallFragmentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ContactSearchFragmentViewModel::class)
    fun bindUserContactSearchFragmentViewModel(viewModel: ContactSearchFragmentViewModel): ViewModel

}