package com.rightchoice.rightclean.di.modules

import com.rightchoice.rightclean.DashboardPackage.DashboardActivity
import com.rightchoice.rightclean.di.modules.activity_modules.*
import com.rightchoice.rightclean.di.scopes.PerActivity
import com.rightchoice.rightclean.views.activities.*
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

    @PerActivity
    @ContributesAndroidInjector(modules = [CompanyIDActivityModule::class])
    abstract fun contributeCompanyIDActivity(): CompanyIDActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [SignInActivityModule::class])
    abstract fun contributeSignInActivity(): SignInActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [ScheduleActivityModule::class])
    abstract fun contributeScheduleActivity(): ScheduleActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [ProfileActivityModule::class])
    abstract fun contributeProfileActivity(): ProfileActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [DashboardActivityModule::class])
    abstract fun contributeDashboardActivity(): DashboardActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [ForgotPasswordActivityModule::class])
    abstract fun contributeForgotPasswordActivity(): ForgotPasswordActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [ChangePasswordActivityModule::class])
    abstract fun contributeChangePasswordActivity(): ChangePasswordActivity
}