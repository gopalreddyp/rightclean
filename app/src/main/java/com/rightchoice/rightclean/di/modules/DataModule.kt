package com.rightchoice.rightclean.di.modules

import com.rightchoice.rightclean.di.scopes.PerApplication
import com.rightchoice.rightclean.utilities.GsonProvider
import dagger.Module
import dagger.Provides

@Module
abstract class DataModule {

    @Module
    companion object {
        @PerApplication
        @Provides
        @JvmStatic
        fun provideGson(): GsonProvider = GsonProvider()

    }

    /*  @PerApplication
      @Binds
      abstract fun provideContactsManagerRepo(contactMng : ContactsDataRepository) : ContactsRepository


      @PerApplication
      @Binds
      abstract fun bindPostExecutionThread(uiThread: UiThread): PostExecutionThread

      @PerApplication
      @Binds
      abstract fun bindCountryDataRepository(countryLocalDataRepository: CountryDataRepository): CountryRepository

      @PerApplication
      @Binds
      abstract fun bindOtpGenerateDataRepository(otpDataRepository: OtpDataRepository): OtpRepository

      @PerApplication
      @Binds
      abstract fun bindUserProfileDataRepository(userProfileRegisterDataRepository: UserProfileRegisterDataRepository): UserProfileRegisterRepository

      @PerApplication
      @Binds
      abstract fun bindGeneralDataRepository(generalDataRepository: GeneralDataRepository): GeneralRepository

      @PerApplication
      @Binds
      abstract fun bindContactSyncApiDataRepository(contactSyncApiDataRepository: ContactSyncApiDataRepository): ContactSyncApiRepository

      @PerApplication
      @Binds
      abstract fun bindSettingsDataRepository(settingsDataRepository: SettingsDataRepository): SettingsRepository

      @PerApplication
      @Binds
      abstract fun bindContactOperationDataRepository(contactOperationDataRepository: ContactOperationDataRepository): ContactOperationRepository

      @PerApplication
      @Binds
      abstract fun bindImageDataRepository(imageDataRepository: ImageDataRepository): ImageRepository

      @PerApplication
      @Binds
      abstract fun bindGroupSyncApiDataRepository(groupSyncApiDataRepository: GroupSyncApiDataRepository): GroupSyncApiRepository*/

}