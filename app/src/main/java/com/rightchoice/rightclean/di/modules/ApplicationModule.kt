package com.rightchoice.rightclean.di.modules

import android.content.Context
import com.rightchoice.rightclean.JsonNetworkPackage.AppController
import com.rightchoice.rightclean.di.scopes.ApplicationContext
import com.rightchoice.rightclean.di.scopes.PerApplication
import dagger.Binds
import dagger.Module

@Module
abstract class ApplicationModule {

    @ApplicationContext
    @PerApplication
    @Binds
    abstract fun bindApplication(application: AppController): Context
}