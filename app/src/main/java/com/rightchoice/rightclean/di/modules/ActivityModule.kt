package com.rightchoice.rightclean.di.modules

import android.app.Activity
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.FragmentManager
import com.rightchoice.rightclean.activity_view_models.BaseViewModel
import com.rightchoice.rightclean.di.scopes.ActivityContext
import com.rightchoice.rightclean.di.scopes.PerActivity
import com.rightchoice.rightclean.views.activities.BaseActivity2
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class ActivityModule {

    @Module
    companion object {
        @JvmStatic
        @Provides
        @PerActivity
        fun provideFragmentManager(activity: AppCompatActivity): FragmentManager {
            return activity.supportFragmentManager
        }
    }

    @Binds
    @PerActivity
    abstract fun bindAppCompatActivity(activity: BaseActivity2<ViewDataBinding, BaseViewModel<*>>): AppCompatActivity

    @Binds
    @PerActivity
    abstract fun bindActivity(activity: AppCompatActivity): Activity

    @ActivityContext
    @Binds
    @PerActivity
    abstract fun bindActivityContext(activity: Activity): Context

}