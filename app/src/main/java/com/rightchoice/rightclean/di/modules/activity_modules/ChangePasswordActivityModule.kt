package com.rightchoice.rightclean.di.modules.activity_modules


import com.rightchoice.rightclean.di.modules.ActivityModule
import dagger.Module

@Module(includes = [ActivityModule::class])
abstract class  ChangePasswordActivityModule {


}