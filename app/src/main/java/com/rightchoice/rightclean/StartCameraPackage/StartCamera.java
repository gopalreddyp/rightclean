package com.rightchoice.rightclean.StartCameraPackage;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.rightchoice.rightclean.CameraPackage.AccessCam;
import com.rightchoice.rightclean.CircularProgressdialog.CircularProgressDialogue;
import com.rightchoice.rightclean.R;

import java.util.Objects;

public class StartCamera extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_camera);
        Objects.requireNonNull(getSupportActionBar()).hide();
    }

    public void Touchtostart(View view) {
        CircularProgressDialogue.showDialog(StartCamera.this,"","");
        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                CircularProgressDialogue.dismissdialog();
                startActivity(new Intent(StartCamera.this, AccessCam.class));
                finish();
            }
        },2000);


    }


    public void termsandconditions(View view)
    {

       // Intent intent = new Intent(StartCamera.this, TandC.class);
       // startActivity(intent);

    }


}
