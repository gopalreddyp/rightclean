package com.rightchoice.rightclean.utilities.Interfaces

interface ClickPositionProvider {
    fun onClick(position: Int)
}