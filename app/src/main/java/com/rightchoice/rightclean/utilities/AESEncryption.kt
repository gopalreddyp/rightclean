package com.rightchoice.rightclean.utilities

class AESEncryption {

    /* private val TAG: String = AESEncryption()::class.java.getSimpleName()
     private val instance: AESEncryption? = null
     private lateinit var KEY: ByteArray

     // Your IV Initialization Vector
     private lateinit var ivx: ByteArray

     fun readFileToByteArray(file: File): ByteArray? {
         var fis: FileInputStream? = null
         // Creating a byte array using the length of the file
         // file.length returns long which is cast to int
         val bArray = ByteArray(file.length().toInt())
         try {
             fis = FileInputStream(file)
             fis.read(bArray)
             fis.close()
         } catch (ioExp: IOException) {
             ioExp.printStackTrace()
         }
         return bArray
     }

     fun AESComeraEncryption() {
         try {
             KEY = BuildConfig.KEY.getBytes("UTF8")
             ivx = BuildConfig.IV.getBytes()
         } catch (e: UnsupportedEncodingException) {
             e.printStackTrace()
         }
     }

     @Throws(
         NoSuchAlgorithmException::class,
         NoSuchPaddingException::class,
         IllegalBlockSizeException::class,
         BadPaddingException::class,
         InvalidKeyException::class,
         UnsupportedEncodingException::class,
         InvalidAlgorithmParameterException::class
     )
     fun encrypt(message: String): String? {
         val srcBuff = message.toByteArray(charset("UTF8"))
         val skeySpec = SecretKeySpec(KEY, "AES")
         val ivSpec = IvParameterSpec(ivx)
         val ecipher = Cipher.getInstance("AES/CBC/PKCS7Padding")
         ecipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivSpec)
         val dstBuff = ecipher.doFinal(srcBuff)
         return Base64.encodeToString(dstBuff, Base64.DEFAULT)
     }

     @Throws(
         NoSuchAlgorithmException::class,
         NoSuchPaddingException::class,
         IllegalBlockSizeException::class,
         BadPaddingException::class,
         InvalidKeyException::class,
         InvalidAlgorithmParameterException::class
     )
     fun encrypt(srcBuff: ByteArray?): ByteArray? {
         val skeySpec = SecretKeySpec(KEY, "AES")
         val ivSpec = IvParameterSpec(ivx)
         val ecipher = Cipher.getInstance("AES/CBC/PKCS7Padding")
         ecipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivSpec)
         return ecipher.doFinal(srcBuff)
     }

     fun decrypt(encrypted: String?): String? {
         var original = ""
         try {
             val skeySpec = SecretKeySpec(KEY, "AES")
             val ivSpec = IvParameterSpec(ivx)
             val ecipher = Cipher.getInstance("AES/CBC/PKCS7Padding")
             ecipher.init(Cipher.DECRYPT_MODE, skeySpec, ivSpec)
             val raw = Base64.decode(encrypted, Base64.DEFAULT)
             val originalBytes = ecipher.doFinal(raw)
             original = String(originalBytes, "UTF8")
             return original
         } catch (e: Exception) {
             MyLogger.debug(TAG, "Decrypt Exception" + e.localizedMessage)
         }
         return original
     }

     @Throws(
         NoSuchAlgorithmException::class,
         NoSuchPaddingException::class,
         InvalidKeyException::class,
         InvalidAlgorithmParameterException::class,
         IllegalBlockSizeException::class,
         BadPaddingException::class
     )
     fun decrypt(encrypted: ByteArray?): ByteArray? {
         val skeySpec = SecretKeySpec(KEY, "AES")
         val ivSpec = IvParameterSpec(ivx)
         val ecipher = Cipher.getInstance("AES/CBC/PKCS7Padding")
         ecipher.init(Cipher.DECRYPT_MODE, skeySpec, ivSpec)
         return ecipher.doFinal(encrypted)
     }*/
}
