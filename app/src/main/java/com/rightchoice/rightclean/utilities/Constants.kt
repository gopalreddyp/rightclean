package com.rightchoice.rightclean.utilities

object Constants {
    object BundleKeys {
        const val CATEGORY_TYPE = "category_type"
    }

    object ListingType{
        const val MODE_LIST:Byte = 0
        const val MODE_GRID:Byte = 1
    }

    object BottomSheetOptions {
        const val HOME:Byte = 0
        const val HAPPINESS:Byte = 1
        const val SHOPS:Byte = 2
    }
}