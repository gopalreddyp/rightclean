package com.rightchoice.rightclean.utilities.Interfaces

interface RemoveFromFavourite {
    fun productPosition(position: Int)
}