package com.rightchoice.rightclean.utilities.Interfaces;

public interface DrawableClickListener {
     enum DrawablePosition { TOP, BOTTOM, LEFT, RIGHT };
     void onClick(DrawablePosition target);
}