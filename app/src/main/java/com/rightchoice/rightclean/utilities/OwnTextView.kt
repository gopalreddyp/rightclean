package com.rightchoice.rightclean.utilities

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.annotation.Nullable
import androidx.appcompat.widget.AppCompatTextView

class OwnTextView @JvmOverloads constructor(context: Context?, @Nullable attrs: AttributeSet? = null, defStyleAttr: Int = 0) : AppCompatTextView(context!!, attrs, defStyleAttr) {
    var drawableRightClick: DrawableRightClickListener? = null
        private set

    var drawableLeftClick: DrawableLeftClickListener? = null
        private set
    val DRAWABLE_LEFT = 0 //Left picture
    val DRAWABLE_TOP = 1 //Picture above
    val DRAWABLE_RIGHT = 2 //Right picture
    val DRAWABLE_BOTTOM = 3 //The following picture

    interface DrawableRightClickListener {
        fun onDrawableRightClickListener(view: View?)
    }

    interface DrawableLeftClickListener {
        fun onDrawableLeftClickListener(view: View?)
    }

    fun setDrawableRightClickListener(listener: DrawableRightClickListener?) {
        drawableRightClick = listener
    }

    fun setDrawableLeftClickListener(listener: DrawableLeftClickListener?) {
        drawableLeftClick = listener
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                if (drawableRightClick != null) {
                    val drawableRight = compoundDrawables[DRAWABLE_RIGHT]
                    if (drawableRight != null && event.rawX >= right - drawableRight.bounds.width()) {
                        drawableRightClick!!.onDrawableRightClickListener(this)
                        return true
                    }
                }
                if (drawableLeftClick != null) {
                    val drawableLeft = compoundDrawables[DRAWABLE_LEFT]
                    if (drawableLeft != null && event.rawX <= left + drawableLeft.bounds.width()) {
                        drawableLeftClick!!.onDrawableLeftClickListener(this)
                        return true
                    }
                }
            }
        }
        return super.onTouchEvent(event)
    }
}