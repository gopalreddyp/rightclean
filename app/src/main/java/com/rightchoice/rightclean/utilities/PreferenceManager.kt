package com.rightchoice.rightclean.utilities

import android.content.Context
import com.rightchoice.rightclean.di.scopes.ApplicationContext
import com.rightchoice.rightclean.di.scopes.PerApplication
import com.rightchoice.rightclean.utilities.PreferenceManager.SharedPrefKeys.SHARED_PREF
import javax.inject.Inject

@PerApplication
class PreferenceManager @Inject constructor(
    @ApplicationContext private val context: Context
) {

    // region VARIABLES

    private val sharedPreferences by lazy {
        val preferences =
            context.getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE)
        preferences
    }


    object SharedPrefKeys {
        const val KEY_IS_USER_LOGGED_IN = "key_is_user_logged_in"
        const val KEY_COMPANY_LOGO = "key_company_logo"
        const val KEY_COMPANY_INFO = "key_company_info"
        const val KEY_LOG_IN_RESPONSE = "key_login_response"
        const val SHARED_PREF = "shared_pref"
    }

    fun clearPreferences(){
        sharedPreferences.edit().clear().apply()
    }

    /*
     * @param key is unique identity to stored data
     * @param value is the data you want to store against key
     * */
    fun setString(key: String, value: String) {
        sharedPreferences.edit().putString(key, value).apply()
    }

    /*
     * @param key This parameter is used as a key store to fetch the data against it.
     * @return  This method returns require String value if the key not found then it returns 'null' as
     * default value
     * */
    fun getString(key: String): String? {
        return sharedPreferences.getString(key, null)
    }

    /*
    * @param key is unique identity to stored data
    * @param value is the data you want to store against key
    * */
    fun setInt(key: String?, value: Int) {
        sharedPreferences.edit().putInt(key, value).apply()
    }

    /*
    * @param key This parameter is used as a key store to fetch the data against it.
    * @return  This method returns require Integer value if the key not found then it returns '-1' as
    * default value
    * */
    fun getInt(key: String?): Int {
        return sharedPreferences.getInt(key, -1)
    }

    /*
     * @param key is unique identity to stored data
     * @param value is the data you want to store against key
     * */
    fun setBoolean(key: String?, value: Boolean) {
        sharedPreferences.edit().putBoolean(key, value).apply()
    }

    /*
     * @param key This parameter is used as a key store to fetch the data against it.
     * @return  This method returns require boolean value if the key not found then it returns 'false' as
     * default value
     * */
    fun getBoolean(key: String?): Boolean {
        return sharedPreferences.getBoolean(key, false)
    }

}