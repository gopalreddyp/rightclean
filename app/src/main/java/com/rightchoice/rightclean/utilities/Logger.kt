package com.rightchoice.rightclean.utilities

import android.util.Log

object MyLogger {

    private val shouldLog: Boolean = true

    fun debug(tag: String, message: String) {
        if (shouldLog)
            Log.d(tag, message)
    }
}